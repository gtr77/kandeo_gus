<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>CMS imaginamos V 1.6</title>

        <!--[if lt IE 9]>
          <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!--css Files-->
        <link href="<?php echo back_asset('css/0ui-custom.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo back_asset('css/buttons.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo back_asset('css/cms.style.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo back_asset('css/icon.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo back_asset('css/timepicker.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo back_asset('css/ui-custom.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo back_asset('components/colorpicker/css/colorpicker.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo back_asset('components/elfinder/css/elfinder.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo back_asset('components/datatables/dataTables.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo back_asset('components/validationEngine/validationEngine.jquery.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo back_asset('components/jscrollpane/jscrollpane.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo back_asset('components/fancybox/jquery.fancybox.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo back_asset('components/tipsy/tipsy.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo back_asset('components/editor/jquery.cleditor.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo back_asset('components/chosen/chosen.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo back_asset('components/confirm/jquery.confirm.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo back_asset('components/sourcerer/sourcerer.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo back_asset('components/fullcalendar/fullcalendar.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo back_asset('components/Jcrop/jquery.Jcrop.css') ?>" rel="stylesheet" type="text/css" />

        <link rel="stylesheet" href="<?php echo back_asset('fonts/opensans/stylesheet.css') ?>">
        <link rel="stylesheet" href="<?php echo back_asset('js/lib/font-awesome/css/font-awesome.css')?>">
        <link href="<?php echo back_asset('css/kandeo.css')?>" rel="stylesheet" type="text/css" />
      

        <?php echo assets_css(array('login.less', 'keyframes.css')) ?>

        <style type="text/css">
            html {
                background-image: none;
            }
            #versionBar {
                background-color:#212121;
                position:fixed;
                width:100%;
                height:35px;
                bottom:0;
                left:0;
                text-align:center;
                line-height:35px;
            }
            .copyright{
                text-align:center; font-size:10px; color:#CCC;
            }
            .copyright a{
                color:#A31F1A; text-decoration:none
            }
        </style>
    </head>
   
<body class="bg_login">

<section class="preload">
  <div class="loading waves-button waves-effect waves-light">
    <div class="logo_load"><img src="../back/assets/images/logo.png" alt="Kandeo"></div>
  </div>
</section>

<section class="content_all ">
  <a href="javascript:void(0)" class="btn_"></a>
  
  <!--Contenidos Sitio-->
  <section class="cont_home">   

    <section class="cont_login">

      <div class="logo_login wow flipInX" data-wow-delay="0.2s">
        <img src="../back/assets/images/logo.png" alt="">
      </div>

      <form method="post" id="form1" name="login" class="form_login elem_blancos wow fadeInUp" data-wow-delay="0.4s" action="<?php echo cms_url('login/ingreso') ?>" >
         <fieldset class="fila">
           <input name="email" type="email" placeholder="Correo - Usuario">
         </fieldset>
         <fieldset class="fila">
           <input name="password" type="password"  placeholder="Contraseña"> 
         </fieldset>
         
         <button type="submit" class="btn_border" name="btnLogin">Login</button>
            <a href="javascript:void(0)" class="link2 link_recup_pass">¿Forget your password?</a>
      </form>

      <div class="form_recuperar elem_blancos wow fadeInUp">   
           <p>Enter your email address and send you a link to recover your password.</p>      
         <fieldset>
           <input type="text" name="email" placeholder="Enter your email" class="validate[required,custom[email]]" minlength="2">         
         </fieldset>                          
          <button type="submit" class="btn_border" name="btnLogin">Restore password</button>
          <div class="modal_all">
            <a href="#" data-effect="mfp-3d-unfold" class="Modalrecuperar"></a>
          </div>
            <a href="javascript:void(0)" class="link2 link_ingreso">Back to login</a>
      </div>  

    </section>

      <div class="clear"></div>
  </section>
  <!--Fin Contenidos Sitio-->

    <div class="clear"></div>

<script src="<?php echo back_asset('js/lib/jquery-1.11.0.min.js')?>"></script>   
<script src="<?php echo back_asset('js/lib/jquery-ui.min.js')?>"></script>
<!--Plungis-->
<script src="<?php echo back_asset('js/lib/plugins.min.js')?>"></script>
<script src="<?php echo back_asset('js/app.min.js')?>"></script>


<script type="text/javascript" charset="utf-8" async defer>
//Iniciar graficas
window.onload = function(){
  var grafl = document.getElementById("canvas").getContext("2d");
  window.myLine = new Chart(grafl).Line(lineChartData, {
    responsive: true
  });
  var ctx = document.getElementById("chart_area").getContext("2d");
  window.myPie = new Chart(ctx).Pie(pieData, {
    responsive: true
  });
  // window.myPie = new Chart(ctx).Doughnut(pieData, {
  //   responsive: true
  // });
}
</script>


        <script type="text/javascript" src="<?php echo back_asset('scripts/login.min.js') ?>"></script>

</section><!--Content_all-->


</body>
</html>
