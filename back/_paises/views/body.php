<section class="content_all">



    <!--Contenidos Sitio-->
    <section class="cont_home">           

      <section class="conten_inicial">

        <!---Titulo-->
        <?php
          echo menu_configuracion();          
        ?>
        <!--Fin Titulo-->                  

<section class="drag-container">
   <ul class="drag-list listEtapas">
        <li class="drag-column drag-column-on-hold drag-column2">
             
            <div class="tittleList">
                <a href="#">
                  <span><strong></strong></span>
                  <i class="fa fa-flag" aria-hidden="true"></i>
                   
                  <h3>Países</h3>
                     <span class="price new_crear_pais">Nuevo País</span>
                </a>
            </div>
            
            <ul class=" list_user_register list_paises m_t_20">
                
                          <?php
                          foreach($listado as $li){
                              ?>

                    <li class="large-6 medium-5 small-5 column padding animated fadeInUp paisdel<?php echo $li->id; ?>">
                           <div class="bg_panel funts">
                              <section class="left p_r_20 txt_center padd_all">
                                <div class="name_user animated flipInY">
                                  <img src="<?php echo base_url() ?>uploads/<?php echo $li->imagen; ?>">
                                </div>
                              </section>
                              <div class="txt_left padd_all left">
                                <div class="per_nombre">
                                   <h3><?php echo $li->nombre; ?></h3>
                                </div>  
                              </div>  
                              <div class="right">
                                <div class="action_user">
                                  <div class="list_setting"><i class="ti-align-left"></i>
                                    <ul>  
                                        <li><a href="javascript:void(0)" class="new_paises" onclick="id_editar_pais(<?php echo $li->id; ?>)">Editar</a></li>
                                        <li><a href="javascript:void(0)" onclick="id_eliminar_pais(<?php echo $li->id; ?>)">Delete</a></li>
                                    </ul>
                                  </div> 
                                </div>
                              </div>
                                 <div class="clear"></div>
                           </div>
                      </li>


                              <?php
                              }
                              ?>
                
                </ul>
        </li>
                  
             
    <li class="drag-column drag-column-on-hold drag-column2">
                
              <div class="tittleList">
                <a href="#">
                  <span><strong></strong></span>
                  <i class="fa fa-braille" aria-hidden="true"></i>
                  <h3>Sectores</h3>
                  <span class="price new_crear_prueba" onclick="nuevo_sector()">Nuevo Sector</span>
                </a>
              </div>
        
        <ul class=" list_user_register list_paises m_t_20">
             
                  <?php
                  foreach($sectores as $li){
                      ?>
                  
                           <li class="large-6 medium-5 small-5 column padding animated fadeInUp sectordel<?php echo $li->id; ?>">
                                   <div class="bg_panel funts">

                                      <div class="txt_left padd_all left">
                                        <div class="per_nombre">
                                           <h3><?php echo $li->nombre; ?></h3>
                                        </div>  
                                      </div>  
                                      <div class="right">
                                        <div class="action_user">
                                          <div class="list_setting"><i class="ti-align-left"></i>
                                            <ul>  
                                <li><a href="javascript:void(0)"  onclick="formulario_editar(<?php echo $li->id; ?>)">Editar</a></li>
                                    <li><a href="javascript:void(0)" onclick="id_eliminar_sector(<?php echo $li->id; ?>)">Eliminar</a></li>
                                            </ul>
                                          </div> 
                                        </div>
                                      </div>
                                         <div class="clear"></div>
                                   </div>

                         </li>
                 <?php
                  }
                  ?>
                </ul>
        </li>
             
             
   </ul>
              
			   
        </section>
      
          <div class="clear"></div>
      </section>

        <div class="clear"></div>
    </section>
    <!--Fin Contenidos Sitio-->

    <div class="clear"></div>


</section><!--Content_all-->
<script>
    
//FUNCIONES PAISES
       
  function add_pais(){
      
      
         
           var datos = new FormData($("#formu_paises")[0]);
     
      
             $.ajax({
                url: "<?php echo base_url() ?>cms/paises/add_paises", 
                data: datos,
                type: "POST",
                contentType: false,
                processData: false,

                success: function(data) {
                  location.reload();
                  return data; 
                }
              });
    }
    
    function id_editar_pais(id){
        
        $("#formu_paises_edit").append('<input type="hidden" name="id" id="id" value="'+id+'">');
    }
    
    
    function editar_pais(){
        
        var datos = new FormData($("#formu_paises_edit")[0]);
        
      
             $.ajax({
                url: "<?php echo base_url() ?>cms/paises/edit_paises", 
                data: datos,
                type: "POST",
                contentType: false,
                processData: false,

                success: function(data) {
                  location.reload();
                  return data; 
                }
              });
        
    }
    
    function id_eliminar_pais(id){

            $.ajax({
                url: "<?php echo base_url() ?>cms/paises/deletePaises", 
                data: {id: id} ,
                type: "POST",
                dataType: "json",
                success: function(data) {
                    $('.paisdel'+id).css({'display':'none'});
                  return data; 
                }
              });
    }
 
    
//FUNCIONES SECTORES
    
    function crear_sector(){
      
        var datos = new FormData($("#formu_crear_sector")[0]);
     
      
             $.ajax({
                url: "<?php echo base_url() ?>cms/sectores/add_sectores", 
                data: datos,
                type: "POST",
                contentType: false,
                processData: false,

                success: function(data) {
                  location.reload();
                  return data; 
                }
              });
    }
    
    function formulario_editar(id){
        
       // $("#formu_paises_edit").append('<input type="hidden" name="id" id="id" value="'+id+'">');
        
        var id_referencia = "editar_sector";
        var nombre_formulario = "Editar Sector";
        var campos = Array('nombre|text','id|hidden|'+id+'');
       
        
        generar_formulario(id_referencia,nombre_formulario,campos);
    }
    
    function editar_sector(){
        
            var datos = new FormData($("#formu_editar_sector")[0]);
        
      
             $.ajax({
                url: "<?php echo base_url() ?>cms/sectores/edit_sectores", 
                data: datos,
                type: "POST",
                contentType: false,
                processData: false,

                success: function(data) {
                  location.reload();
                  return data; 
                }
              });
        
    }
    
    function nuevo_sector(){
        
        var id_referencia = "crear_sector";
        var nombre_formulario = "Nuevo Sector";
        var campos = Array('nombre|text');
       
        
        generar_formulario(id_referencia,nombre_formulario,campos);
    }
    
    function id_eliminar_sector(id){

            $.ajax({
                url: "<?php echo base_url() ?>cms/sectores/deleteSectores", 
                data: {id: id} ,
                type: "POST",
                dataType: "json",
                success: function(data) {
                    $('.sectordel'+id).css({'display':'none'});
                  return data; 
                }
              });
    }

</script>
