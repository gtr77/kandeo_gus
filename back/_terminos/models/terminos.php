<?php

class Terminos extends DataMapper {

    public $model = 'terminos';
    public $table = 'terminos';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getTerminos(){
        return $this->get();
    }
    
    public function updateTerminos($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
}
