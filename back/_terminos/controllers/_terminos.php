<?php

class _Terminos extends Back_Controller {

    public function __construct() {
        parent::__construct();
        $this->_data['nombremodulo'] = "TERMINOS Y CONDICIONES";
        if (true != $this->have_admin_access()) {
            return redirect(cms_url('login'), 'refresh');
        }
    }

  
    
    // ------------------------------TERMINOS Y CONDICIONES----------------------------------------
    
    public function index($not = '') {
        $this->_data['not'] = $not;
        $c = new Terminos();
        $info = $c->getTerminos();
        $this->_data["info"] = $info;
        return $this->build('terminos');
    }       
    
    public function update_terminos(){
        $f = new Terminos();
        $datos = array(
            'id' => $this->input->post('id'),               
            'titulo' => $this->input->post('titulo'),               
            'texto' => $this->input->post('texto'),                            
        );
        $update = $f->updateTerminos($datos); 
        $not = FALSE;
        if ($update){
            $not = TRUE;
        }
        return redirect("cms/terminos/index/".$not);
    }
}
