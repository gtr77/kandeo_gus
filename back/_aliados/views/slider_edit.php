<div class="widget">
    <div class="header"><span><span class="ico gray sphere"></span><?php echo $nombremodulo ?></span>
        <br /><?php echo anchor('cms/aliados/aliados', 'Volver', 'class="uibutton icon special answer" style="float:right;position: relative;top: -5px"') ?>                 
    </div><!-- End header -->
    <div class="content">
        <div class="formEl_b">
            <div>
                <fieldset>
                    <div class="imu_info" id="info"></div>
                    <?php echo form_open_multipart('cms/aliados/update_slider/', 'id="form"') ?>
                        <input style="width:250px" type="hidden"  id="id" name="id" class="small" value="<?php echo $info->id; ?>" /> 
                    <div class="section">
                         <label>Titulo </label>
                        <div>
                            <input style="width:250px" type="text"  id="titulo" name="titulo" class="large" value="<?php echo $info->titulo; ?>" maxlength="28" title="28 Caracteres máximo" /> 
                            <span class="f_help"> L&iacute;mite de car&aacute;cteres: <span class="titulo"></span></span>
                                        <script type="text/javascript">
                                        $("#titulo").limit("28",".titulo");
                                        </script>
                        </div>
                    </div>
                        <div class="section" >
                            <label>Texto</label>
                            <div>
                                <textarea name="subtitulo" id="subtitulo" cols="75" rows="15" ><?php echo $info->subtitulo; ?></textarea>
                                <span class="f_help"> L&iacute;mite de car&aacute;cteres: <span class="subtitulo"></span></span>
                                <script>
                                    $("#subtitulo").limit("120",".subtitulo");
                                </script>
                            </div>
                        </div>
                    <div class="section">
                        <label>Imagen </label>
                        <label>Subir nueva imagen (400px x 300px)</label><br/><br/>
                        <div>
                            <img class="cuadro_edicion_fotos" id="img1"  src="<?php echo base_url()."uploads/slider/new/".$info->imagen; ?>" width="450">
                            <div class="cuadro_edicion_fotos" id="divimg1"></div>
                            <br />
                            <input style="border-radius: 5px; " type="file" name="nombre" id="fileUpload1" class="fileUpload2" />    
                            <input type="hidden" name="imagen" id="imagen" />
                        </div>
                    </div>
                  
                    
                    <div style="width: 100px;height: 30px;position: relative;top: 30px">
                        <input onclick="valida();" type="button" value="Guardar" class="uibutton confirm" />
                        
                    </div>
                    <?php echo form_close() ?>
                    <p>&nbsp;</p>
                 </fieldset>
                <br>
            </div>
        </div>
    </div>	
</div><!-- End content -->
<script type="text/javascript">
    function valida(){        
        var titulo = $("#titulo").val();
        if (titulo == ''){
            $('#info').focus();
            showError('Complete todos los campos.',3000);
        }else{
            $('#form').submit();   
        }
   }                   
    $(document).ready(function() {
        var texto = '';
        var nextinput = parseInt($("#inputTotal").val());
        $('#fileUpload1').uploadify({
                    'uploader'          : '<?php echo site_url(); ?>'+'back/assets/components/uploadify/uploadify.swf',
                    'script'            : '<?php echo site_url(); ?>' + 'cms/aliados/upload_slider/',
                    'cancelImg'         : '<?php echo site_url(); ?>'+'back/assets/components/uploadify/cancel.png',
                    'auto'              : true,
                    'folder'            : '',
                    'queueSizeLimit'    : 3,
                    'multi'             : false,
                    'fileExt'           : '*.jpg;*.jpeg;*.png;*gif',
                    'auto'              : true,
                    'buttonText'        : 'Cargar imagen.',
                    'onComplete'  : function(event, queueId, fileObj, response, data) {
                var responseJson = $.parseJSON(response);
                if (responseJson.ok === true) {
                        $("#img1").attr('src','<?php echo base_url() ?>uploads/slider/'+responseJson.data.file_name);
                        $("#img1").hide(800).delay(2000).show(800);
                        $("#divimg1").html('<h2>Cargando imagen...</h2>')
                        $("#divimg1").show(850).delay(2000).hide(750);
                        $("#imagen").val(responseJson.data.file_name)
                } else {
                        showError('Problemas al carga imagen.',3000);                            
                }
            }
                });   
    
    });
</script>
 <script>
    <?php if (isset($not)){
            if ($not ==  1){?>
            showSuccess('Actualización correcta.',3000);
            <?php } ?>
    <?php } ?>
</script>
