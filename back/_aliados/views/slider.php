<div class="widget">
    <div class="header"><span><span class="ico gray sphere"></span><?php echo $nombremodulo ?></span>
    </div><!-- End header -->
    <div class="content">
        <div class="formEl_b">
            <div>
                <fieldset>
                    <div class="imu_info" id="info"></div>
                    <?php echo form_open_multipart('cms/aliados/new_slider/', 'id="form"') ?>
                    <div class="section">
                         <label>Titulo </label>
                        <div>
                            <input style="width:250px" type="text"  id="titulo" name="titulo" class="large" maxlength="28" title="28 Caracteres máximo" /> 
                            <span class="f_help"> L&iacute;mite de car&aacute;cteres: <span class="titulo"></span></span>
                                        <script type="text/javascript">
                                        $("#titulo").limit("28",".titulo");
                                        </script>
                        </div>
                    </div>
                    <div class="section" >
                            <label>Texto</label>
                            <div>
                                <textarea name="subtitulo" id="subtitulo" cols="75" rows="15" ></textarea>
                                <span class="f_help"> L&iacute;mite de car&aacute;cteres: <span class="subtitulo"></span></span>
                                <script>
                                    $("#subtitulo").limit("120",".subtitulo");
                                </script>
                            </div>
                        </div>
                    <div class="section">
                        <label>Imagen </label>
                        <label>Subir nueva imagen (400px x 300px)</label><br/><br/>
                        <div>
                            <img class="cuadro_edicion_fotos" id="img1" src="" width="450">
                            <div class="cuadro_edicion_fotos" id="divimg1"></div>
                            <br />
                            <input style="border-radius: 5px; " type="file" name="nombre" id="fileUpload1" class="fileUpload" />    
                            <input type="hidden" name="imagen" id="imagen" />
                        </div>
                    </div>
                    <div style="width: 100px;height: 30px;position: relative;top: 30px">
                        <input onclick="valida();" type="button" value="Guardar" class="uibutton confirm" />                        
                    </div>
                    <?php echo form_close() ?>
                    <p>&nbsp;</p>
                 </fieldset>                
                <br />
                <p>&nbsp;</p>
                <fieldset>
                    <div class="tableName toolbar">
                        <table class="display data_table2" >
                            <thead>
                                <tr>                                    
                                    <th><div class='th_wrapp'>Titulo</div></th> 
                                    <th><div class='th_wrapp'>Imagen</div></th> 
                                    <th><div class='th_wrapp'>Acciones</div></th> 

                                </tr>
                            </thead>
                            <tbody id="move">
                                <?php 
                                $i = 1;
                                foreach ($info as $d){ ?>
                                <tr class="odd gradeX" id="<?php echo $i.'-'.$d->id ; ?>"> 
                                    <td width="25%" class="center"><label><?php echo $d->titulo; ?></label></td>                                    
                                    <td width="25%" class="center"><div ><img class="cuadro_edicion_fotos"  src="<?php echo base_url()."uploads/slider/new/".$d->imagen?>" width="100" ></div></td> 
                                    <td width="25%" class="center">
                                        <?php echo anchor("cms/aliados/edit_slider/".$d->id, "Editar", "class='uibutton'"); ?> 
                                        <?php echo anchor("cms/aliados/delete_slider/".$d->id, "Eliminar", "class='uibutton  special'"); ?>                                        
                                    </td> 
                                </tr>
                                <?php $i++; }?>
                            </tbody>
                        </table>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>	
</div><!-- End content -->


<script type="text/javascript">
    function valida(){
        var imagen = $("#imagen").val();
        var titulo = $("#titulo").val();
        if (imagen == "" || titulo == ''){
            $('#info').focus();
            showError('Complete todos los campos.',3000);
        }else{
            $('#form').submit();   
        }
   }                   
    $(document).ready(function() {
        $('#fileUpload1').uploadify({
                    'uploader'          : '<?php echo site_url(); ?>'+'back/assets/components/uploadify/uploadify.swf',
                    'script'            : '<?php echo site_url(); ?>' + 'cms/aliados/upload_slider/',
                    'cancelImg'         : '<?php echo site_url(); ?>'+'back/assets/components/uploadify/cancel.png',
                    'auto'              : true,
                    'folder'            : '',
                    'queueSizeLimit'    : 3,
                    'multi'             : false,
                    'fileExt'           : '*.jpg;*.jpeg;*.png;*gif',
                    'auto'              : true,
                    'buttonText'        : 'Cargar imagen.',
                    'onComplete'  : function(event, queueId, fileObj, response, data) {
                var responseJson = $.parseJSON(response);
                if (responseJson.ok === true) {
                        $("#img1").attr('src','<?php echo base_url() ?>uploads/slider/'+responseJson.data.file_name);
                        $("#img1").hide(800).delay(2000).show(800);
                        $("#divimg1").html('<h2>Cargando imagen...</h2>')
                        $("#divimg1").show(850).delay(2000).hide(750);
                        $("#imagen").val(responseJson.data.file_name)
                } else {
                        showError('Problemas al carga imagen.',3000);                            
                }
            }
                });   
                
    $("#move").sortable({update: function() {
        var order = $(this).sortable("toArray");
        $.post("<?php echo cms_url('aliados/orden_slider');  ?>", {order: order}, function(datos){
            });
        }
    });
    
    });
</script>
 <script>
    <?php if (isset($not)){
            if ($not ==  1){?>
            showSuccess('Acción realizada correctamente.',3000);
            <?php } ?>
    <?php } ?>
</script>
