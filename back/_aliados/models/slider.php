<?php

class Slider extends DataMapper {

    public $model = 'slider';
    public $table = 'slider_articulos';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getSlider(){
        return $this->order_by('orden_id', 'ASC')->get();
    }
    
    public function getSliderById($id = ""){        
        return $this->get_by_id($id);
    }
    
    public function saveSlider($object = "") {
        $this->imagen = $object['imagen'];
        $this->titulo = $object['titulo'];
        $this->subtitulo = $object['subtitulo'];
        $this->orden_id = 0;

        return $this->save();
    }
    
    public function updateSlider($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function deleteSlider($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }
}
