<?php

class _Aliados extends Back_Controller {

    public function __construct() {
        parent::__construct();
        $this->_data['nombremodulo'] = "ALIANZAS";
        if (true != $this->have_admin_access()) {
            return redirect(cms_url('login'), 'refresh');
        }
    }

    // ---------------------------GENERALES ---------------------------------
    
    public function upload_slider(){
        $config = array(
            'allowed_types' => '*',
            'upload_path' => UPLOADSFOLDER.'slider/',
            'max_size' => 0,
            'encrypt_name' => true
        );
        $this->load->library('Upload', $config);
        if (!$this->upload->do_upload('Filedata')) {
            $data = array(
                'ok' => false,
                'data' => $this->upload->display_errors()
            );
            
        } else {
                $data = array(
                        'data' => $this->upload->data()
                );
                $resize['image_library'] = 'GD2';
                $resize['source_image']	= UPLOADSFOLDER."slider/".$data["data"]["file_name"];
                $resize['maintain_ratio'] = FALSE;
                $resize['width']	 = 1366;
                $resize['height']	= 589;
                $resize['new_image'] = UPLOADSFOLDER."slider/new/".$data["data"]["file_name"];

                $this->load->library('image_lib', $resize); 

                if ( ! $this->image_lib->resize())
                    {
                    $data = array(
                            'ok' => false,
                            'resize' => $this->image_lib->display_errors()
                        );    
                    }else{
                        $data = array(
                            'ok' => true,
                            'resize' => true,
                            'data' => $this->upload->data()
                        );
                    }
        }
        echo json_encode($data);
    }
    
    public function upload_aliados() {
        $config = array(
            'allowed_types' => '*',
            'upload_path' => UPLOADSFOLDER.'alianzas/',
            'max_size' => 0,
            'encrypt_name' => true
        );
        $this->load->library('Upload', $config);
        if (!$this->upload->do_upload('Filedata')) {
            $data = array(
                'ok' => false,
                'data' => $this->upload->display_errors()
            );
            
        } else {
                $data = array(
                        'data' => $this->upload->data()
                );
                $resize['image_library'] = 'GD2';
                $resize['source_image']	= UPLOADSFOLDER."alianzas/".$data["data"]["file_name"];
                $resize['maintain_ratio'] = FALSE;
                $resize['width']	 = 400;
                $resize['height']	= 300;
                $resize['new_image'] = UPLOADSFOLDER."alianzas/new/".$data["data"]["file_name"];

                $this->load->library('image_lib', $resize); 

                if ( ! $this->image_lib->resize())
                    {
                    $data = array(
                            'ok' => false,
                            'resize' => $this->image_lib->display_errors()
                        );    
                    }else{
                        $data = array(
                            'ok' => true,
                            'resize' => true,
                            'data' => $this->upload->data()
                        );
                    }
        }
        echo json_encode($data);
    }
    
    // ------------------------------SLIDER----------------------------------------

    public function slider($not = '') {        
        $this->_data['not'] = $not;
        $a = new Slider();
        $info = $a->getSlider();
        $this->_data["info"] = $info;
        return $this->build('slider');
    }    

    
    public function new_slider() {
        $datos = array(            
            'imagen' => $this->input->post('imagen'),
            'titulo' => $this->input->post('titulo'),
            'subtitulo' => $this->input->post('subtitulo'),
        );
        $not = FALSE;
        $a = new Slider();
        if ($datos['imagen'] != ''){  
            if ($a->saveSlider($datos)){
                $not = TRUE;
            }
        }
        return redirect('cms/aliados/slider/'.$not);        
    }

    public function delete_slider($id = ""){
        $a = new Slider();
        $not = FALSE;
        if ($a->deleteSlider($id)){
            $not = TRUE;
        };
        return redirect("cms/aliados/slider/".$not);
    }


    public function edit_slider($id = "",$not = ""){        
        $a = new Slider();
        $dat = $a->getSliderById($id);
        $this->_data["info"] = $dat;
        $this->_data["not"] = $not;
        return $this->build("slider_edit");
    }

        
    public function update_slider(){
        $a = new Slider();
        $datos = array(
            'id' => $this->input->post('id'),               
            'titulo' => $this->input->post('titulo'),                          
            'subtitulo' => $this->input->post('subtitulo'),                    
        );
        $update = $a->updateSlider($datos); 
        $not = FALSE;
        if($this->input->post('imagen') != ''){
            $datos = array(
            'imagen' => $this->input->post('imagen'),
            'id' => $this->input->post('id')
        );
            $update = $a->updateSlider($datos);
        }
        if ($update){
            $not = TRUE;
        }
        return redirect("cms/aliados/slider/".$not);
    }
    

    public function orden_slider(){
        $orden = $this->input->post('order');
        $cant = count($orden);
        $a = new Slider();
        $id = Array();
        $pos = Array();
        for($i = 0 ; $i < $cant ; $i++){
            $data = explode('-', $orden[$i]);
            $pos[$i] = $data[0];
            $id[$i] = $data[1];
        }        
        $ini = min($pos);         
        for($i = 0 ; $i < $cant ; $i++){
            $datos = array(
                'orden_id' => $ini,
                'id_art' => $id[$i]
            );
            $ini = $ini + 1 ;
        $a->updateSlider($datos); 
        }         
    }

    
    // ------------------------------ALIADOS----------------------------------------
    
    public function aliados($not = '') {        
        $this->_data['not'] = $not;
        $a = new Aliados();
        $info = $a->getAliados();
        $this->_data["info"] = $info;
        return $this->build('aliados');
    }      
    
    public function new_aliados() {
        $datos = array(            
            'imagen' => $this->input->post('imagen'),
            'titulo' => $this->input->post('titulo'),
            'texto' => $this->input->post('texto'),
        );
        $not = FALSE;
        $a = new Aliados();
        if ($datos['imagen'] != ''){  
            if ($a->saveAliados($datos)){
                $not = TRUE;
            }
        }
        return redirect('cms/aliados/aliados/'.$not);        
    }
    
    public function edit_aliados($id = "",$not = ""){        
        $a = new Aliados();
        $dat = $a->getAliadosById($id);
        $this->_data["info"] = $dat;
        $this->_data["not"] = $not;
        return $this->build("aliados_edit");
    }
        
    public function update_aliados(){
        $a = new Aliados();
        $datos = array(
            'id' => $this->input->post('id'),               
            'titulo' => $this->input->post('titulo'),                          
            'texto' => $this->input->post('texto'),                    
        );
        $update = $a->updateAliados($datos); 
        $not = FALSE;
        if($this->input->post('imagen') != ''){
            $datos = array(
            'imagen' => $this->input->post('imagen'),
            'id' => $this->input->post('id')
        );
            $update = $a->updateAliados($datos);
        }
        if ($update){
            $not = TRUE;
        }
        return redirect("cms/aliados/aliados/".$not);
    }
    
    public function delete_aliados($id = ""){
        $a = new Aliados();
        $not = FALSE;
        if ($a->deleteAliados($id)){
            $not = TRUE;
        };
        return redirect("cms/aliados/aliados/".$not);
    }
    
    public function orden_aliados(){
        $orden = $this->input->post('order');
        $cant = count($orden);
        $a = new Aliados();
        $id = Array();
        $pos = Array();
        for($i = 0 ; $i < $cant ; $i++){
            $data = explode('-', $orden[$i]);
            $pos[$i] = $data[0];
            $id[$i] = $data[1];
        }        
        $ini = min($pos);         
        for($i = 0 ; $i < $cant ; $i++){
            $datos = array(
                'orden_id' => $ini,
                'id' => $id[$i]
            );
            $ini = $ini + 1 ;
        $a->updateAliados($datos); 
        }         
    }
}
