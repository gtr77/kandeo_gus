<section class="preload">
  <div class="loading waves-button waves-effect waves-light">
    <div class="logo_load"><img src="<?php echo base_url() ?>back/assets/images/logo.png" alt="Kandeo"></div>
  </div>
</section>

<header class="animated fadeInDown">
 <section class="marco">
    <a href="<?php echo base_url() ?>cms/dashboard" class="logo animated fadeInLeft">
      <img src="<?php echo base_url() ?>back/assets/images/logo.png" alt="">
    </a> 
    <!--<div class="list_countre left">
      <a href="">Perú <i class="fa fa-sort-desc"></i></a>
      <ul>
        <li><a href="">Kandeo Perú</a></li>
        <li><a href="">Kandeo Mexico</a></li>
        <li><a href="">Kandeo Colombia</a></li>
      </ul>
    </div>--> 
    <nav>
      <ul>
          <li class="bottom_tip" data-tips="Inicio"><a href="<?php echo base_url() ?>cms/dashboard" class="animated flipInX" data-wow-delay="0.1s"><i class="fa fa-home" aria-hidden="true"></i></a></li>
          <li class="bottom_tip" data-tips="Pipeline"><a href="pipeline.php" class="animated flipInX" data-wow-delay="0.1s"><i class="fa fa-sellsy" aria-hidden="true"></i></a></li>
          <li class="bottom_tip" data-tips="Inversiones"><a href="prospect.php" class="animated flipInX" data-wow-delay="0.1s"><i class="fa fa-line-chart" aria-hidden="true"></i></a></li>                    
<!--           <li class="bottom_tip" data-tips="Task Manager"><a href="activities.php" class="animated flipInX ti-clipboard" data-wow-delay="0.1s"></a></li> -->
          <li class="bottom_tip" data-tips="fondos"><a href="fondos.php" class="animated flipInX square" data-wow-delay="0.1s"><i class="fa fa-money" aria-hidden="true"></i></a></li>
          <li class="bottom_tip" data-tips="Inversionistas"><a href="investers.php" class="animated flipInX" data-wow-delay="0.1s"><i class="fa fa-users" aria-hidden="true"></i></a></li>
          <li class="bottom_tip" data-tips="Actualizaciones"><a href="stream.php" class="animated flipInX" data-wow-delay="0.1s"><i class="fa fa-refresh" aria-hidden="true"></i></a></li>
      </ul>      
    </nav>     
    <div class="buscador left">
      <form action="" class="" role="form" enctype="multipart/form-data" method="post">                  
        <input type="text" name="shearch" class="search" maxlength="100" value="Search..." onBlur="if(this.value=='') this.value='Search...'" onFocus="if(this.value =='Search...' ) this.value=''">
        <button type="submit" class="btn_bucador"></button>
        <ul>
          <li>
            <a href="" title="">
            <div class="img_perfil">
              <img src="../back/assets/images/user/2.jpg">
            </div>
            <p>Miguel Armando</p>
            <p><span>Vendedor</span></p>
            </a>
          </li>
          <li>
            <a href="" title="">
            <div class="img_perfil">
              <img src="../back/assets/images/user/3.jpg">
            </div>
            <p>Sandra Robayo</p>
            <p><span>Gerente de Ventas</span></p>
            </a>
          </li>
          <li>
            <a href="" title="">
            <div class="img_perfil">
              <img src="http://martacarballo.com/wp-content/uploads/2009/05/logo-hello1.jpg?w=150">
            </div>
            <p>Name Company</p>
            <p><span>Company</span></p>
            </a>
          </li>
          <li>
            <a href="" title="">
            <div class="img_perfil">
              <img src="http://martacarballo.com/wp-content/uploads/2009/05/logo-hello1.jpg?w=150">
            </div>
            <p>Name Company</p>
            <p><span>Company</span></p>
            </a>
          </li>
          <li>
            <a href="" title="">
            <div class="img_perfil">
              <img src="http://martacarballo.com/wp-content/uploads/2009/05/logo-hello1.jpg?w=150">
            </div>
            <p>Name Company</p>
            <p><span>Company</span></p>
            </a>
          </li>
          <li>
            <a href="" title="">
            <div class="img_perfil">
              <img src="http://martacarballo.com/wp-content/uploads/2009/05/logo-hello1.jpg?w=150">
            </div>
            <p>Name Company</p>
            <p><span>Company</span></p>
            </a>
          </li>
          <li>
            <a href="" title="">
            <div class="img_perfil">
              <img src="http://martacarballo.com/wp-content/uploads/2009/05/logo-hello1.jpg?w=150">
            </div>
            <p>Name Company</p>
            <p><span>Company</span></p>
            </a>
          </li>
          <li>
            <a href="" title="">
            <div class="img_perfil">
              <img src="http://martacarballo.com/wp-content/uploads/2009/05/logo-hello1.jpg?w=150">
            </div>
            <p>Name Company</p>
            <p><span>Company</span></p>
            </a>
          </li>
        </ul>
      </form>     
        <div class="clear"></div>                
    </div>
    <div class="contro_user">
        <div class="icon_notif">
           <i class="animated">
             <span class="fadeInLeft">15</span>
           </i>
        </div>
        <div class="user_head">
           <a href="profile.php"></a>
           
		
           <h3>Leonardo Hernandez <span>Usuario</span></h3>
           <div class="img_perfil animated fadeInDown">
              <img src="<?php echo base_url() ?>back/assets/images/user/3.jpg" alt="">
            </div>            
            <ul class="option_profil_h">
              <li><a href="<?php echo base_url() ?>cms/users/index/<?php echo $this->session->userdata['user_id']; ?>">Perfil</a></li>
              <li><a href="<?php echo cms_url('login/logout') ?>">Salir</a></li>
            </ul>
        </div>
    </div>
    <nav class="ka_tools">
        <ul>
           <li class="bottom_tip" data-tips="Eliminar"><a href="dismissed_prospects.php" class="animated flipInX ti-trash" data-wow-delay="0.1s"></a></li>
          <li class="bottom_tip" data-tips="Configuración"><a href="<?php echo base_url() ?>cms/users/config" class="animated flipInX ti-settings" data-wow-delay="0.1s"></a></li>
        </ul>
    </nav> 
 </section>
    <div class="nav-resp left" data-effect="st-effect-7"><ul> <li></li> <li></li> <li></li> </ul></div>
</header>
<section class="over_nav"></section>

<!--Nofificaciones-->
<section class="bg_notificaciones"></section>
<section class="notificaciones">
   <div class="close_not"></div>
  <h2>notifications</h2>
  <ul class="list_notif">
    <li>
      <a href="" title="">
        <div class="large-3 medium-3 small-3 columns txt_center">
          <i class="ti-pencil-alt"></i> 
        </div>
        <div class="large-9 medium-9 small-9 columns">
          <p>Editing an activity <span class="fecha_not">10:00 a.m</span></p>
        </div>
         <div class="clear"></div>
      </a>
    </li>
    <li>
      <a href="" title="">
        <div class="large-3 medium-3 small-3 columns txt_center">
          <i class="ti-reload"></i> 
        </div>
        <div class="large-9 medium-9 small-9 columns">
          <p>Change of status of a "Prospect" <span class="fecha_not">2 day ago</span></p>
        </div>
         <div class="clear"></div>
      </a>
    </li>
    <li>
      <a href="" title="">
        <div class="large-3 medium-3 small-3 columns txt_center">
          <i class="ti-pencil-alt"></i> 
        </div>
        <div class="large-9 medium-9 small-9 columns">
          <p>Editing a Prospect <span class="fecha_not">1 day ago</span></p>
        </div>
         <div class="clear"></div>
      </a>
    </li>
    <li>
      <a href="" title="">
        <div class="large-3 medium-3 small-3 columns txt_center">
          <i class="ti-check-box"></i> 
        </div>
        <div class="large-9 medium-9 small-9 columns">
          <p>Activity completed successfully <span class="fecha_not">10:00 a.m</span></p>
        </div>
         <div class="clear"></div>
      </a>
    </li>
    <li>
      <a href="" title="">
        <div class="large-3 medium-3 small-3 columns txt_center">
          <i class="ti-flag"></i> 
        </div>
        <div class="large-9 medium-9 small-9 columns">
          <p>New activity associated with "Name prospect" <span class="fecha_not">12:00 a.m</span></p>
        </div>
         <div class="clear"></div>
      </a>
    </li>
  </ul>
</section>
<!--Fin Nofificaciones-->


<!---Modales Laterales---->
<section class="modalesLeft">
    <div class="modal_generado">
    
    </div>
    
  <!--Modal fondos-->
  
    <section class="modales_form modal_crear_fondo modal_small  ">
        <div class="large-12 medium-12 small-12 columns padding titulo_bright">
            <div class="titulo_sec">
                <h1>Crear Fondo</h1>
                <a href="javascript:void(0)" class="btn_gris close_inicial"></a>
                <div class="oganizaciones_h">
                    <section class="padding">
                        <form action="" class="elem_blancos form_new_user" id="formu_crear_fondo">
                            <section class="row">
                                <div class="small-12 meidum-12 large-12 columns">
                                    <input type="text" value="nombre" name="nombre" id="nombre" onblur="if(this.value=='') this.value='nombre'" onfocus="if(this.value =='nombre' ) this.value=''"> 
                                    <input type="text" value="pais" name="pais" id="pais" onblur="if(this.value=='') this.value='pais'" onfocus="if(this.value =='pais' ) this.value=''">
                                     <div class="small-6 meidum-6 large-6 columns">
                                      <input type="text" value="tamano_fondo" name="tamano_fondo" id="tamano_fondo" onblur="if(this.value=='') this.value='tamano_fondo'" onfocus="if(this.value =='tamano_fondo' ) this.value=''"> 
                                    </div>
                                    <div class="small-6 meidum-6 large-6 columns">
                                        <select name="monedas" data-placeholder="Tipo Moneda" tabindex="4" >
                                            <option>Tipo Moneda</option>
                                            <?php 
                                                foreach($monedas as $mon){
                                            ?>
                                            <option value="<?php echo $mon->id; ?>"><?php echo $mon->nombre; ?></option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <select name="sociedades"  id="sociedades"  data-placeholder="Sociedades Administradoras" class="chosen_sociedades" multiple tabindex="4" >
                                        
                                        <?php 
                                            foreach($sociedades as $so){
                                        ?>
                                        <option value="<?php echo $so->id; ?>"><?php echo $so->nombre; ?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                    <input type="text" value="invertido" name="invertido" id="invertido" onblur="if(this.value=='') this.value='invertido'" onfocus="if(this.value =='invertido' ) this.value=''">  
                                    <input type="text" value="comprometido" name="comprometido" id="comprometido" onblur="if(this.value=='') this.value='comprometido'" onfocus="if(this.value =='comprometido' ) this.value=''">  
                                    <input type="text" value="gastos" name="gastos" id="gastos" onblur="if(this.value=='') this.value='gastos'" onfocus="if(this.value =='gastos' ) this.value=''"> 
                                    <input type="text" value="gastos_proyectados" name="gastos_proyectados" id="gastos_proyectados" onblur="if(this.value=='') this.value='gastos_proyectados'" onfocus="if(this.value =='gastos_proyectados' ) this.value=''"> 
                                    <input type="text" value="disponible" name="disponible" id="disponible" onblur="if(this.value=='') this.value='disponible'" onfocus="if(this.value =='disponible' ) this.value=''">
                                    
                           
                                </div>
                            </section>
                            <div class="clear">
                            </div>
                            <div class="small-12 medium-12 large-12 columns">
                                <a type="submit" class="btnG right m_l_10" onclick="crear_fondo()">Guardar</a>
                            </div>
                            <div class="clear"></div>
                        </form>
                        <div class="clear"></div>
                    </section>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </section>
  <!--FINModal fondos-->
  <!--Modal New Prospect-->
  <section class="modales_form modal_prospect_new">
      <div class="large-12 medium-12 small-12 columns padding titulo_bright">
         <div class="titulo_sec">
           <h1>New Prospect</h1>
           <a href="javascript:void(0)" class="btn_gris close_inicial"></a>
         </div>
           <div class="clear"></div> 
      </div>
       <div class="clear"></div>
      <div class="oganizaciones_h">

        <div class="table_doc">
          <form action="" class="elem_blancos wow fadeInUp form_new_prospect">
            <section class="row">
              <div class="small-6 meidum-6 large-6 columns p_r_10">
                  <label>Company Name </label>
                  <input type="text" placeholder="">
              </div>
              <div class="small-6 meidum-6 large-6 columns p_l_10">
                  <label>Logo</label>
                  <div class="clearfix file">                      
                    <div class="button-group"> 
                      <a href="#" id="browse" class="btn_border_gris adjuntar"><i class="fa fa-cloud-upload"></i> Upload file</a>
                    </div>
                    <input type="file" id="test" class="file">
                    <input type="text" class="valorfile" value="Name file select..." disabled>
                  </div> 
              </div>
              <div class="small-12 meidum-12 large-12 columns">
                  <label>Description </label>
                  <textarea name="" id="" cols="30" rows="10"></textarea>
              </div>
              <div class="small-6 meidum-6 large-6 columns p_r_10">
                  <label>Sector</label>
                  <select name="" id="">
                    <option value="">Select option</option>
                    <option value="">Sector 1</option>
                    <option value="">Sector 2</option>
                    <option value="">Sector 3</option>
                    <option value="">Sector 4</option>                        
                  </select>
              </div>
              <div class="small-6 meidum-6 large-6 columns p_l_10">
                  <label>Date of inclusion</label>
                  <input type="text" placeholder="20/11/2015" disabled>
              </div>
              <div class="small-6 meidum-6 large-6 columns p_r_10">
                  <label>Country</label>
                  <select name="country">
                    <option>Afghanistan</option>
                    <option>Albania</option>
                    <option>Algeria</option>
                    <option>American Samoa</option>
                    <option>Andorra</option>
                    <option>Angola</option>
                    <option>Anguilla</option>
                    <option>Antarctica</option>
                    <option>Antigua and Barbuda</option>
                    <option>Argentina</option>
                    <option>Armenia</option>
                    <option>Aruba</option>
                    <option>Australia</option>
                    <option>Austria</option>
                    <option>Azerbaijan</option>
                    <option>Bahamas</option>
                    <option>Bahrain</option>
                    <option>Bangladesh</option>
                    <option>Barbados</option>
                    <option>Belarus</option>
                    <option>Belgium</option>
                    <option>Belize</option>
                    <option>Benin</option>
                    <option>Bermuda</option>
                    <option>Bhutan</option>
                    <option>Bolivia</option>
                    <option>Bosnia and Herzegovina</option>
                    <option>Botswana</option>
                    <option>Bouvet Island</option>
                    <option>Brazil</option>
                    <option>British Indian Ocean Territory</option>
                    <option>Brunei Darussalam</option>
                    <option>Bulgaria</option>
                    <option>Burkina Faso</option>
                    <option>Burundi</option>
                    <option>Cambodia</option>
                    <option>Cameroon</option>
                    <option>Canada</option>
                    <option>Cape Verde</option>
                    <option>Cayman Islands</option>
                    <option>Central African Republic</option>
                    <option>Chad</option>
                    <option>Chile</option>
                    <option>China</option>
                    <option>Christmas Island</option>
                    <option>Cocos Islands</option>
                    <option>Colombia</option>
                    <option>Comoros</option>
                    <option>Congo</option>
                    <option>Congo, Democratic Republic of the</option>
                    <option>Cook Islands</option>
                    <option>Costa Rica</option>
                    <option>Cote d'Ivoire</option>
                    <option>Croatia</option>
                    <option>Cuba</option>
                    <option>Cyprus</option>
                    <option>Czech Republic</option>
                    <option>Denmark</option>
                    <option>Djibouti</option>
                    <option>Dominica</option>
                    <option>Dominican Republic</option>
                    <option>Ecuador</option>
                    <option>Egypt</option>
                    <option>El Salvador</option>
                    <option>Equatorial Guinea</option>
                    <option>Eritrea</option>
                    <option>Estonia</option>
                    <option>Ethiopia</option>
                    <option>Falkland Islands</option>
                    <option>Faroe Islands</option>
                    <option>Fiji</option>
                    <option>Finland</option>
                    <option>France</option>
                    <option>French Guiana</option>
                    <option>French Polynesia</option>
                    <option>Gabon</option>
                    <option>Gambia</option>
                    <option>Georgia</option>
                    <option>Germany</option>
                    <option>Ghana</option>
                    <option>Gibraltar</option>
                    <option>Greece</option>
                    <option>Greenland</option>
                    <option>Grenada</option>
                    <option>Guadeloupe</option>
                    <option>Guam</option>
                    <option>Guatemala</option>
                    <option>Guinea</option>
                    <option>Guinea-Bissau</option>
                    <option>Guyana</option>
                    <option>Haiti</option>
                    <option>Heard Island and McDonald Islands</option>
                    <option>Honduras</option>
                    <option>Hong Kong</option>
                    <option>Hungary</option>
                    <option>Iceland</option>
                    <option>India</option>
                    <option>Indonesia</option>
                    <option>Iran</option>
                    <option>Iraq</option>
                    <option>Ireland</option>
                    <option>Israel</option>
                    <option>Italy</option>
                    <option>Jamaica</option>
                    <option>Japan</option>
                    <option>Jordan</option>
                    <option>Kazakhstan</option>
                    <option>Kenya</option>
                    <option>Kiribati</option>
                    <option>Kuwait</option>
                    <option>Kyrgyzstan</option>
                    <option>Laos</option>
                    <option>Latvia</option>
                    <option>Lebanon</option>
                    <option>Lesotho</option>
                    <option>Liberia</option>
                    <option>Libya</option>
                    <option>Liechtenstein</option>
                    <option>Lithuania</option>
                    <option>Luxembourg</option>
                    <option>Macao</option>
                    <option>Madagascar</option>
                    <option>Malawi</option>
                    <option>Malaysia</option>
                    <option>Maldives</option>
                    <option>Mali</option>
                    <option>Malta</option>
                    <option>Marshall Islands</option>
                    <option>Martinique</option>
                    <option>Mauritania</option>
                    <option>Mauritius</option>
                    <option>Mayotte</option>
                    <option>Mexico</option>
                    <option>Micronesia</option>
                    <option>Moldova</option>
                    <option>Monaco</option>
                    <option>Mongolia</option>
                    <option>Montenegro</option>
                    <option>Montserrat</option>
                    <option>Morocco</option>
                    <option>Mozambique</option>
                    <option>Myanmar</option>
                    <option>Namibia</option>
                    <option>Nauru</option>
                    <option>Nepal</option>
                    <option>Netherlands</option>
                    <option>Netherlands Antilles</option>
                    <option>New Caledonia</option>
                    <option>New Zealand</option>
                    <option>Nicaragua</option>
                    <option>Niger</option>
                    <option>Nigeria</option>
                    <option>Norfolk Island</option>
                    <option>North Korea</option>
                    <option>Norway</option>
                    <option>Oman</option>
                    <option>Pakistan</option>
                    <option>Palau</option>
                    <option>Palestinian Territory</option>
                    <option>Panama</option>
                    <option>Papua New Guinea</option>
                    <option>Paraguay</option>
                    <option>Peru</option>
                    <option>Philippines</option>
                    <option>Pitcairn</option>
                    <option>Poland</option>
                    <option>Portugal</option>
                    <option>Puerto Rico</option>
                    <option>Qatar</option>
                    <option>Romania</option>
                    <option>Russian Federation</option>
                    <option>Rwanda</option>
                    <option>Saint Helena</option>
                    <option>Saint Kitts and Nevis</option>
                    <option>Saint Lucia</option>
                    <option>Saint Pierre and Miquelon</option>
                    <option>Saint Vincent and the Grenadines</option>
                    <option>Samoa</option>
                    <option>San Marino</option>
                    <option>Sao Tome and Principe</option>
                    <option>Saudi Arabia</option>
                    <option>Senegal</option>
                    <option>Serbia</option>
                    <option>Seychelles</option>
                    <option>Sierra Leone</option>
                    <option>Singapore</option>
                    <option>Slovakia</option>
                    <option>Slovenia</option>
                    <option>Solomon Islands</option>
                    <option>Somalia</option>
                    <option>South Africa</option>
                    <option>South Georgia</option>
                    <option>South Korea</option>
                    <option>Spain</option>
                    <option>Sri Lanka</option>
                    <option>Sudan</option>
                    <option>Suriname</option>
                    <option>Svalbard and Jan Mayen</option>
                    <option>Swaziland</option>
                    <option>Sweden</option>
                    <option>Switzerland</option>
                    <option>Syrian Arab Republic</option>
                    <option>Taiwan</option>
                    <option>Tajikistan</option>
                    <option>Tanzania</option>
                    <option>Thailand</option>
                    <option>The Former Yugoslav Republic of Macedonia</option>
                    <option>Timor-Leste</option>
                    <option>Togo</option>
                    <option>Tokelau</option>
                    <option>Tonga</option>
                    <option>Trinidad and Tobago</option>
                    <option>Tunisia</option>
                    <option>Turkey</option>
                    <option>Turkmenistan</option>
                    <option>Tuvalu</option>
                    <option>Uganda</option>
                    <option>Ukraine</option>
                    <option>United Arab Emirates</option>
                    <option>United Kingdom</option>
                    <option>United States</option>
                    <option>United States Minor Outlying Islands</option>
                    <option>Uruguay</option>
                    <option>Uzbekistan</option>
                    <option>Vanuatu</option>
                    <option>Vatican City</option>
                    <option>Venezuela</option>
                    <option>Vietnam</option>
                    <option>Virgin Islands, British</option>
                    <option>Virgin Islands, U.S.</option>
                    <option>Wallis and Futuna</option>
                    <option>Western Sahara</option>
                    <option>Yemen</option>
                    <option>Zambia</option>
                    <option>Zimbabwe</option>
                  </select>
              </div>
              <div class="small-6 meidum-6 large-6 columns p_l_10">
                  <label>Fund</label>
                  <select name="" id="">
                    <option value="">Select option</option>
                    <option value="">Fund 1</option>
                    <option value="">Fund 2</option>
                    <option value="">Fund 3</option>
                    <option value="">Fund 4</option>                        
                  </select>
              </div>
              <!-- <div class="small-12 meidum-12 large-12 columns m_t_10 m_b_20">        
                <div class="confirm checkboxS">
                  <input type="checkbox" id="inversor" name="inversor">
                  <label for="inversor">Requires co-investor</a>
                  </label>
                </div>  
              </div>          -->           
              <div class="small-6 meidum-6 large-6 columns p_r_10">
                  <label>Share</label>
                  <input type="text" placeholder="%">
              </div>
              <div class="small-6 meidum-6 large-6 columns p_l_10">
                  <label>Investments amount</label>
                  <input type="text" placeholder="$">
              </div>
              <div class="small-6 meidum-6 large-6 columns p_r_10">
                  <label>Equity value </label>
                  <input type="text" placeholder="%">
              </div>

              <div class="small-6 meidum-6 large-6 columns p_l_10">
                  <label>Preferred stock </label>
                  <input type="text" placeholder="">
              </div>
              <div class="small-6 meidum-6 large-6 columns p_r_10">
                  <label>Debt</label>
                  <input type="text" placeholder="%">
              </div>
              <div class="small-6 meidum-6 large-6 columns p_l_10">
                  <label>Kandeo Investment</label>
                  <input type="text" placeholder="%">
              </div>
              <div class="small-12 meidum-12 large-12 columns">
                  <label>Business source</label>
                  <select name="" id="">
                    <option value="">Select option</option>
                    <option value="">Sector 1</option>
                    <option value="">Sector 2</option>
                    <option value="">Sector 3</option>
                    <option value="">Sector 4</option>                        
                  </select>
              </div>
                <div class="clear"></div>
              <div class="small-12 meidum-12 large-12 columns">
                  <label>Contact</label>
                  <select class="chosen" multiple="true" data-placeholder="Select users...">
                      <option value=""></option>
                      <option>Juan Camilo</option>
                      <option>Mariana Lopez</option>
                      <option>Mayerly Giraldo</option>
                      <option>Leonard Martinez</option>                        
                      <option>Carlos Andres</option>
                      <option>Marcela Cangara</option>
                      <option>Lucas Alberto</option>
                      <option>Monica Sofia</option>
                      <option>Angelica Jaramillo</option>
                      <option>Alex Torres</option>
                  </select>                        
              </div>
              <div class="small-12 meidum-12 large-12 columns">
                  <section class="bg_new_contact">  
                    <div class="panelnew_User">
                      <h3>New contact <i class="ti-close"></i></h3>
                      <div class="small-4 meidum-4 large-4 columns p_r_5">
                        <label>Name</label>
                        <input type="text" placeholder="">
                      </div>
                      <div class="small-4 meidum-4 large-4 columns p_l_5 p_r_5">
                        <label>Job</label>
                        <input type="text" placeholder="">
                      </div>
                      <div class="small-4 meidum-4 large-4 columns p_l_5">
                        <label>Email</label>
                        <input type="text" placeholder="">
                      </div>
                      <div class="small-4 meidum-4 large-4 columns p_r_5">
                        <label>Skype</label>
                        <input type="text" placeholder="">
                      </div>
                      <div class="small-4 meidum-4 large-4 columns p_l_5 p_r_5">
                        <label>Phone </label>
                        <input type="number" placeholder="">
                      </div>
                      <div class="small-4 meidum-4 large-4 columns p_l_5">
                        <label>Cellphone</label>
                        <input type="number" placeholder="">
                      </div>
                      <div class="small-4 meidum-4 large-4 columns p_r_5">
                        <label>Extension</label>
                        <input type="text" placeholder="">
                      </div>
                      <div class="small-4 meidum-4 large-4 columns p_l_5 p_r_5">
                        <label>Address </label>
                        <input type="text" placeholder="">
                      </div>
                      <div class="small-4 meidum-4 large-4 columns p_l_5">
                        <label>Birthday</label>
                        <input type="text" class="dateCalendar fecha" placeholder=""></input>
                      </div>
                      <div class="small-12 meidum-12 large-12 columns">
                        <a href="" class="btnG right">Add contact</a>
                      </div>
                    </div>
                     <a href="javascript:void(0)" class="link2 right m_b_20"><i class="fa fa-plus"></i> Create new contact</a>
                  </section>                            
              </div>
               
                <div class="clear"></div>    

              <div class="small-12 meidum-12 large-12 columns">
                  <button type="submit" class="btnG right m_l_10">Save Prospect</button>
                  <button type="submit" class="btn_border right m_l_10">Cancel</button>
              </div>
             
            </section>
            
              <div class="clear"></div>    
          </form>
            <div class="clear"></div>    
        </div>
      </div>
  </section>   
  <!--Fin Modal New Prospect-->

  <!--Modal New Activity-->
  <section class="modales_form modal_activity_new">
      <div class="large-12 medium-12 small-12 columns padding titulo_bright">
         <div class="titulo_sec">
           <h1>New Activity</h1>
           <a href="javascript:void(0)" class="btn_gris close_inicial"></a>
         </div>
           <div class="clear"></div> 
      </div>
       <div class="clear"></div>

      <div class="oganizaciones_h">
        <div class="table_doc">
          <form action="" class="elem_blancos wow fadeInUp form_new_user">
            <section class="row">
              <div class="ListACTIVITYS">
                <div class="radiobtnS small-4 meidum-4 large-4 columns">
                  <input type="radio" name="typeActivity" id="activdad1">
                  <label for="activdad1">Pre-Board
                  </label>
                </div>    
                <div class="radiobtnS small-4 meidum-4 large-4 columns">
                  <input type="radio" name="typeActivity" id="activdad2">
                  <label for="activdad2">Board
                  </label>
                </div>              
                <div class="radiobtnS small-4 meidum-4 large-4 columns">
                  <input type="radio" name="typeActivity" id="activdad3">
                  <label for="activdad3">Committee
                  </label>
                </div> 
                <div class="radiobtnS small-4 meidum-4 large-4 columns">
                  <input type="radio" name="typeActivity" id="activdad11">
                  <label for="activdad11">Quarterly Report Data
                  </label>
                </div>    
                <div class="radiobtnS small-4 meidum-4 large-4 columns">
                  <input type="radio" name="typeActivity" id="activdad4">
                  <label for="activdad4">Quarterly Report 
                  </label>
                </div>              
                <div class="radiobtnS small-4 meidum-4 large-4 columns">
                  <input type="radio" name="typeActivity" id="activdad5">
                  <label for="activdad5">Operational Call 
                  </label>
                </div>  
                <div class="radiobtnS small-4 meidum-4 large-4 columns">
                  <input type="radio" name="typeActivity" id="activdad6">
                  <label for="activdad6">Balance Score Card  
                  </label>
                </div>    
                <div class="radiobtnS small-4 meidum-4 large-4 columns">
                  <input type="radio" name="typeActivity" id="activdad7">
                  <label for="activdad7">Advisory Committee  
                  </label>
                </div>              
                <div class="radiobtnS small-4 meidum-4 large-4 columns">
                  <input type="radio" name="typeActivity" id="activdad8">
                  <label for="activdad8">Invest. Committee  
                  </label>
                </div> 
                <div class="radiobtnS small-4 meidum-4 large-4 columns">
                  <input type="radio" name="typeActivity" id="activdad9">
                  <label for="activdad9">Executive Committee 
                  </label>
                </div>              
                <div class="radiobtnS small-4 meidum-4 large-4 columns">
                  <input type="radio" name="taypeActivity" id="activdad10">
                  <label for="activdad10">Valuations Update
                  </label>
                </div>   
              </div>

              <div class="small-6 meidum-6 large-6 columns p_r_10">
                  <label>Activity name</label>
                  <input type="text" placeholder="">
              </div>
              <div class="small-6 meidum-6 large-6 columns p_l_10">
                  <label>Due Date </label>
                  <input type="text" class="dateCalendar fecha" placeholder=""></input>
              </div>
              <div class="small-12 meidum-12 large-12 columns">
                  <label>Company</label>
                  <input type="text" placeholder="">
              </div>
              <div class="small-12 meidum-12 large-12 columns">
                  <label>In charge </label>
                  <select class="chosen" multiple="true" data-placeholder="Select users...">
                      <option value=""></option>
                      <option>Juan Camilo</option>
                      <option>Mariana Lopez</option>
                      <option>Mayerly Giraldo</option>
                      <option>Leonard Martinez</option>                        
                      <option>Carlos Andres</option>
                      <option>Marcela Cangara</option>
                      <option>Lucas Alberto</option>
                      <option>Monica Sofia</option>
                      <option>Angelica Jaramillo</option>
                      <option>Alex Torres</option>
                  </select> 
              </div>
              <div class="small-12 meidum-12 large-12 columns">
                  <label>High Priority</label>    
                  <div class="swith_pago">
                    <div class="switch-wrapper">
                      <input type="checkbox" value="1" checked>
                    </div>
                  </div>
              </div>
              <div class="small-12 meidum-12 large-12 columns">      
                <div class="total_table m_b_10 m_t_20">
                  <p>Comments</p>
                  <div class="large-12 medium-12 small-12 columns">
                     <textarea id="ccomment" name="comment" cols="22"></textarea>
                  </div>
                    <div class="clear"></div>
                </div>
              </div>
              <div class="small-12 meidum-12 large-12 columns">
                <label>Attachments</label>
                <div class="clearfix file">                      
                  <div class="button-group"> 
                    <a href="#" id="browse" class="btn_border_gris adjuntar"><i class="fa fa-cloud-upload"></i> Attachments file</a>
                  </div>
                  <input type="file" id="test" class="file">
                  <input type="text" class="valorfile" value="Name file select..." disabled>
                </div> 
              </div>

              <div class="clear"></div>    

              <div class="small-12 meidum-12 large-12 columns">
                  <button type="submit" class="btnG right m_l_10">Save</button>
                  <button type="submit" class="btn_border right m_l_10">Cancel</button>
              </div>

            </section>
              <div class="clear"></div>    
          </form>
            <div class="clear"></div>    
        </div>

      </div>
  </section>   
  <!--Fin Modal New Activity-->

  <!--Modal Stream-->
  <section class="modales_form modal_stream modal_small">
      <div class="large-12 medium-12 small-12 columns padding titulo_bright">
         <div class="titulo_sec">
           <h1>Activity Stream</h1>
           <a href="javascript:void(0)" class="btn_gris close_inicial"></a>
         </div>
           <div class="clear"></div> 
      </div>
       <div class="clear"></div>

      <div class="oganizaciones_h">
        <ul class="listStream">
          <li>
            <a href="" title="">
              <div class="img_perfil">
                <img src="../back/assets/images/user/2.jpg">
              </div>
              <p>New prospect created</p>
              <p><span>By Lisandro Acosta - Update: <i>2 day ago</i></span></p>
            </a>
          </li>
          <li>
            <a href="" title="">
              <div class="img_perfil">
                <img src="../back/assets/images/user/4.jpg">
              </div>
              <p>New prospect created</p>
              <p><span>By Maria Vergara - Update: <i>2 day ago</i></span></p>
            </a>
          </li>
          <li>
            <a href="" title="">
              <div class="img_perfil">
                <img src="../back/assets/images/user/2.jpg">
              </div>
              <p>New prospect created</p>
              <p><span>By Lisandro Acosta - Update: <i>2 day ago</i></span></p>
            </a>
          </li>
          <li>
            <a href="" title="">
              <div class="img_perfil">
                <img src="../back/assets/images/user/4.jpg">
              </div>
              <p>New prospect created</p>
              <p><span>By Maria Vergara - Update: <i>2 day ago</i></span></p>
            </a>
          </li>
          <li>
            <a href="" title="">
              <div class="img_perfil">
                <img src="../back/assets/images/user/2.jpg">
              </div>
              <p>New prospect created</p>
              <p><span>By Lisandro Acosta - Update: <i>2 day ago</i></span></p>
            </a>
          </li>
          <li>
            <a href="" title="">
              <div class="img_perfil">
                <img src="../back/assets/images/user/4.jpg">
              </div>
              <p>New prospect created</p>
              <p><span>By Maria Vergara - Update: <i>2 day ago</i></span></p>
            </a>
          </li>
        </ul>
        
        <div class="txt_center padd_all">
          <a href="clientes.php" class="btn_border inline">See all</a>
        </div>
      </div>
  </section>   
  <!--Fin Modal Stream-->

  <!--Modal Users news-->
  <section class="modales_form modal_users modal_small">
      <div class="large-12 medium-12 small-12 columns padding titulo_bright">
         <div class="titulo_sec">
           <h1>Nuevo Usuario</h1>
           <a href="javascript:void(0)" class="btn_gris close_inicial"></a>
         </div>
      </div>

      <div class="oganizaciones_h">
        <section class="padding">
        <form action="" class="elem_blancos form_new_user " id="formu_user_create">
          <section class="row">
            <div class="small-12 meidum-12 large-12 columns">
                <input type="text" name="nombre" id="nombre" value="Name" onblur="if(this.value=='') this.value='Name'" onfocus="if(this.value =='Name' ) this.value=''">
            </div>
            <div class="small-12 meidum-12 large-12 columns">
                <input type="text" name="lastname" id="lastname" value="Last name" onblur="if(this.value=='') this.value='Last name'" onfocus="if(this.value =='Last name' ) this.value=''">
            </div>
            <div class="small-12 meidum-12 large-12 columns cargar_img"> 
              <label>Photo</label>
              <div class="form-item">        
                <div class="file-preview">
                  <a href="#" title="Clic para insertar imagen" class="file-select">
                    <span><i class="ti-image"></i></span>
                  </a>
                  <figure><img src="<?php echo base_url() ?>back/assets/images/icons/cargar_img.png"/></figure>
                </div>
                <input name="file" name="file" id="file" type="file" class="file2 file-preview" />
              </div>
            </div>
            <div class="small-12 meidum-12 large-12 columns">
                <input type="text" name="documento" id="documento" value="Document" onblur="if(this.value=='') this.value='Document'" onfocus="if(this.value =='Document' ) this.value=''">
            </div>
            <div class="small-12 meidum-12 large-12 columns">
                <input type="text" name="email" id="email" value="Email" onblur="if(this.value=='') this.value='Email'" onfocus="if(this.value =='Email' ) this.value=''">
            </div>
            <div class="small-12 meidum-12 large-12 columns">
                <input type="text" name="phone" id="phone" value="Phone" onblur="if(this.value=='') this.value='Phone'" onfocus="if(this.value =='Phone' ) this.value=''">
            </div>
            <div class="small-12 meidum-12 large-12 columns">
                <input type="text" id="cell" name="cell" value="Cell phone" onblur="if(this.value=='') this.value='Cell phone'" onfocus="if(this.value =='Cell phone' ) this.value=''">
            </div>
            <div class="small-12 meidum-12 large-12 columns">
                <input type="text" value="Password" name="pass" id="pass" onblur="if(this.value=='') this.value='Password'" onfocus="if(this.value =='Password' ) this.value=''">
            </div>
            <div class="small-12 meidum-12 large-12 columns">
                <input type="text" name="direccion" id="direccion" value="Address" onblur="if(this.value=='') this.value='Address'" onfocus="if(this.value =='Address' ) this.value=''">
            </div>
            <div class="small-12 meidum-12 large-12 columns">
                <select name="" id="tipouser" name="tipouser">
                  <option value="">Tipos de usuario</option>
                  <option value="">Super usuario</option>
                  <option value="">Administrado</option>
                  <option value="">Administrado 2</option>
                                       
                </select>
            </div>
          </section>
            <div class="clear"></div>    
          <div class="small-12 medium-12 large-12 columns">
              <a type="submit" class="btnG right m_l_10" onclick="guardar_user()">Guardar</a>
              <a type="submit" class="btn_border right m_l_10">Cancelar</a>
          </div>
            <div class="clear"></div>    
        </form>
         <div class="clear"></div> 
        </section>
      </div>
      <div class="clear"></div>

  </section>   
  <!--Fin Modal Users news-->


  <!--Modal News Funds-->
  <section class="modales_form modal_funds modal_small">
      <div class="large-12 medium-12 small-12 columns padding titulo_bright">
         <div class="titulo_sec">
           <h1>New Fund</h1>
           <a href="javascript:void(0)" class="btn_gris close_inicial"></a>
         </div>
      </div>

      <div class="oganizaciones_h">
        <section class="padding">
        <form action="" class="elem_blancos form_new_user">
          <section class="row">
            <div class="small-12 meidum-12 large-12 columns">
                <input type="text" value="Name" onblur="if(this.value=='') this.value='Name'" onfocus="if(this.value =='Name' ) this.value=''">
            </div>
          </section>
            <div class="clear"></div>    
          <div class="small-12 medium-12 large-12 columns">
              <button type="submit" class="btnG right m_l_10">Save</button>
          </div>
            <div class="clear"></div>    
        </form>
         <div class="clear"></div> 
        </section>
      </div>
      <div class="clear"></div>

  </section>   
  <!--Fin Modal News Funds-->


  <!--Modal edit moneda-->
  <section class="modales_form modal_moneda modal_small">
      <div class="large-12 medium-12 small-12 columns padding titulo_bright">
         <div class="titulo_sec">
           <h1>Moneda</h1>
           <a href="javascript:void(0)" class="btn_gris close_inicial"></a>
         </div>
      </div>

      <div class="oganizaciones_h">
       
        <form action="" class="elem_blancos form_new_moneda" id="formu_paises_edit">
          <section class="row">
            <div class="small-12 meidum-12 large-12 columns">
                
                <div class="caja-moneda">
                
                    <div class="item">
                        <h5>Tipos de Moneda</h5>   <a href="javascript:void(0)" class="btn_border right new_element">Nueva Moneda</a>
                                <div class="clear"></div>
                           <ul class="tipos">
                               <div class="mon"><div><input type="text" class="name_money" placeholder="Estados Unidos" disabled></div><div><input class="price_in" type="text" placeholder="US" disabled></div><div class="editar ti-pencil-alt"></div> <div class="ti-trash delete"></div></div>
                               <div class="mon"><div><input type="text" class="name_money" placeholder="Estados Unidos" disabled></div><div><input class="price_in" type="text" placeholder="US" disabled></div><div class="editar ti-pencil-alt"></div> <div class="ti-trash delete"></div></div>
                               <div class="mon"><div><input type="text" class="name_money" placeholder="Estados Unidos" disabled></div><div><input class="price_in" type="text" placeholder="US" disabled></div><div class="editar ti-pencil-alt"></div> <div class="ti-trash delete"></div></div>
                               <div class="mon"><div><input type="text" class="name_money" placeholder="Estados Unidos" disabled></div><div><input class="price_in" type="text" placeholder="US" disabled></div><div class="editar ti-pencil-alt"></div> <div class="ti-trash delete"></div></div>
                               <div class="mon"><div><input type="text" class="name_money" placeholder="Estados Unidos" disabled></div><div><input class="price_in" type="text" placeholder="US" disabled></div><div class="editar ti-pencil-alt"></div> <div class="ti-trash delete"></div></div>
                               <div class="mon"><div><input type="text" class="name_money" placeholder="Estados Unidos" disabled></div><div><input class="price_in" type="text" placeholder="US" disabled></div><div class="editar ti-pencil-alt"></div> <div class="ti-trash delete"></div></div>
                               <div class="mon"><div><input type="text" class="name_money" placeholder="Estados Unidos" disabled></div><div><input class="price_in" type="text" placeholder="US" disabled></div><div class="editar ti-pencil-alt"></div> <div class="ti-trash delete"></div></div>
                               <div class="mon"><div><input type="text" class="name_money" placeholder="Estados Unidos" disabled></div><div><input class="price_in" type="text" placeholder="US" disabled></div><div class="editar ti-pencil-alt"></div> <div class="ti-trash delete"></div></div>                          
                           </ul>
                        
                    </div>
                      
                    <div class="item">
                        <h5>Abreviaciones</h5>  
                        <div class="clear"></div>  
                        <div class="tipos">
                            <div class="mon"><div>Miles</div><div>K</div><div class="editar ti-pencil-alt"></div> <div class="ti-trash delete"></div></div>
                         
                            <div class="mon"><div>Millones</div><div>M</div><div class="editar ti-pencil-alt"></div> <div class="ti-trash delete"></div></div>
                            
                            <div class="mon"><div>Miles de millones</div><div>mm</div><div class="editar ti-pencil-alt"></div> <div class="ti-trash delete"></div></div>
                            
                            <div class="mon"><div>Billones</div><div>b</div><div class="editar ti-pencil-alt"></div> <div class="ti-trash delete"></div></div>
                            
                        </div>
                        <div class="clear"></div>
                    </div>
                    
                
                </div>
              </div>
            </section>
       
         
            <div class="clear"></div>    
          <div class="small-12 medium-12 large-12 columns">
              <a type="submit" class="btnG right m_l_10" onclick="editar_pais()">Guardar</a>
          </div>
            <div class="clear"></div>    
        </form>
         <div class="clear"></div> 
           </div> 
    </section>
 
  <!--Fin Modal edit moneda-->




  <!--Modal edit paises-->
  <section class="modales_form modal_paises modal_small">
      <div class="large-12 medium-12 small-12 columns padding titulo_bright">
         <div class="titulo_sec">
           <h1>Editar País</h1>
           <a href="javascript:void(0)" class="btn_gris close_inicial"></a>
         </div>
      </div>

      <div class="oganizaciones_h">
        <section class="padding">
        <form action="" class="elem_blancos form_new_user" id="formu_paises_edit">
          <section class="row">
            <div class="small-12 meidum-12 large-12 columns">
                <input type="text" value="Nombre" name="nombre" name="nombre" onblur="if(this.value=='') this.value='Nombre'" onfocus="if(this.value =='Nombre' ) this.value=''">
                
                  <section class="cargar_img">
                    <div class="form-item">        
                      <div class="file-preview">
                        <a href="#" title="Clic para insertar imagen" class="file-select">
                          <span><i class="ti-image"></i></span>
                        </a>
                        <figure><img src="<?php echo base_url() ?>/back/assets/images/icons/cargar_img.png"/></figure>
                      </div>
                      <input name="file" type="file" name="foto_pais" class="file2 file-preview" />
                    </div>  
                  </section>
            </div>
          </section>
            <div class="clear"></div>    
          <div class="small-12 medium-12 large-12 columns">
              <a type="submit" class="btnG right m_l_10" onclick="editar_pais()">Guardar</a>
          </div>
            <div class="clear"></div>    
        </form>
         <div class="clear"></div> 
        </section>
      </div>
      <div class="clear"></div>

  </section>   
  <!--Fin Modal edit paises-->

<!--Modal crear paises-->
  <section class="modales_form modal_crear_pais modal_small">
      <div class="large-12 medium-12 small-12 columns padding titulo_bright">
         <div class="titulo_sec">
           <h1>Nuevo País</h1>
           <a href="javascript:void(0)" class="btn_gris close_inicial"></a>
         </div>
      </div>

      <div class="oganizaciones_h">
        <section class="padding">
        <form action="" class="elem_blancos form_new_pais" id="formu_paises" enctype="multipart/form-data">
          <section class="row">
            <div class="small-12 meidum-12 large-12 columns">
                <input type="text" value="Name" name="nombre" id="nombre" onblur="if(this.value=='') this.value='Name'" onfocus="if(this.value =='Name' ) this.value=''">
                
                <section class="cargar_img">
                    <div class="form-item">        
                      <div class="file-preview">
                        <a href="#" title="Clic para insertar imagen" class="file-select">
                          <span><i class="ti-image"></i></span>
                        </a>
                        <figure><img src="<?php echo base_url() ?>/back/assets/images/icons/cargar_img.png"/></figure>
                      </div>
                      <input name="file" type="file" name="foto_pais" class="file2 file-preview" />
                    </div>  
                  </section>
            </div>
          </section>
            <div class="clear"></div>    
          <div class="small-12 medium-12 large-12 columns">
              <a type="submit" class="btnG right m_l_10" onclick="add_pais()">Guardar</a>
          </div>
            <div class="clear"></div>    
        </form>
         <div class="clear"></div> 
        </section>
      </div>
      <div class="clear"></div>

  </section>   
  <!--Fin Modal crear paises-->

<!--Modal crear sector-->
  <section class="modales_form modal_crear_sector modal_small">
      <div class="large-12 medium-12 small-12 columns padding titulo_bright">
         <div class="titulo_sec">
           <h1>Nuevo Sector</h1>
           <a href="javascript:void(0)" class="btn_gris close_inicial"></a>
         </div>
      </div>

      <div class="oganizaciones_h">
        <section class="padding">
        <form action="" class="elem_blancos form_new_pais" id="formu_sectores" enctype="multipart/form-data">
          <section class="row">
            <div class="small-12 meidum-12 large-12 columns">
                <input type="text" value="Name" name="nombre" id="nombre" onblur="if(this.value=='') this.value='Name'" onfocus="if(this.value =='Name' ) this.value=''">
            
            </div>
          </section>
            <div class="clear"></div>    
          <div class="small-12 medium-12 large-12 columns">
              <a type="submit" class="btnG right m_l_10" onclick="add_sector()">Guardar</a>
          </div>
            <div class="clear"></div>    
        </form>
         <div class="clear"></div> 
        </section>
      </div>
      <div class="clear"></div>

  </section>   
  <!--Fin Modal crear sector-->


  <!--Modal New Teaser-->
  <section class="modales_form modal_teaser">
      <div class="large-12 medium-12 small-12 columns padding titulo_bright">
         <div class="titulo_sec">
           <h1>New Teaser</h1>
           <a href="javascript:void(0)" class="btn_gris close_inicial"></a>
         </div>
           <div class="clear"></div> 
      </div>
       <div class="clear"></div>

      <div class="oganizaciones_h">
        <div class="table_doc">
          <form action="" class="elem_blancos wow fadeInUp form_new_user">
            <section class="row">
              <div class="small-12 meidum-12 large-12 columns">
                  <label>Title one</label>
                  <input type="text" placeholder="Overview">
              </div>
              <div class="small-12 meidum-12 large-12 columns">
                  <label>Subtitle and text one</label>
                  <input type="text" placeholder="Business Background">
                  <textarea name="" id="" cols="30" rows="10" placeholder="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur quidem omnis corporis ea, hic maxime laudantium neque expedita fugit consequatur delectus atque nesciunt in fugiat nihil tempora, iusto natus illum."></textarea>
              </div>
               <div class="clear"></div>
                <div class="lineap padding"></div>
               <div class="clear"></div>  
              <div class="small-12 meidum-12 large-12 columns">
                  <label>Subtitle and text two</label>
                  <input type="text" placeholder="Business Highlights">
                  <textarea name="" id="" cols="30" rows="10" placeholder="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur quidem omnis corporis ea, hic maxime laudantium neque expedita fugit consequatur delectus atque nesciunt in fugiat nihil tempora, iusto natus illum."></textarea>
              </div>
               <div class="clear"></div>
                <div class="lineap padding"></div>
               <div class="clear"></div>  
              <div class="small-12 meidum-12 large-12 columns">
                  <label>Title two</label>
                  <input type="text" placeholder="Opportunity">
                  <textarea name="" id="" cols="30" rows="10" placeholder="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur quidem omnis corporis ea, hic maxime laudantium neque expedita fugit consequatur delectus atque nesciunt in fugiat nihil tempora, iusto natus illum."></textarea>
              </div>
               <div class="clear"></div>
                <div class="lineap padding"></div>
               <div class="clear"></div>  
              <div class="small-12 meidum-12 large-12 columns">
                  <label>Title three</label>
                  <input type="text" placeholder="Risk and Mitigators">
                  <textarea name="" id="" cols="30" rows="10" placeholder="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur quidem omnis corporis ea, hic maxime laudantium neque expedita fugit consequatur delectus atque nesciunt in fugiat nihil tempora, iusto natus illum."></textarea>
              </div>
               <div class="clear"></div>
                <div class="lineap padding"></div>
               <div class="clear"></div>  
              <div class="small-12 meidum-12 large-12 columns">
                  <label>Title four</label> 
                  <input type="text" placeholder="Revenues">
                  <section class="cargar_img">
                    <div class="form-item">        
                      <div class="file-preview">
                        <a href="#" title="Clic para insertar imagen" class="file-select">
                          <span><i class="ti-image"></i></span>
                        </a>
                        <figure><img src="../back/assets/images/icons/cargar_img.png"/></figure>
                      </div>
                      <input name="file" type="file" class="file2 file-preview" />
                    </div>  
                  </section>
              </div>
               <div class="clear"></div>
                <div class="lineap padding"></div>
               <div class="clear"></div>  

              <div class="clear"></div>    

              <div class="small-12 meidum-12 large-12 columns">
                  <button type="submit" class="btnG right m_l_10">Save</button>
                  <button type="submit" class="btn_border right m_l_10">Cancel</button>
              </div>

            </section>
              <div class="clear"></div>    
          </form>
            <div class="clear"></div>    
        </div>

      </div>
  </section>   
  <!--Fin Modal New Teaser-->

</section>
<div class="mask_modalForm"></div>
<!---Fin Modales Laterales---->


<!--Pop up-->
<div id="new_folder" class="popup_block popup_small mfp-with-anim mfp-hide">
  <section class="elem_blancos">
    <div class="head">
      <h1>New folder</h1>
    </div>
    <form action="" class="p_b_20">
       <div class="small-12 meidum-12 large-12 columns padding">
            <label>Name folder</label>
            <input type="text" placeholder="">
       </div>
       <div class="small-12 meidum-12 large-12 columns padding">
         <button type="submit" class="btn_border right">Save</button>
       </div>
        <div class="clear"></div>
    </form>
  </section>
   <div class="clear"></div>
</div>

<div id="new_document" class="popup_block popup_small mfp-with-anim mfp-hide">
  <section class="elem_blancos">
    <div class="head">
      <h1>New document</h1>
    </div>
    <form action="" class="p_b_20">
       <div class="small-12 meidum-12 large-12 columns padding">
            <label>Folder</label>
            <input type="text" placeholder="Lorem ipsum dolor" disabled>
       </div>
       <div class="small-12 meidum-12 large-12 columns padding">
            <label>Name new document</label>
            <input type="text" placeholder="">
       </div>
       <div class="small-12 meidum-12 large-12 columns padding">
            <label>Upload file</label>
            <div class="clearfix file">                      
              <div class="button-group"> 
                <a href="#" id="browse" class="btn_border_gris adjuntar"><i class="fa fa-cloud-upload"></i> Upload file</a>
              </div>
              <input type="file" id="test" class="file">
              <input type="text" class="valorfile" value="Name file select..." disabled>
            </div> 
       </div>
       <div class="small-12 meidum-12 large-12 columns padding">
         <button type="submit" class="btn_border right">Save</button>
       </div>
        <div class="clear"></div>
    </form>
  </section>
   <div class="clear"></div>
</div>
<!--Fin Pop up-->


<!---CALENDAR-->
<div id="root-picker-outlet"></div>

<?php
function menu_configuracion(){
    
    
    $conten ='         <section class="titulo">
             <div class="large-4 medium-4 small-4 columns">
               <h1 class="left">Configuración</h1>               
             </div>
             <div class="subnav_compa large-4 medium-4 small-4 columns">
                <nav>
                    <!--Items tab-->
                    <ul class=" item_detalle">
                        <li><a href="'.base_url().'cms/users/config" class="bottom_tip" data-tips="Usuarios"><span class="ti-user"></span></a></li>
                        <li><a href="'.base_url().'cms/permisos" class="bottom_tip" data-tips="Permisos"><span class="ti-lock"></span></a></li>
                        <li><a href="'.base_url().'cms/fondos" class="bottom_tip" data-tips="Fondos"><span class="ti-flag-alt-2"></span></a></li>
                        
                        <li><a href="'.base_url().'cms/paises/" class=" bottom_tip" data-tips="Paises / Sectores"><span class="ti-agenda"></span></a></li> 
                        
                          <li><a href="#" class=" bottom_tip new_moneda" data-tips="Tipo de moneda"><span class="fa fa-money"></span></a></li> 
                          
                          <li><a href="'.base_url().'cms/sociedades" class=" bottom_tip " data-tips="Sociedades Administradoras"><span class="fa fa-briefcase"></span></a></li> 
                    <!--Fin Items tab-->
                </nav>
              </div>
              <div class="large-4 medium-4 small-4 columns">
                <a href="javascript:void(0)" class="btn_blanco right m_l_10 new_activity waves-button waves-effect waves-light"><i class="fa fa-sticky-note-o" aria-hidden="true"></i>Nueva Tarea</a>          
                <a href="javascript:void(0)" class="btn_blanco right m_l_10 new_prospect waves-button waves-effect waves-light"><i class="fa fa-building" aria-hidden="true"></i>Nueva Empresa</a>           
                  <div class="clear"></div>
              </div>
               <div class="clear"></div>
          </section>';
    
    return $conten;
    
}
?>



