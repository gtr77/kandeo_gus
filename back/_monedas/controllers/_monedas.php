<?php

////////////////////////////////
//@autor: Brayan Acebo
//brayan.acebo@imaginamos.co
//Agencia: imaginamos.com
//Bogotá, Colombia, 2012
////////////////////////////////
 
class _monedas extends Back_Controller {
	

   // protected $admin_area = TRUE;
   
    
    public function __construct() {
        parent::__construct();
    }

    // ----------------------------------------------------------------------

    public function index() {
         
        $mon = new Monedas();
        $mon->getMonedas();
        $this->_data["monedas"] = $mon;
        return $this->build('body');
    }

    public function add_paises(){
        
         $file = $_FILES["file"];
          $nombre = $file["name"];
          $tipo = $file["type"];
            $ruta_provisional = $file["tmp_name"];
            $size = $file["size"];
            $dimensiones = getimagesize($ruta_provisional);
            $width = $dimensiones[0];
            $height = $dimensiones[1];
            $carpeta = "uploads/";
        
          $src = $carpeta.$nombre;
           move_uploaded_file($ruta_provisional, $src);

        
        
        $datos = array(
                'nombre' => $this->input->post('nombre'),
                'imagen'=> $nombre
        );
        $a = new Paises();
        $a->savePaises($datos);
        
    }
    
    public function edit_paises(){
        
           $file = $_FILES["file"];
          $nombre = $file["name"];
          $tipo = $file["type"];
            $ruta_provisional = $file["tmp_name"];
            $size = $file["size"];
            $dimensiones = getimagesize($ruta_provisional);
            $width = $dimensiones[0];
            $height = $dimensiones[1];
            $carpeta = "uploads/";
        
         $src = $carpeta.$nombre;
           move_uploaded_file($ruta_provisional, $src);
        
         $a = new Paises();
        $datos = array(
           "id" => $this->input->post('id'),                  
           "nombre" => $this->input->post('nombre'),                  
           "imagen" => $nombre            
        );
        $update = $a->updatePaises($datos);
    }

     public function deletePaises(){
        
       $id = $this->input->post('id');
        $a = new Paises();
       $a->delete_Paises($id);
    }
}
