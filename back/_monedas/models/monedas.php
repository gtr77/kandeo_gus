<?php

class Monedas extends DataMapper {
	
	

    public $model = 'monedas';
    public $table = 'monedas';
    public $has_one = array();
    public $has_many = array(
        'group' => array('auto_populate' => true, 'join_table' => 'users_groups'),
        'area' => array('join_table' => 'users_areas', 'auto_populate' => true),
        'project' => array('auto_populate' => true),
        'projects_comment' => array('auto_populate' => true),
        'projects_comments_winner' => array('auto_populate' => true),
        'projects_follower' => array('auto_populate' => true),
        'proposal' => array('auto_populate' => true),
        'report' => array('auto_populate' => true),
        'users_level' => array('auto_populate' => true),
        'users_transaction' => array('auto_populate' => true)
    );
    public $is_owner = false;

    public function __construct($id = NULL) {
        parent::__construct($id);
    }

    // ----------------------------------------------------------------------

    public function __call($method, $arguments) {
        static $watched_methods = array('is_role_');

        foreach ($watched_methods as $watched_method) {

            if (strpos($method, $watched_method) !== FALSE) {
                $pieces = explode($watched_method, $method);

                return $this->{'_' . trim($watched_method, '_')}($pieces[1]);
            }
        }

        return parent::__call($method, $arguments);
    }

    // ----------------------------------------------------------------------

    public function _required_terms($field) {
        if (!empty($this->{$field})) {
            return TRUE;
        }

        return 'Acepte los <strong>Términos y condiciones</strong> para registarse.';
    }
    
    public function saveMonedas($object = "") {
        
        $data = array(
          'nombre' => $object['nombre'],
         'imagen' => $object['imagen']
        );

        //$this->save();
        $this->db->insert('cms_paises',$data);
    }

    public function getMonedas(){
        return $this->order_by('id', 'ASC')->get();
    }
    
    public function updateMonedas($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function delete_Monedas($id) {
       $this->db->where('id', $id);
       $this->db->delete('cms_monedas');
    }
	

    // ----------------------------------------------------------------------

  
}
