<?php

class _Userapp extends Back_Controller {

    public function __construct() {
        parent::__construct();
        $this->_data['nombremodulo'] = "USUARIO APP";
        if (true != $this->have_admin_access()) {
            return redirect(cms_url('login'), 'refresh');
        }
    }

    // ------------------------------USUARIOS----------------------------------------
    
    public function index($not = '') {
        $this->_data['not'] = $not;
        $c = new Userapp();
        $info = $c->getUserapp();
        $this->_data["info"] = $info;
        return $this->build('userapp');
    }     
    
    
    
    public function delete_userapp($id = ""){
        $a = new Userapp();
        $not = FALSE;
        if ($a->deleteUserapp($id)){
            $not = TRUE;
        };
        return redirect("cms/userapp/index/".$not);
    }
    

    
}
