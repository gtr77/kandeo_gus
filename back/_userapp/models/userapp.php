<?php

class Userapp extends DataMapper {

    public $model = 'userapp';
    public $table = 'userapp';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getUserapp(){
        return $this->get();
    }
    
    public function deleteUserapp($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }
}
