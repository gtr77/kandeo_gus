<?php

////////////////////////////////
//@autor: Brayan Acebo
//brayan.acebo@imaginamos.co
//Agencia: imaginamos.com
//Bogotá, Colombia, 2012
////////////////////////////////
 
class _Fondos extends Back_Controller {
	

   // protected $admin_area = TRUE;
   
    
    public function __construct() {
        parent::__construct();
    }

    // ----------------------------------------------------------------------

    public function index() {
       
        $fon = new Fondos();
        $fon->getFondos();
        $this->_data["fondos"] = $fon;
        
        $this->load->model('_sociedades/sociedades');
        $so = new Sociedades();
        $so->getSociedades();
        $this->_data["sociedades"] = $so;
        
        $this->load->model('_monedas/monedas');
        $mon = new Monedas();
        $mon->getMonedas();
        $this->_data["monedas"] = $mon;
        
        return $this->build('body');
    }

    public function add_fondos(){
        

        
        $datos = array(
            
                'nombre' => $this->input->post('nombre'),
                'pais' => $this->input->post('pais'),
                'tamano_fondo' => $this->input->post('tamano_fondo'),
                'tipo_moneda' => $this->input->post('monedas'),
                'invertido' => $this->input->post('invertido'),
                'comprometido' => $this->input->post('comprometido'),
                'gastos' => $this->input->post('gastos'),
                'gastos_proyectados' => $this->input->post('gastos_proyectados'),
                'disponible' => $this->input->post('disponible')
                
        );
       print_r($_POST);
        
       /* $cantidad_sociedades = count( $this->input->post('sociedades'));
        
     //   $this->load->model('_fondos/monedas');
           $foas = new fondo_as_sociedades();
        
                for ($x = 0; $x <= $cantidad_sociedades; $x++) {
                    $sociedades = array('id_sociedad' => $this->input->post('sociedades')[$x],'id_fondo' => 1);
                     $foas->saveFondosSociedad($sociedades);
                } 
     */
     
       
        
        $a = new Fondos();
        $a->saveFondos($datos);
        
    }
    

    public function edit_sociedades(){
        
        $a = new Sociedades();
        $datos = array(
            
           "id" => $this->input->post('id'),                  
           "nit" => $this->input->post('nit'), 
            "nombre" => $this->input->post('nombre'),                  
           "pais" => $this->input->post('pais'),                  
           "descripcion" => $this->input->post('descripcion'),                  
           "direccion" => $this->input->post('direccion'),                  
           "fecha_inicio" => $this->input->post('fecha_inicio'),                  
                    
        );
        
            $file = $_FILES["file"];
            $nombre = $file["name"];
            $tipo = $file["type"];
            $ruta_provisional = $file["tmp_name"];
            $size = $file["size"];
            $dimensiones = getimagesize($ruta_provisional);
            $width = $dimensiones[0];
            $height = $dimensiones[1];
            $carpeta = "uploads/";
        
         $src = $carpeta.$nombre;
           move_uploaded_file($ruta_provisional, $src);
            
            $nom = array("logo" => $nombre);
            
           
        if($nombre != ""){
            $dat = array_merge( $datos , $nom);
        }else{
            $dat = $datos;
        }
        
        
        
        $update = $a->updateSociedades($dat);
    }

    public function deleteFondos(){
        
       $id = $this->input->post('id');
        $a = new Fondos();
       $a->delete_Fondos($id);
    }
    
}
