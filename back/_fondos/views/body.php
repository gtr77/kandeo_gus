<section class="content_all">



    <!--Contenidos Sitio-->
    <section class="cont_home">           

      <section class="conten_all_site">

        <!---Titulo-->
        <?php
          echo menu_configuracion();          
        ?>
        <!--Fin Titulo-->                  

          

          <section class="tabla_contenidos sin_bg"> 

            <section class="padding"> 
              <div class="head_table_settings">
                <div class="large-6 medium-6 small-6 columns">
                  
                </div>
                <div class="large-12 medium-6 small-6 columns">
                <a href="javascript:void(0)" class="btn_border right m_l_10  waves-button waves-effect waves-light new_crear_fondo" >Nuevo Fondo</a>    
                  <div class="clear"></div>
              </div>
              </div>
            </section>

              <div class="clear"></div>

            <!--All fonds-->          
            <ul class="list_user_register m_t_20">
                
                <?php
                foreach($fondos as $fo){
                ?>
              <li class="large-3 medium-3 small-3 column padding animated fadeInUp fondel<?php echo $fo->id; ?>">
               <div class="bg_panel funts">
                
                 
                  <div class="txt_left padd_all left">
                    <div class="per_nombre">
                       <h3><?php echo $fo->nombre; ?></h3>
                       <p class="cargo sin_margin"><?php echo $fo->pais; ?></p>
                       <p class="cargo sin_margin">
                           
                                   <?php 
                                foreach($monedas as $mon){
                                    if($mon->id == $fo->tipo_moneda){
                                        echo "<b>".$mon->referencia." $</b>";
                                    }
                                }
                              
                           ?>
                           <?php echo $fo->tamano_fondo; ?></p>
                        <p class="cargo sin_margin"><b>$</b><?php echo $fo->invertido; ?></p>
                       <p class="cargo sin_margin"><b>$</b><?php echo $fo->comprometido; ?></p>
                       <p class="cargo sin_margin"><b>$</b><?php echo $fo->gastos; ?></p>
                       <p class="cargo sin_margin"><b>$</b><?php echo $fo->gastos_proyectados; ?></p>
                       <p class="cargo sin_margin"><b>$</b><?php echo $fo->disponible; ?></p>
                    </div>  
                  </div>  
                  <div class="right">
                    <div class="action_user">
                      <div class="list_setting"><i class="ti-align-left"></i>
                        <ul>  
                            <li><a href="javascript:void(0)" class="new_crear_fondo">Editar</a></li>
                            <li><a href="#" onclick="eliminar_fondo(<?php echo $fo->id; ?>)">Eliminar</a></li>
                        </ul>
                      </div> 
                    </div>
                  </div>
                     <div class="clear"></div>
               </div>
              </li>
                
                <?php
                }
                ?>
                
            </ul>      
            <!--Fin All fonds-->

          <div class="clear"></div>
        </section>

            <div class="clear"></div>
         </div>
      </section>     

      <div class="clear"></div>
  </section>
  <!--Fin Contenidos Sitio-->

<script>

    
    function crear_fondo(){
              
        var datos = new FormData($("#formu_crear_fondo")[0]);
      
        var sociedades = $('#sociedades').val();
      
        
        
        console.log(datos);
             $.ajax({
                url: "<?php echo base_url() ?>cms/fondos/add_fondos", 
                data: {sociedades:sociedades},
                type: "POST",
                contentType: false,
                processData: false,

                success: function(data) {
                 //location.reload();
                  return data; 
                }
              });
    }
    
    function eliminar_fondo(id){
        
            $.ajax({
                url: "<?php echo base_url() ?>cms/fondos/deleteFondos", 
                data: {id: id} ,
                type: "POST",
                dataType: "json",
                success: function(data) {
                    $('.fondel'+id).css({'display':'none'});
                  return data; 
                }
              });
        
    }
    $('.chosen_sociedades').chosen();
  
</script>