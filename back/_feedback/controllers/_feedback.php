<?php

class _Feedback extends Back_Controller {

    public function __construct() {
        parent::__construct();
        $this->_data['nombremodulo'] = "FEEDBACK";
        if (true != $this->have_admin_access()) {
            return redirect(cms_url('login'), 'refresh');
        }
    }

    // ------------------------------FEEDBACK----------------------------------------
    
    public function index($not = '') {
        $this->_data['not'] = $not;
        $c = new Feedback();
        $info = $c->getFeedback();
        $this->_data["info"] = $info;
        return $this->build('feedback');
    }     
    
    
    
    public function delete_feedback($id = ""){
        $a = new Feedback();
        $not = FALSE;
        if ($a->deleteFeedback($id)){
            $not = TRUE;
        };
        return redirect("cms/feedback/index/".$not);
    }
    

    public function ver($id = "",$not = ""){        
        $a = new Feedback();
        $dat = $a->getFeedbackById($id);
        $this->_data["info"] = $dat;
        $this->_data["not"] = $not;
        return $this->build("ver");
    }



    
}
