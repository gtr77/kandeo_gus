<?php

class Feedback extends DataMapper {

    public $model = 'feedback';
    public $table = 'feedback';
    
    public $has_one = array();
    
    public function __construct($id = NULL) {
        parent::__construct($id);
    }
    
    public function getFeedback(){
        return $this->get();
    }
    
    public function getFeedbackById($id = ""){        
        return $this->get_by_id($id);
    }
    public function deleteFeedback($idP) {
        $this->where('id',$idP)->get();
        return $this->delete();
    }
}
