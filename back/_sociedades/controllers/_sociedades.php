<?php

////////////////////////////////
//@autor: Brayan Acebo
//brayan.acebo@imaginamos.co
//Agencia: imaginamos.com
//Bogotá, Colombia, 2012
////////////////////////////////
 
class _Sociedades extends Back_Controller {
	

   // protected $admin_area = TRUE;
   
    
    public function __construct() {
        parent::__construct();
    }

    // ----------------------------------------------------------------------

    public function index() {
       
        $sos = new Sociedades();
        $sos->getSociedades();
        $this->_data["sociedades"] = $sos;
        return $this->build('body');
    }

    public function add_sociedades(){
        
         $file = $_FILES["file"];
          $nombre = $file["name"];
          $tipo = $file["type"];
            $ruta_provisional = $file["tmp_name"];
            $size = $file["size"];
            $dimensiones = getimagesize($ruta_provisional);
            $width = $dimensiones[0];
            $height = $dimensiones[1];
            $carpeta = "uploads/";
        
          $src = $carpeta.$nombre;
           move_uploaded_file($ruta_provisional, $src);

        
        
        $datos = array(
            
                'nit' => $this->input->post('nit'),
                'logo'=> $nombre,
                'nombre' => $this->input->post('nombre'),
                'pais' => $this->input->post('pais'),
                'descripcion' => $this->input->post('descripcion'),
                'direccion' => $this->input->post('direccion'),
                'fecha_inicio' => $this->input->post('fecha_inicio')
                
        );
        $a = new Sociedades();
        $a->saveSociedades($datos);
        
    }
    

    public function edit_sociedades(){
        
        $a = new Sociedades();
        $datos = array(
            
           "id" => $this->input->post('id'),                  
           "nit" => $this->input->post('nit'), 
            "nombre" => $this->input->post('nombre'),                  
           "pais" => $this->input->post('pais'),                  
           "descripcion" => $this->input->post('descripcion'),                  
           "direccion" => $this->input->post('direccion'),                  
           "fecha_inicio" => $this->input->post('fecha_inicio'),                  
                    
        );
        
            $file = $_FILES["file"];
            $nombre = $file["name"];
            $tipo = $file["type"];
            $ruta_provisional = $file["tmp_name"];
            $size = $file["size"];
            $dimensiones = getimagesize($ruta_provisional);
            $width = $dimensiones[0];
            $height = $dimensiones[1];
            $carpeta = "uploads/";
        
         $src = $carpeta.$nombre;
           move_uploaded_file($ruta_provisional, $src);
            
            $nom = array("logo" => $nombre);
            
           
        if($nombre != ""){
            $dat = array_merge( $datos , $nom);
        }else{
            $dat = $datos;
        }
        
        
        
        $update = $a->updateSociedades($dat);
    }

    public function deleteSociedades(){
        
       $id = $this->input->post('id');
        $a = new Sociedades();
       $a->delete_Sociedades($id);
    }
    
    public function contactos($id_sociedad){
        $con = new Contactos();
        $con->getContactos($id_sociedad);
        $this->_data["contactos"] = $con;
        $this->_data["id_sociedad"] = $id_sociedad;
        return $this->build('contactos');
        
    }
    
     public function add_contactos(){
        
        $datos = array(
            
                'id_sociedad' => $this->input->post('id_sociedad'),
                'nombre' => $this->input->post('nombre'),
                'apellido' => $this->input->post('apellido'),
                'cargo' => $this->input->post('cargo'),
                'email' => $this->input->post('email'),
                'skype' => $this->input->post('skype'),
                'telefono' => $this->input->post('telefono'),
                'celular' => $this->input->post('celular'),
                'direccion' => $this->input->post('direccion'),
                'fecha_nacimiento' => $this->input->post('fecha_nacimiento'),
                'descripcion' => $this->input->post('descripcion')
                
        );
        $a = new Contactos();
        $a->saveContactos($datos);
        
    }
    
    public function deleteContactos(){
        
       $id = $this->input->post('id');
        $a = new Contactos();
       $a->delete_Contactos($id);
        
    }
    
    public function edit_contactos(){
        
        $a = new Contactos();
        $datos = array(
            
                'id' => $this->input->post('id'),
                'nombre' => $this->input->post('nombre'),
                'apellido' => $this->input->post('apellido'),
                'cargo' => $this->input->post('cargo'),
                'email' => $this->input->post('email'),
                'skype' => $this->input->post('skype'),
                'telefono' => $this->input->post('telefono'),
                'celular' => $this->input->post('celular'),
                'direccion' => $this->input->post('direccion'),
                'fecha_nacimiento' => $this->input->post('fecha_nacimiento'),
                'descripcion' => $this->input->post('descripcion')                
                    
        );
        
  
        
        
        $update = $a->updateContactos($datos);
    }
    
}
