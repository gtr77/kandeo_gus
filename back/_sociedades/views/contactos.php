<section class="content_all">
    
<section class="cont_home">           

      <section class="conten_all_site">

       <?php
          echo menu_configuracion();          
        ?>

          <section class="tabla_contenidos sin_bg"> 

            <section class="padding"> 
              <div class="head_table_settings">
                <div class="large-6 medium-6 small-6 columns">
                  
                </div>
                <div class="large-12 medium-6 small-6 columns">
                <a href="javascript:void(0)" class="btn_border right m_l_10  waves-button waves-effect waves-light" onclick="formulario_contactos(<?php echo $id_sociedad; ?>);">Nuevo Contacto</a>    
                  <div class="clear"></div>
              </div>
              </div>
            </section>

              <div class="clear"></div>

            <!--All fonds-->          
            <ul class="list_user_register m_t_20">
                
            <?php
            foreach($contactos as $con){    
                
                $data_editar = $con->id.'|'.$con->nombre.'|'.$con->apellido.'|'.$con->cargo.'|'.$con->email.'|'.$con->skype.'|'.$con->telefono.'|'.$con->celular.'|'.$con->direccion.'|'.$con->fecha_nacimiento.'|'.$con->descripcion;
                
            ?>
              <li class="large-3 medium-3 small-3 column padding animated fadeInUp contadel<?php echo $con->id;?>">
               <div class="bg_panel funts">
                
                  <div class="txt_left padd_all left">
                    <div class="per_nombre">
                       <h3><?php echo $con->nombre.' '.$con->apellido; ?></h3>
                       <p class="cargo sin_margin"><?php echo $con->cargo;?></p>
                       <p class="email sin_margin"><?php echo $con->email;?></p>
                       <p class="skype sin_margin"><?php echo $con->skype;?></p>
                       <p class="tel sin_margin"><?php echo $con->telefono;?></p>
                       <p class="cel sin_margin"><?php echo $con->celular;?></p>
                       <p class="direccion sin_margin"><?php echo $con->direccion;?></p>
                       <p class="anos sin_margin"><?php echo $con->fecha_nacimiento;?></p>
                        <h4>Descripción</h4>
                        <p class="descripcion ">
                        <?php echo $con->descripcion;?>
                        </p>
                    </div>  
                  </div>  
                  <div class="right">
                    <div class="action_user">
                      <div class="list_setting"><i class="ti-align-left"></i>
                        <ul>  
                            <li><a href="javascript:void(0)"  onclick="formulario_editar_contactos('<?php echo $data_editar; ?>')">Editar</a></li>
                            <li><a href="#" onclick="eliminar_contactos(<?php echo $con->id;?>)">Eliminar</a></li>
                        </ul>
                      </div> 
                    </div>
                  </div>
                     <div class="clear"></div>
               </div>
              </li>
            <?php
            }
            ?>
 

            </ul>      
            <!--Fin All fonds-->

          <div class="clear"></div>
        </section>

            <div class="clear"></div>
         </div>
      </section>     

      <div class="clear"></div>
  </section>

    <div class="clear"></div>


</section><!--Content_all-->

<script>
        function formulario_contactos(id_sociedad){
               
        var id_referencia = "crear_contactos";
        var nombre_formulario = "Crear Contacto";
        var campos = Array('id_sociedad|hidden|'+id_sociedad,'nombre|text','apellido|text','cargo|text','email|text','skype|text','telefono|text','celular|text','direccion|text','fecha_nacimiento|date','descripcion|textarea');
       
        
        generar_formulario(id_referencia,nombre_formulario,campos);
            
        }
    
        function crear_contactos(){
            
             var datos = new FormData($("#formu_crear_contactos")[0]);
     
      
             $.ajax({
                url: "<?php echo base_url() ?>cms/sociedades/add_contactos", 
                data: datos,
                type: "POST",
                contentType: false,
                processData: false,

                success: function(data) {
                  location.reload();
                  return data; 
                }
              });
            
        }
    
        function formulario_editar_contactos(dat){
        
                var partes = dat.split("|");

                var id_referencia = "editar_contactos";
                var nombre_formulario = "Editar Contctos";
                var campos = Array('id|hidden|'+partes[0],'nombre|text|'+partes[1],'apellido|text|'+partes[2],'cargo|text|'+partes[3],'email|text|'+partes[4],'skype|text|'+partes[5],'telefono|text|'+partes[6],'celular|text|'+partes[7],'direccion|text|'+partes[8],'fecha_nacimiento|date|'+partes[9],'descripcion|textarea|'+partes[10]);



                generar_formulario(id_referencia,nombre_formulario,campos);
       }
    
        function eliminar_contactos(id){

            $.ajax({
                url: "<?php echo base_url() ?>cms/sociedades/deleteContactos", 
                data: {id: id} ,
                type: "POST",
                dataType: "json",
                success: function(data) {
                    $('.contadel'+id).css({'display':'none'});
                  return data; 
                }
              });
         }
    
        function editar_contactos(){
                

            var datos = new FormData($("#formu_editar_contactos")[0]);
        
      
             $.ajax({
                url: "<?php echo base_url() ?>cms/sociedades/edit_contactos", 
                data: datos,
                type: "POST",
                contentType: false,
                processData: false,

                success: function(data) {
                  location.reload();
                  return data; 
                }
              });
        
    
        }
        
</script>