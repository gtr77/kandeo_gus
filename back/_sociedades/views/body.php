<section class="content_all">



    <!--Contenidos Sitio-->
    <section class="cont_home">           

      <section class="conten_inicial">

        <!---Titulo-->
        <?php
          echo menu_configuracion();          
        ?>
        <!--Fin Titulo-->                  

          
        <section class="listProspectBissnes"> 
            
                <div class="large-12 medium-9 small-9 columns">
                  <a href="javascript:void(0)" class="btn_border right" onclick="formulario_sociedades()">Nueva Sociedad</a>
                 </div>
             <div class="clear"></div>
            <div class="espacio_head"></div>
            <div class="tabla_contenidos cont_sociedades" style="width: 1240px; left: 20px; height: 500px;">
                
                
           
                
                
                
                <div class="head_table ">
                    <div class="large-12 medium-3 small-3 columns">
                          <div class="large-1 medium-2 small-2 columns">
                            <h3>Nit</h3>
                          </div>
                          <div class="large-1 medium-3 small-3 columns">
                            <h3>Logo</h3>
                          </div>
                          <div class="large-2 medium-3 small-3 columns">
                            <h3>Nombre</h3>
                          </div> 
                          <div class="large-1 medium-3 small-3 columns">
                            <h3>País</h3>
                          </div>
                          <div class="large-2 medium-3 small-3 columns">
                            <h3>Descripción</h3>
                          </div>
                          <div class="large-2 medium-3 small-3 columns">
                            <h3>Dirección</h3>
                          </div>
                          <div class="large-2 medium-3 small-3 columns">
                            <h3>Fecha de Inicio</h3>
                          </div>
                      
                    </div>
                    <div class="large-3 medium-3 small-3 columns">
                        
                    </div>
                </div>
                <div class="espacio_head"></div>
                
                <?php
                    foreach($sociedades as $so){
                        
                        $data_edit = $so->id.'|'.$so->nit.'|'.$so->logo.'|'.$so->nombre.'|'.$so->pais.'|'.$so->descripcion.'|'.$so->fecha_inicio.'|'.$so->direccion;
                ?>
                <article class="responsive tabla_uno tabla_campan socidel<?php echo $so->id; ?>">
                    <div class="large-12 medium-3 small-3 columns">
                          <div class="large-1 medium-3 small-3 columns">
                            <p><?php echo $so->nit; ?></p>
                          </div>
                          <div class="large-1 medium-3 small-3 columns">
                                 <div class="img_empresa">
                                    <img src="../uploads/<?php echo $so->logo; ?>" alt="" class="animated fadeIn" data-pin-nopin="true">
                                  </div>
                          </div>
                          <div class="large-2 medium-3 small-3 columns">
                            <p><?php echo $so->nombre; ?></p>
                          </div> 
                          <div class="large-1 medium-3 small-3 columns">
                            <p><?php echo $so->pais; ?></p>
                          </div>
                          <div class="large-3 medium-3 small-3 columns">
                            <p><?php echo $so->descripcion; ?></p>
                          </div>
                          <div class="large-2 medium-3 small-3 columns">
                            <p><?php echo $so->direccion; ?></p>
                          </div>
                          <div class="large-1 medium-3 small-3 columns">
                            <p><?php echo $so->fecha_inicio; ?></p>
                          </div>
                           <div class="large-1 medium-3 small-3 columns ics" style="margin-top: 21px;">
                               <a href="#" class="action_table1 borar_t top_tip" data-tips="Eliminar" onclick="delete_sociedad(<?php echo $so->id; ?>);">
                               <i class="ti-trash"></i>
                               </a> 
                               <a href="#" class="action_table1 borar_t top_tip" data-tips="Editar" onclick="formulario_editar_sociedad(".$data_edit.");">
                               <i class="ti-pencil-alt"></i>
                               </a> 
                               <a href="http://45.79.10.117/kandeo-leo/cms//sociedades/contactos/<?php echo $so->id; ?>" class="action_table1 borar_t top_tip" data-tips="Contactos">
                               <i class="fa fa-user"></i>
                               </a>
                           </div>    
                    </div>
                </article>
                <?php
                    }
                ?>
                
            </div>


</section><!--Content_all-->

<script>
    function formulario_sociedades(){
        
        var id_referencia = "crear_sociedad";
        var nombre_formulario = "Nueva Sociedad";
        var campos = Array('nit|text','imagen|file','nombre|text','pais|text','descripcion|textarea','direccion|text','fecha_inicio|date');
        
        generar_formulario(id_referencia,nombre_formulario,campos);
    }
    function crear_sociedad(){
        
            var datos = new FormData($("#formu_crear_sociedad")[0]);
     
      
             $.ajax({
                url: "<?php echo base_url() ?>cms/sociedades/add_sociedades", 
                data: datos,
                type: "POST",
                contentType: false,
                processData: false,

                success: function(data) {
                  location.reload();
                  return data; 
                }
              });
        
    }
    function delete_sociedad(id){
        
              $.ajax({
                url: "<?php echo base_url() ?>cms/sociedades/deleteSociedades", 
                data: {id: id} ,
                type: "POST",
                dataType: "json",
                success: function(data) {
                    $('.socidel'+id).css({'display':'none'});
                  return data; 
                }
              });
    }
    
    function formulario_editar_sociedad(dat){
        
        var partes = dat.split("|");
       
        var id_referencia = "editar_sociedad";
        var nombre_formulario = "Editar Sociedad";
        var campos = Array('id|hidden|'+partes[0],'nit|text|'+partes[1],'logo|file|'+partes[2],'nombre|text|'+partes[3],'pais|text|'+partes[4],'descripcion|text|'+partes[5],'direccion|text|'+partes[7],'fecha_inicio|text|'+partes[6]);
        
       
         
        generar_formulario(id_referencia,nombre_formulario,campos);
    }
    
    
    function editar_sociedad(){
        console.log('prueba');
            var datos = new FormData($("#formu_editar_sociedad")[0]);
        
      
             $.ajax({
                url: "<?php echo base_url() ?>cms/sociedades/edit_sociedades", 
                data: datos,
                type: "POST",
                contentType: false,
                processData: false,

                success: function(data) {
                 location.reload();
                  return data; 
                }
              });
        
    }
    
</script>