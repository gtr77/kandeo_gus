<?php

class Contactos extends DataMapper {
	
	

    public $model = 'contactos_sociedades';
    public $table = 'contactos_sociedades';
    public $has_one = array();
    public $has_many = array(
        'groups' => array(
            'join_self_as' => 'id_sociedad', 
            'join_other_as' => 'id', 
            'join_table' => 'cms_contactos_sociedades'
            )   
    );
    public $is_owner = false;

    public function __construct($id = NULL) {
        parent::__construct($id);
    }

    // ----------------------------------------------------------------------

    public function __call($method, $arguments) {
        static $watched_methods = array('is_role_');

        foreach ($watched_methods as $watched_method) {

            if (strpos($method, $watched_method) !== FALSE) {
                $pieces = explode($watched_method, $method);

                return $this->{'_' . trim($watched_method, '_')}($pieces[1]);
            }
        }

        return parent::__call($method, $arguments);
    }

    // ----------------------------------------------------------------------

    public function _required_terms($field) {
        if (!empty($this->{$field})) {
            return TRUE;
        }

        return 'Acepte los <strong>Términos y condiciones</strong> para registarse.';
    }
    

    
    public function getContactos($id_sociedad){
    return $this->where('id_sociedad',$id_sociedad)->get();
   
        
     }
    
    public function saveContactos($object = "") {
  
         $data = array(
                'id_sociedad' => $object['id_sociedad'],
                'nombre' => $object['nombre'],
                'apellido' => $object['apellido'],
                'cargo' => $object['cargo'],
                'email' => $object['email'],
                'skype' => $object['skype'],
                'telefono' => $object['telefono'],
                'celular' => $object['celular'],
                'direccion' => $object['direccion'],
                'fecha_nacimiento' => $object['fecha_nacimiento'],
                'descripcion' => $object['descripcion']
         );
        
        //$this->save();
        $this->db->insert('cms_contactos_sociedades',$data);
    }
    
    public function delete_Contactos($id) {
       $this->db->where('id', $id);
       $this->db->delete('cms_contactos_sociedades');
    }

    public function updateContactos($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    // ----------------------------------------------------------------------

  
}
