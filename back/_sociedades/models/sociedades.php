<?php

class Sociedades extends DataMapper {
	
	

    public $model = 'sociedades';
    public $table = 'sociedades';
    public $has_one = array();
    public $has_many = array(
        'groups' => array(
            'join_self_as' => 'id_sociedad', 
            'join_other_as' => 'id', 
            'join_table' => 'cms_contactos_sociedades'
            )   
    );
    public $is_owner = false;

    public function __construct($id = NULL) {
        parent::__construct($id);
    }

    // ----------------------------------------------------------------------

    public function __call($method, $arguments) {
        static $watched_methods = array('is_role_');

        foreach ($watched_methods as $watched_method) {

            if (strpos($method, $watched_method) !== FALSE) {
                $pieces = explode($watched_method, $method);

                return $this->{'_' . trim($watched_method, '_')}($pieces[1]);
            }
        }

        return parent::__call($method, $arguments);
    }

    // ----------------------------------------------------------------------

    public function _required_terms($field) {
        if (!empty($this->{$field})) {
            return TRUE;
        }

        return 'Acepte los <strong>Términos y condiciones</strong> para registarse.';
    }
    
    public function saveSociedades($object = "") {
        
        $data = array(
          'nit' => $object['nit'],
         'logo' => $object['logo'],
         'nombre' => $object['nombre'],
         'pais' => $object['pais'],
         'descripcion' => $object['descripcion'],
         'direccion' => $object['direccion'],
         'fecha_inicio' => $object['fecha_inicio'],
        );

        //$this->save();
        $this->db->insert('cms_sociedades',$data);
    }

    public function getSociedades(){
        return $this->order_by('id', 'ASC')->get();
    }
    
    public function getContactos($id_sociedad){
   
          $this->db->select('Image');
       return $this->db->from('cms_contactos_sociedades');
        
           
        
    }
    
    public function updateSociedades($datos = ""){
       return $this->where('id',$datos["id"])->update($datos, TRUE);
    }
    
    public function delete_Sociedades($id) {
       $this->db->where('id', $id);
       $this->db->delete('cms_sociedades');
    }
	

    // ----------------------------------------------------------------------

  
}
