
<section class="content_all">



    <!--Contenidos Sitio-->
    <section class="cont_home">           

      <section class="conten_inicial">

        <!---Titulo-->
        <section class="titulo">
           <h1 class="large-5 medium-5 small-5 columns"><a href="dashboard.php">Inicio</a></h1>
<!--            <a href="javascript:void(0)" class="btn_blanco right m_l_10 stream_dash waves-button waves-effect waves-light">Streams</a>     -->
            <a href="javascript:void(0)" class="btn_blanco right m_l_10 new_activity waves-button waves-effect waves-light"><i class="fa fa-sticky-note-o" aria-hidden="true"></i>Nueva Tarea</a>          
            <a href="javascript:void(0)" class="btn_blanco right m_l_10 new_prospect waves-button waves-effect waves-light"><i class="fa fa-building" aria-hidden="true"></i>Nueva Empresa</a>                   
             <div class="clear"></div>
        </section>
        <!--Fin Titulo-->                  

        <section class="drag-container">
          <ul class="drag-list listEtapas">
            <li class="drag-column drag-column-on-hold drag-column2">
              <div class="tittleList">
                <a href="prospect.php">
                  <span><strong>17</strong></span>
                  <i class="fa fa-sellsy" aria-hidden="true"></i>
                  <h3>Pipeline</h3>
                  <span class="price">$ 18.450M</span>
                </a>
              </div>
              <ul class="drag-list">
                <li class="drag-column drag-column-on-hold">    
                  <div class="scrolldash">  
                    <ul class="drag-inner-list list_componentes" id="1">
                      <li class="drag-item">
                        <a href="detail_prospect.php">
                          <h4>Kandeo II</h4>
                          <span>Actualizado</span><br>
                          <span>Hace 2 días</span>
                          <span class="price">$ 18.450M</span>
                        </a>
                      </li>
                      <li class="drag-item">
                        <a href="detail_prospect.php">
                          <h4>Kandeo II</h4>
                          <span>Actualizado</span><br>
                          <span>Hace 2 días</span>
                          <span class="price">$ 18.450M</span>
                        </a>
                      </li>
                      <li class="drag-item">
                        <a href="detail_prospect.php">
                          <h4>Kandeo II</h4>
                          <span>Actualizado</span><br>
                          <span>Hace 2 días</span>
                          <span class="price">$ 18.450M</span>
                        </a>
                      </li>
                      <li class="drag-item">
                        <a href="detail_prospect.php">
                          <h4>Kandeo II</h4>
                          <span>Actualizado</span><br>
                          <span>Hace 2 días</span>
                          <span class="price">$ 18.450M</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </li>
                <li class="drag-column drag-column-in-progress">
                  <div class="scrolldash">  
                    <ul class="drag-inner-list list_componentes" id="2">
                      <li class="drag-item">
                        <a href="detail_prospect.php">
                          <h4>Kandeo II</h4>
                          <span>Actualizado</span><br>
                          <span>Hace 2 días</span>
                          <span class="price">$ 18.450M</span>
                        </a>
                      </li>
                      <li class="drag-item">
                        <a href="detail_prospect.php">
                          <h4>Kandeo II</h4>
                          <span>Actualizado</span><br>
                          <span>Hace 2 días</span>
                          <span class="price">$ 18.450M</span>
                        </a>
                      </li>
                      <li class="drag-item">
                        <a href="detail_prospect.php">
                          <h4>Kandeo II</h4>
                          <span>Actualizado</span><br>
                          <span>Hace 2 días</span>
                          <span class="price">$ 18.450M</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </li>
              </ul>
            </li>
            
            <li class="drag-column drag-column-on-hold drag-column2">
              <div class="tittleList">
                <a href="prospect.php">
                  <span><strong>17</strong></span>
                  <i class="fa fa-sellsy" aria-hidden="true"></i>
                  <h3>Inversiones</h3>
                  <span class="price">$ 18.450M</span>
                </a>
              </div>
              <ul class="drag-list">
                <li class="drag-column drag-column-on-hold">    
                  <div class="scrolldash">  
                    <ul class="drag-inner-list list_componentes" id="3">
                      <li class="drag-item">
                        <a href="detail_prospect.php">
                          <h4>Kandeo II</h4>
                          <span>Actualizado</span><br>
                          <span>Hace 2 días</span>
                          <span class="price">$ 18.450M</span>
                        </a>
                      </li>
                      <li class="drag-item">
                        <a href="detail_prospect.php">
                          <h4>Kandeo II</h4>
                          <span>Actualizado</span><br>
                          <span>Hace 2 días</span>
                          <span class="price">$ 18.450M</span>
                        </a>
                      </li>
                      <li class="drag-item">
                        <a href="detail_prospect.php">
                          <h4>Kandeo II</h4>
                          <span>Actualizado</span><br>
                          <span>Hace 2 días</span>
                          <span class="price">$ 18.450M</span>
                        </a>
                      </li>
                      <li class="drag-item">
                        <a href="detail_prospect.php">
                          <h4>Kandeo II</h4>
                          <span>Actualizado</span><br>
                          <span>Hace 2 días</span>
                          <span class="price">$ 18.450M</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </li>
                  
                <li class="drag-column drag-column-in-progress">
                  <div class="scrolldash">  
                    <ul class="drag-inner-list list_componentes" id="4">
                      <li class="drag-item">
                        <a href="detail_prospect.php">
                          <h4>Kandeo II</h4>
                          <span>Actualizado</span><br>
                          <span>Hace 2 días</span>
                          <span class="price">$ 18.450M</span>
                        </a>
                      </li>
                      <li class="drag-item">
                        <a href="detail_prospect.php">
                          <h4>Kandeo II</h4>
                          <span>Actualizado</span><br>
                          <span>Hace 2 días</span>
                          <span class="price">$ 18.450M</span>
                        </a>
                      </li>
                      <li class="drag-item">
                        <a href="detail_prospect.php">
                          <h4>Kandeo II</h4>
                          <span>Actualizado</span><br>
                          <span>Hace 2 días</span>
                          <span class="price">$ 18.450M</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </li>
              </ul>
            </li>

          </ul>
        </section>
      
          <div class="clear"></div>
      </section>

        <div class="clear"></div>
    </section>
    <!--Fin Contenidos Sitio-->

    <div class="clear"></div>


</section><!--Content_all-->
