
<section class="content_all">


  <!--Contenidos Sitio-->
  <section class="cont_home">           

    <section class="bg_portada"> 
      <div class="cont_portada">  
          <div class="img_perfil animated flipInY left">
            <img src="<?php echo base_url() ?>uploads/<?php echo $info->image; ?>">
          </div>
          <div class="info left">  
            <h3><?php echo $info->first_name.' '.$info->last_name; ?></h3>
            <p>
               <!--<span class="m_r_10">UX/UI Director</span>-->
              <small><i class="fa fa-map-marker m_r_10"></i><?php echo $info->address; ?></small>
            </p>
            <div class="redesSociales">
              <a href="" target="_blank"><i class="fa fa-facebook"></i></a>
              <a href="" target="_blank"><i class="fa fa-twitter"></i></a>
              <a href="" target="_blank"><i class="fa fa-google-plus"></i></a>
              <a href="" target="_blank"><i class="fa fa-linkedin"></i></a>
            </div>
          </div>
      </div>
      <div class="mask"></div>
      <div class="img_bg" style="background-image:url('../back/assets/images/user/3.jpg')"></div>
    </section>
    <section class="action_profile m_b_20">  
      <div class="large-6 medium-6 small-6 columns padd_all">
        <ul class="tab_p">  
            <a href="javascript:void(0)" class="active">Profile date</a>
            <!-- <a href="javascript:void(0)">Stream</a> -->
        </ul>
      </div>
      <div class="large-6 medium-6 small-6 columns padd_all">
        <ul class="txt_right">
          <li class="inline txt_center padding"> 
              <span class="block">34</span>
              <small class="text_small block">Activities</small>
          </li>
        </ul>
      </div>
      <div class="clear"> </div>
    </section>

      <section class="conten_inicial">

          
          
          <section class="large-12 medium-12 small-12 columns marco_reportes">
            <section class="bg_perfil_user">

                <div class="datos_user large-12 medium-12 small-12 columns">
                    <div class="large-7 medium-7 small-12 columns bor_right">

                       <div class="datos_edit">
                          <form action="cms/users/edit" class="elem_blancos edit_user" id="formu_user_edit">
                <input type="hidden" name="id"  id="id" value="<?php echo $info->id; ?>" >
                            <section class="row">
                              <div class="small-6 meidum-6 large-6 columns padding">
                                <div class="small-3 medium-3 large-3 columns">
                                  <label class="lab_d">Name</label>
                                </div>
                                <div class="small-9 medium-10 large-9 columns top_tip">
                                  <input type="text" name="nombre"  id="nombre" value="<?php echo $info->first_name; ?>" disabled>
                                </div>
                              </div>
                              <div class="small-6 meidum-6 large-6 columns padding">
                                <div class="small-3 medium-2 large-3 columns">
                                  <label class="lab_d">Last name</label>
                                </div>
                                <div class="small-9 medium-10 large-9 columns top_tip">
                                  <input type="text"  name="lastname" id="lastname" value="<?php echo $info->last_name; ?>" disabled>
                                </div>
                              </div>
                              <div class="small-6 meidum-6 large-6 columns padding">
                                <div class="small-3 medium-2 large-3 columns">
                                  <label class="lab_d">Document</label>
                                </div>
                                <div class="small-9 medium-10 large-9 columns top_tip">
                                  <input type="number" name="document" id="document" value="<?php echo $info->documento; ?>" disabled>
                                </div>
                              </div>                              
                              <div class="small-6 meidum-6 large-6 columns padding">
                                <div class="small-3 medium-3 large-3 columns">
                                  <label class="lab_d">Email</label>
                                </div>
                                <div class="small-9 medium-9 large-9 columns top_tip">
                                  <input type="email" name="email" id="email" value="<?php echo $info->email; ?>" disabled>
                                </div>
                              </div>                              
                              <div class="small-6 meidum-6 large-6 columns padding">
                                <div class="small-3 medium-2 large-3 columns">
                                  <label class="lab_d">Phone</label>
                                </div>
                                <div class="small-9 medium-10 large-9 columns top_tip">
                                  <input type="number" name="phone" id="phone" value="<?php echo $info->phone; ?>" disabled>
                                </div>
                              </div>
                              <div class="small-6 meidum-6 large-6 columns padding">
                                <div class="small-3 medium-2 large-3 columns">
                                  <label class="lab_d">Movil</label>
                                </div>
                                <div class="small-9 medium-10 large-9 columns top_tip">
                                  <input type="number" name="movil" id="movil" value="<?php echo $info->movil; ?>" disabled>
                                </div>
                              </div>
                              <div class="small-6 meidum-6 large-6 columns padding">
                                <div class="small-3 medium-2 large-3 columns">
                                  <label class="lab_d">Address</label>
                                </div>
                                <div class="small-9 medium-10 large-9 columns top_tip">
                                  <input type="text" name="adress" id="adress" value="<?php echo $info->address; ?>" disabled>
                                </div>
                              </div>
                              <div class="small-6 meidum-6 large-6 columns padding">
                                <div class="small-3 medium-2 large-3 columns">
                                  <label class="lab_d">Type user</label>
                                </div>
                                <div class="small-9 medium-10 large-9 columns top_tip">
                                  <input type="text" value="Vendedor lider" disabled>
                                </div>
                              </div>
                              <div class="small-12 meidum-12 large-12 columns padding">
                                <!-- <div class="small-12 medium-12 large-12 columns">
                                    <label class="lab_d">Image profile</label>
                                </div> -->
                                 <div class="clear"></div>
                                <div class="clearfix file">                      
                                  <div class="button-group"> 
                                    <a href="#" id="browse" class="btn_border_gris adjuntar"><i class="fa fa-cloud-upload"></i> Change profile picture</a>
                                  </div>
                                  <input type="file" id="test" name="file" class="file">
                                  <input type="text" class="valorfile" value="Name file select..." disabled>
                                </div> 
                              </div>
                            </section>    
                            <div class="row padding">
                              <div class="small-12 medium-12 large-12 columns">
                                <div class="padding">
                                    <a href="javascript:void(0)" onclick="editar_usuario()" class="btn_border right save_form">Save changes</a>
                                    <a href="javascript:void(0)" class="btn_border right edit_form">Edit profile</a>
                                </div>
                              </div>
                            </div>
                          </form>
                       </div>
                    </div>
                    <div class="large-5 medium-5 small-12 columns padding edit_contraseña">
                       <section class="large-11 medium-11 small-12 columns padding">
                          <h3 class="padding">Cambiar Contraseña</h3>
                          <form action="" class="elem_blancos">

                                             <section class="row">
                                <div class="small-12 meidum-12 large-12 columns padding">
                                  <div class="small-3 medium-2 large-3 columns">
                                   <input type="hidden" name="sal"  id="sal" value="<?php echo $info->salt; ?>" >

                                    <label class="lab_d">Contraseña actual</label>
                                  </div>
                                  <div class="small-9 medium-10 large-9 columns">
                                    <input class="validate[required]" value="sdadsadass" type="password" name="password_anterior" id="form-validation-field-0">
                                  </div>
                                </div>
                                <div class="small-12 meidum-12 large-12 columns padding">
                                  <div class="small-3 medium-2 large-3 columns">
                                    <label class="lab_d">Nueva contraseña</label>
                                  </div>
                                  <div class="small-9 medium-10 large-9 columns">
                                    <input class="validate[required]" value="sdadsadass" type="password" name="password" id="password">
                                  </div>
                                </div>
                                <div class="small-12 meidum-12 large-12 columns padding">
                                  <div class="small-3 medium-2 large-3 columns">
                                    <label class="lab_d">Confirmar contraseña</label>
                                  </div>
                                  <div class="small-9 medium-10 large-9 columns">
                                    <input class="validate[required,equals[password]]" value="sdadsadass" type="password" name="password2" id="password2">
                                  </div>
                                </div>                                
                              </section>    
                              <div class="row padding">
                                <div class="small-12 medium-12 large-12 columns">
                                  <div class="padding">
                                      <a type="submit" class="btn_border right" onclick="cambiarcon()">Guardar contraseña</a>
                                  </div>
                                </div>
                              </div>
                            </form>
                        </section>    
                    </div>
                </div>

                <div class="clear"></div>
            </section>
              <div class="clear"></div>
          </section>                  
         
      </section>     

      <div class="clear"></div>
  </section>
  <!--Fin Contenidos Sitio-->

    <div class="clear"></div>


</section><!--Content_all-->
<script type="text/javascript">
    
    function editar_usuario(){
           /* var id = $('.edit_user #id').val();
            var nombre = $('.edit_user #nombre').val();
            var lastname = $('.edit_user #lastname').val();
            var documento = $('.edit_user #document').val();
            var email = $('.edit_user #email').val();
            var phone = $('.edit_user #phone').val();
            var movil = $('.edit_user #movil').val();
            var adress = $('.edit_user #adress').val();
            console.log('nombre: '+nombre);*/
			
			 var datos = new FormData($("#formu_user_edit")[0]);
       

             $.ajax({
                url: "<?php echo base_url() ?>cms/users/edit", 
                data: datos,
                type: "POST",
                contentType: false,
                processData: false,
                success: function(data) {
					location.reload();
                  return data; 
                }
              });
    }

    function cambiarcon(){

        var id_contra = $('#id').val();
        var contrasena = $('#password').val();
        var sal = $('#sal').val();

               $.ajax({
                url: "<?php echo base_url() ?>cms/users/edit_contrasena", 
                data: {id: id_contra , contrasena: contrasena  , sal: sal},
                type: "POST",
                dataType: "json",
                success: function(data) {
                  return data; 
                }
              });
    }

</script>
