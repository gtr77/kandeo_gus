<?php

////////////////////////////////
//@autor: Brayan Acebo
//brayan.acebo@imaginamos.co
//Agencia: imaginamos.com
//Bogotá, Colombia, 2012
////////////////////////////////

class _users extends Back_Controller {

    protected $admin_area = TRUE;

    public function __construct() {
        parent::__construct();
    }

    // ----------------------------------------------------------------------

    public function index($id) {

        $dat = new User();
        $dat->getAliadosById($id);
        $this->_data["info"] = $dat;
        return $this->build('body');
    }

    // ----------------------------------------------------------------------

    /*public function save() {
        $level_id = $this->_get('id');
        $datos = new Level($level_id);

        $type = $this->_get('type');
        $value = intval($this->_get('value'));

        $datos->{$type} = $value;

        $ok = $datos->save();

        return $this->render_json($ok);
    }*/

    public function config(){
        $dat = new User();
        $dat->getUser();
        $this->_data["listado"] = $dat;
        return $this->build('config');
    }
    public function paisessectores(){
        return $this->build('PaisesSectores');
    }
    // ----------------------------------------------------------------------

    public function edit() {
		
		
		     $file = $_FILES["file"];
          $nombre = $file["name"];
          $tipo = $file["type"];
            $ruta_provisional = $file["tmp_name"];
            $size = $file["size"];
       
			
			

      

        $a = new User();
        $datos = array(
            'id' => $this->input->post('id'),               
            'first_name' => $this->input->post('nombre'),                          
            'last_name' => $this->input->post('lastname'),                    
            'documento' => $this->input->post('document'),                    
            'email' => $this->input->post('email'),                    
            'phone' => $this->input->post('phone'),                    
            'movil' => $this->input->post('movil'),                    
            'address' => $this->input->post('adress'),                    
                        
        );
		if($nombre != ""){
			
			$dimensiones = getimagesize($ruta_provisional);
            $width = $dimensiones[0];
            $height = $dimensiones[1];
            $carpeta = "uploads/";
			$src = $carpeta.$nombre;
            move_uploaded_file($ruta_provisional, $src);
			
			$img = array('image' => $nombre);
			$datos = array_merge($datos, $img);
		}
		
		$update = $a->updateUser($datos);
    }
    
   
        public function edit_contrasena() {
      


            $securify = $this->load->library('securify');
            $new_password = $securify($this->input->post('contrasena'));
            
            $new_hash_password = $this->ACL->hash_password($new_password, $this->input->post('sal'));
            
            

        $a = new User();
        $datos = array(
           "id" => $this->input->post('id'),                  
           "password" => $new_hash_password,                  
        );
        $update = $a->updateUser($datos);
        
    }
    
    public function add_user(){
        
		
           $file = $_FILES["file"];
          $nombre = $file["name"];
          $tipo = $file["type"];
            $ruta_provisional = $file["tmp_name"];
            $size = $file["size"];
            $dimensiones = getimagesize($ruta_provisional);
            $width = $dimensiones[0];
            $height = $dimensiones[1];
            $carpeta = "uploads/";
        
         $src = $carpeta.$nombre;
         move_uploaded_file($ruta_provisional, $src);
		   
        $datos = array(
                'username' => $this->input->post('nombre'),
                'pass'=> $this->input->post('pass'),
                'email' => $this->input->post('email'),
                'first_name' => $this->input->post('nombre'),
                'last_name' => $this->input->post('lastname'),
                'phone' => $this->input->post('phone'),
                'documento' => $this->input->post('documento'),
                'movil' => $this->input->post('cell'),
                'adress' => $this->input->post('direccion'),
                'image' => $nombre,
        );
        $a = new User();
        $a->saveUser($datos);
        
    }

    public function deleteUsers(){
        
       $id = $this->input->post('id');
        $a = new User();
       $a->delete_Users($id);
    }
    // ----------------------------------------------------------------------

}
