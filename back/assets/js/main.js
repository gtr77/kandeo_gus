$(document).ready(function(){
	$(".adjuntar").click(function() {
        return e = $(this).parents().eq(1).children("input[type=file].file"), i = $(this).parents().eq(1).children(".valorfile"), $(e).change(function() {
            if ("multiple" == e.attr("multiple")) {
                for (var t = [], n = 0; n < $(this).get(0).files.length; ++n) t.push($(this).get(0).files[n].name);
                i.val(t)
            } else i.val($(this).val()), $(".valorfile").addClass("archivoAdjunto")
        }), $(e).click(), !1
    }), $(".cargar_img").each(function() {
        $(this).find(".file-select").click(function(e) {
            e.preventDefault(), $(this).parents(".cargar_img").find(".file2").click(), $(".cargar_img").removeClass("load"), $(this).parents(".cargar_img").addClass("load")
        }), $(this).find("input[type=file].file-preview").change(function(e) {
            var i = (this.files[0].name.toString(), new FileReader);
            i.onload = function(e) {
                $(".load .file-preview img").attr("src", e.target.result)
            }, i.readAsDataURL(this.files[0]), e.preventDefault()
        })
    });
	$('.cont_activida_categ ul li a div p,.cont_activida_categ ul li a div h2').on('mouseover', function(){

		$(this).parent().siblings().removeClass('nw');
		$(this).closest('ul').siblings('ul').find('.nw').removeClass('nw');
		$(this).closest('a').find('.nw').removeClass('nw');
		$(this).parent().addClass('nw');

		if($('.cont_activida_categ ul li a div').hasClass('nw')){
			var md = $('.md-vw');
			var content = $(this).parent().find('p').text();
			md.remove();
			var nwnode = $('<div class="md-vw">'+content+'</div>').hide();
			var nw = $('.nw');
			nwnode.appendTo(nw).fadeIn(500);
			if($('.cont_activida_categ ul li a div').hasClass('hide_elm')){
				$('.hide_elm').find('.md-vw').hide();
			}
		}
	});

	$('.cont_activida_categ ul li a div p,.cont_activida_categ ul li a div h2').on('mouseleave', function(){
		$('.md-vw').fadeOut();
	});


    // Cambiar box Model 
    function agregarElementos(){
        var nwMoney = '<div class="mon"><div><input type="text" class="name_money" placeholder="Estados Unidos" disabled=""></div><div><input class="price_in" type="text" placeholder="US" disabled=""></div><div class="editar ti-pencil-alt"></div><div class="ti-trash delete"></div></div>';
        var fther = $(this).closest('.item').find('.tipos');
            fther.prepend(nwMoney);
    }   
    function modificarElementos(){
        $(this).closest('.mon').find("div > input:disabled").attr("disabled", false).focus();
        $(this).closest('.mon').find("div > input.name_money").attr("placeholder", "Editar");
        $(this).closest('.mon').find("div > input.price_in").attr("placeholder", "$");
        // $(this).closest('.mon').find(".editar.ti-pencil-alt").removeClass('ti-pencil-alt').addClass('shw').append(checkIcon);
        $(this).closest('.tipos').find(".mon div > input").removeClass('active_input');
        $(this).parent().find("div > input.price_in").addClass('active_input');
        $(this).parent().find("div > input.name_money").addClass('active_input');
    }

    function saveElements(){
        var checkIcon = '<div class="shwlnk"><i class="fa fa-check-square-o" aria-hidden="true"></i></div>';
        if($(this).hasClass('ti-pencil-alt')){
            $(this).removeClass('ti-pencil-alt').append(checkIcon);
        }
        else{
            $(".shwlnk").remove();
            $(this).addClass('ti-pencil-alt');
            console.log('si is ');
            setTimeout(function(){
                $("input").attr("disabled", true).removeClass('active_input');
            },500);

        }
    }

    function removerElementos(){
        $(this).closest('.mon').remove();
    }
    $('.editar').on('click', saveElements);
    $('.new_element').on('click', agregarElementos);
    $('.mon .editar').on('click', modificarElementos);
    $('.ti-trash.delete').on('click', removerElementos);
    //modulo moneda
    

    
    

});



/*GENERADOR FORMULARIOS*/

 function generar_formulario(id_referencia,nombre_formulario,campos){
     
     //EJEMPLO
     
        /*var id_referencia = "crear_sector";
        var nombre_formulario = "Nuevo Sector";
        var campos = Array('nombre|text','imagen|file');*/
           
    var formulario = '';
     formulario += '<section class="modales_form modales_'+id_referencia+' modal_small ">';
        formulario += '<div class="large-12 medium-12 small-12 columns padding titulo_bright">';
            formulario += '<div class="titulo_sec">';
                formulario += '<h1>'+nombre_formulario+'</h1>';
                formulario += '<a href="javascript:void(0)" class="btn_gris close_inicial"></a>';
    
            formulario += '<div class="oganizaciones_h">';
                formulario += '<section class="padding">';
                    formulario += '<form action="" class="elem_blancos form_new_user" id="formu_'+id_referencia+'">';
    
                         formulario += '<section class="row">';
                             formulario += '<div class="small-12 meidum-12 large-12 columns">';
           
           
        var nuemro_campos = campos.length;
         for (i = 0; i < nuemro_campos; i++) { 
            var campo = campos[i].split('|');

             console.log('campo_nombre: '+campo[0]);
             console.log('campo_tipo: '+campo[1]);
             
             if(campo[1] == "text"){
                     
                 if(campo[2]){
                          formulario += '  <input type="text" value="'+campo[2]+'" name="'+campo[0]+'" id="'+campo[0]+'" onblur="if(this.value==\'\') this.value=\''+campo[0]+'\'" onfocus="if(this.value ==\''+campo[0]+'\' ) this.value=\'\'">';
                 }else{
                          formulario += '  <input type="text" value="'+campo[0]+'" name="'+campo[0]+'" id="'+campo[0]+'" onblur="if(this.value==\'\') this.value=\''+campo[0]+'\'" onfocus="if(this.value ==\''+campo[0]+'\' ) this.value=\'\'">';
                 }
                        
             }else if(campo[1] == "hidden"){
                 formulario += ' <input type="'+campo[1]+'" name="'+campo[0]+'" id="'+campo[0]+'" value="'+campo[2]+'">';
             }else if(campo[1] == "textarea"){
                 formulario += ' <textarea  name="'+campo[0]+'" id="'+campo[0]+'">'+campo[0]+'</textarea>';
             }else if(campo[1] == "file"){ 
                 
                 
                             formulario += '<section class="cargar_img">';
                             formulario += '<div class="form-item"> ';
                             formulario += '<div class="file-preview">';
    
                             formulario += '<a href="#" title="Clic para insertar imagen" class="file-select">';
                             formulario += '<span><i class="ti-image"></i></span>';
                             formulario += '</a>';
                             formulario += '<figure>';
                             formulario += '<img src="'+window.location.href+'../../../back/assets/images/icons/cargar_img.png"/>';
                             formulario += '</figure>';
                             formulario += '<input name="file" type="file" name="'+campo[0]+'" id="'+campo[0]+'" class="file2 file-preview" />';
                             formulario += '</div>';
    
                               // formulario += '<input name="file" type="file" name="foto_pais" class="file2 file-preview" />';
    
                             formulario += '</div>';
                             formulario += '</section>';
                 
             }else{
                 formulario += ' <input type="'+campo[1]+'" name="'+campo[0]+'" id="'+campo[0]+'" value="'+campo[2]+'">';
             }//if file
         }//for
                     
    

    
                         formulario += '</div>';
                         formulario += '</section>';
    
    
                         formulario += '<div class="clear"></div>';
                         formulario += '<div class="small-12 medium-12 large-12 columns">';
                         formulario += '<a type="submit" class="btnG right m_l_10" onclick="'+id_referencia+'()">Guardar</a>';
                         formulario += '</div>';
    
 
    
                     formulario += '<div class="clear"></div>';
                     formulario += '</form>';
                     formulario += '<div class="clear"></div>';
                formulario += '</section>';
            formulario += '</div>';
    
            formulario += '<div class="clear"></div>';
    
            formulario += '</div>';
        formulario += '</div>';
    formulario += '</section>';

         setTimeout(function(){
             $('.modal_generado').html(formulario);
        }, 80);
 
        //ABRIR MODAL
        setTimeout(function(){
            // $('.modales_crear_prueba').addClass('open_modal_form');
             $(".modales_"+id_referencia).toggleClass("open_modal_form"), $(".mask_modalForm").toggleClass("open");
            
                    //AGREGAR EVENTO DE CERRADO
        $(".close_inicial").click(function(e) {
            $(this).parent().parent().parent().removeClass("open_modal_form"), $(".conten_inicial").removeClass("efect_blur"), $(".mask_modalForm").removeClass("open"), $(".acor_template li ul.open .checkboxS input").removeAttr("checked")
        });
            
            //ABRIR IMAGEN
            
         $(".cargar_img").each(function() {
            $(this).find(".file-select").click(function(e) {
                e.preventDefault(), $(this).parents(".cargar_img").find(".file2").click(), $(".cargar_img").removeClass("load"), $(this).parents(".cargar_img").addClass("load")
            }), $(this).find("input[type=file].file-preview").change(function(e) {
                var i = (this.files[0].name.toString(), new FileReader);
                i.onload = function(e) {
                    $(".load .file-preview img").attr("src", e.target.result)
                }, i.readAsDataURL(this.files[0]), e.preventDefault()
            })
        });
            
            
        }, 100);
        
     
   

        
        


    
}
    



