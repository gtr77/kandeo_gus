<?php

////////////////////////////////
//@autor: Brayan Acebo
//brayan.acebo@imaginamos.co
//Agencia: imaginamos.com
//Bogotá, Colombia, 2012
////////////////////////////////

class _sectores extends Back_Controller {

    protected $admin_area = TRUE;

    public function __construct() {
        parent::__construct();
    }

    // ----------------------------------------------------------------------



    public function add_sectores(){
    
        $datos = array(
                'nombre' => $this->input->post('nombre')
        );
        $a = new Sectores();
        $a->saveSectores($datos);
        
    }
    
    public function edit_sectores(){

        $a = new Sectores();
        $datos = array(
           "id" => $this->input->post('id'),                  
           "nombre" => $this->input->post('nombre')       
        );
        $update = $a->updateSectores($datos);
    }

     public function deleteSectores(){
        
       $id = $this->input->post('id');
        $a = new Sectores();
       $a->delete_Sectores($id);
    }
}
