
<section class="content_all">



    <!--Contenidos Sitio-->
    <section class="cont_home">           

      <section class="conten_inicial">

        <!---Titulo-->
         <section class="titulo">
             <div class="large-4 medium-4 small-4 columns">
               <h1 class="left">Configuración</h1>               
             </div>
             <div class="subnav_compa large-4 medium-4 small-4 columns">
                <nav>
                    <!--Items tab-->
                    <ul class=" item_detalle">
                        <li><a href="<?php echo base_url() ?>cms/users/config" class="bottom_tip" data-tips="Usuarios"><span class="ti-user"></span></a></li>
                        <li><a href="<?php echo base_url() ?>cms/permisos" class="bottom_tip" data-tips="Permisos"><span class="ti-lock"></span></a></li>
                        <li><a href="<?php echo base_url() ?>cms/fondos" class="bottom_tip" data-tips="Fondos"><span class="ti-flag-alt-2"></span></a></li>
                        
                        <li><a href="<?php echo base_url() ?>cms/paises/" class="active bottom_tip" data-tips="Paises / Sectores"><span class="ti-agenda"></span></a></li> 
                    <!--Fin Items tab-->
                </nav>
              </div>
              <div class="large-4 medium-4 small-4 columns">
                <a href="javascript:void(0)" class="btn_blanco right m_l_10 new_activity waves-button waves-effect waves-light"><i class="fa fa-sticky-note-o" aria-hidden="true"></i>Nueva Tarea</a>          
                <a href="javascript:void(0)" class="btn_blanco right m_l_10 new_prospect waves-button waves-effect waves-light"><i class="fa fa-building" aria-hidden="true"></i>Nueva Empresa</a>           
                  <div class="clear"></div>
              </div>
               <div class="clear"></div>
          </section>
        <!--Fin Titulo-->                  

        <section class="drag-container">
          <ul class="drag-list listEtapas">
            <li class="drag-column drag-column-on-hold drag-column2">
              <div class="tittleList">
                <a href="#">
                  <span><strong>1</strong></span>
                  <i class="fa fa-flag" aria-hidden="true"></i>
                   
                  <h3>Países</h3>
                     <span class="price new_crear_pais">Nuevo País</span>
                </a>
              </div>
              <ul class=" list_user_register list_paises m_t_20">
                
                  <?php
                  foreach($listado as $li){
                      ?>
                  
            <li class="large-6 medium-5 small-5 column padding animated fadeInUp paisdel<?php echo $li->id; ?>">
                   <div class="bg_panel funts">
                      <section class="left p_r_20 txt_center padd_all">
                        <div class="name_user animated flipInY">
                          <img src="<?php echo base_url() ?>uploads/<?php echo $li->imagen; ?>">
                        </div>
                      </section>
                      <div class="txt_left padd_all left">
                        <div class="per_nombre">
                           <h3><?php echo $li->nombre; ?></h3>
                        </div>  
                      </div>  
                      <div class="right">
                        <div class="action_user">
                          <div class="list_setting"><i class="ti-align-left"></i>
                            <ul>  
                                <li><a href="javascript:void(0)" class="new_paises" onclick="id_editar_pais(<?php echo $li->id; ?>)">Editar</a></li>
                                <li><a href="javascript:void(0)" onclick="id_eliminar_pais(<?php echo $li->id; ?>)">Delete</a></li>
                            </ul>
                          </div> 
                        </div>
                      </div>
                         <div class="clear"></div>
                   </div>
              </li>
                  
                  
                  <?php
                  }
                  ?>

                  
             
                  
                  
              </ul>
            </li>
            
            <li class="drag-column drag-column-on-hold drag-column2">
              <div class="tittleList">
                <a href="prospect.php">
                  <span><strong>2</strong></span>
                  <i class="fa fa-braille" aria-hidden="true"></i>
                  <h3>Sectores</h3>
                  <span class="price">Nuevo Sector</span>
                </a>
              </div>
              <ul class=" list_user_register list_paises m_t_20">
                
                  <?php
                  foreach($sectores as $li){
                      ?>
                  
            <li class="large-6 medium-5 small-5 column padding animated fadeInUp sectordel<?php echo $li->id; ?>">
                   <div class="bg_panel funts">
                    
                      <div class="txt_left padd_all left">
                        <div class="per_nombre">
                           <h3><?php echo $li->nombre; ?></h3>
                        </div>  
                      </div>  
                      <div class="right">
                        <div class="action_user">
                          <div class="list_setting"><i class="ti-align-left"></i>
                            <ul>  
                                <li><a href="javascript:void(0)" class="new_paises" onclick="id_editar_pais(<?php echo $li->id; ?>)">Editar</a></li>
                                <li><a href="javascript:void(0)" onclick="id_eliminar_pais(<?php echo $li->id; ?>)">Delete</a></li>
                            </ul>
                          </div> 
                        </div>
                      </div>
                         <div class="clear"></div>
                   </div>
              </li>
                  
                  
                  <?php
                  }
                  ?>

				</ul>
			  
                  </div>
                </li>
              </ul>
            </li>

          </ul>
        </section>
      
          <div class="clear"></div>
      </section>

        <div class="clear"></div>
    </section>
    <!--Fin Contenidos Sitio-->

    <div class="clear"></div>


</section><!--Content_all-->
<script>
    

       
  function add_pais(){
      
      
         
           var datos = new FormData($("#formu_paises")[0]);
     
      
             $.ajax({
                url: "<?php echo base_url() ?>cms/paises/add_paises", 
                data: datos,
                type: "POST",
                contentType: false,
                processData: false,

                success: function(data) {
                  location.reload();
                  return data; 
                }
              });
    }
    
    function id_editar_pais(id){
        
        $("#formu_paises_edit").append('<input type="hidden" name="id" id="id" value="'+id+'">');
    }
    
    function editar_pais(){
        
        var datos = new FormData($("#formu_paises_edit")[0]);
        
      
             $.ajax({
                url: "<?php echo base_url() ?>cms/paises/edit_paises", 
                data: datos,
                type: "POST",
                contentType: false,
                processData: false,

                success: function(data) {
                  location.reload();
                  return data; 
                }
              });
        
    }
    
    function id_eliminar_pais(id){

            $.ajax({
                url: "<?php echo base_url() ?>cms/paises/deletePaises", 
                data: {id: id} ,
                type: "POST",
                dataType: "json",
                success: function(data) {
                    $('.paisdel'+id).css({'display':'none'});
                  return data; 
                }
              });
    }
 
</script>
