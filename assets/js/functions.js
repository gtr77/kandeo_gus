/*
Author: Jhon Moreno
Author URL: jhonmo09@gmail.com/
*/

$(window).load(function(e) {
	var winHeight= $(window).innerHeight();
	var winWidth = $(window).innerWidth();
	var mainWidth = $('.main_content').width();
    $('#loader').fadeOut('slow');
	var slideWidth = $('.slider').width();
	$('.slide_caption').css('font-size',slideWidth*0.004+'em');
	setTimeout(function(){
		var sliderHeight = $('.slider').height();
		if($('.content_main_slider').size()>0){
			$('.menu_desk').height(sliderHeight-15);
			$('.content_menu_desk, .content_head').height(sliderHeight);
			$('.item_dest_head').height((sliderHeight/3)-4);
		}else{
			$('.menu_desk').height(winHeight-15);
			$('.content_head').height(0);
		}
		var destImgWidth= $('.item_dest_head img').width();
		$('.item_dest_head').width(destImgWidth);
		$('.info_item_dest').css('font-size',slideWidth*0.0010+'em');
		var imgNoticHeight = $('.notic_home img').height();
		$('.dest_resp').height(imgNoticHeight)
		
	},20)
	
	var itemWidth = $('.item_dest_home').width();
	$('.info_dest_home').css('font-size',itemWidth*0.004+'em');
	$('.content_int').css('min-height',winHeight-25);
	if(winWidth>960){
		setTimeout(function(){
			mainWidth = $('.main_content').width();
			$('.content_int').width(mainWidth-100);
		},20)
	}else{
		$('.content_int').width(mainWidth);
	}
	
	if($('.content_main_slider').size()==0 & mainWidth>960){
		$('.content_head').css('position','fixed');
	}
	
	
});

$(window).resize(function(e) {
	var winHeight= $(window).innerHeight();
	var winWidth = $(window).innerWidth();
    var slideWidth = $('.slider').width();
	var mainWidth = $('.main_content').width();
	$('.slide_caption').css('font-size',slideWidth*0.004+'em');
	var sliderHeight = $('.slider').height();
	if($('.content_main_slider').size()>0){
		$('.menu_desk').height(sliderHeight-15);
		$('.content_menu_desk, .content_head').height(sliderHeight);
		$('.item_dest_head').height((sliderHeight/3)-4);
	}else{
		$('.menu_desk').height(winHeight-15);
		$('.content_head').height(0);
	}
	var destImgWidth= $('.item_dest_head img').width();
	$('.item_dest_head').width(destImgWidth);
	$('.info_item_dest').css('font-size',slideWidth*0.0010+'em');
	var imgNoticHeight = $('.notic_home img').height();
	$('.dest_resp').height(imgNoticHeight)
	
	if(slideWidth<=960 & menu==1){
		menu=0
		$('.contenido_menu_desk').width(0);
		$('.content_menu_desk').removeClass('content_menu_abierto');
		$('.menu_desk').removeClass('menu_abierto');
		$('.icon_menu').text('Menú');
	}
	var itemWidth = $('.item_dest_home').width();
	$('.info_dest_home').css('font-size',itemWidth*0.004+'em');
	
	$('.content_int').css('min-height',winHeight-25);
	if(winWidth>960){
		
		$('.content_int').width(mainWidth-100);
	}else{
		$('.content_int').width(mainWidth);
	}
	
	if($('.content_main_slider').size()==0 & mainWidth>960){
		$('.content_head').css('position','fixed');
	}else{
		$('.content_head').css('position','absolute');
	}
});

var menu=0
var over=0

$(document).ready(function(e) {
	if($('.slider').size()>0){
		var sudoSlider = $(".slider").sudoSlider({
			responsive: true,
			prevNext: true,
			numeric:true,
			continuous:true,
			auto:true,
			pause:4500,
			controlsfade:true,
			resumePause:4500,
			speed:1000
		});
	}
	
	if($('.slider2').size()>0){
		var sudoSlider2 = $(".slider2").sudoSlider({
			responsive: true,
			prevNext: true,
			numeric:false,
			continuous:true,
			auto:true,
			pause:4500,
			controlsfade:true,
			resumePause:4500,
			speed:1000
		});
	$(window).on("resize focus", function () {
				var width = $(".slider2").width();
				var orgSlideCount = sudoSlider2.getOption("slideCount");
				var slideCount;
				if (width > 768) {
					slideCount = 4;
				} else if (width > 320) {
					slideCount = 2;
				} else {
					slideCount = 1;
				}
				if (slideCount != orgSlideCount) {
					sudoSlider2.setOption("slideCount", slideCount);
				}
			}).resize();
	}
	
	if($('.menu_desk').hasClass('menu_abierto')){
		$('.logo_desk').attr('src','assets/img/logo_desk_blanco.png');
		
	}
	
	$('.icon_menu').click(function(e) {
        if(menu==0){
			$('.contenido_menu_desk').stop().animate({width:294},500);
			$('.menu_desk').addClass('menu_abierto');
			$('.content_menu_desk').addClass('content_menu_abierto');
			$('.icon_menu').text('Cerrar');
			menu=1
		}else{
			$('.contenido_menu_desk').stop().animate({width:0},500,function(){
				$('.content_menu_desk').removeClass('content_menu_abierto');
			});
			$('.menu_desk').removeClass('menu_abierto');
			
			$('.icon_menu').text('Menú');
			menu=0
		}
    });
	
	$('.btn_menu_resp').click(function(e) {
        if(menu==0){
			$('.menu_resp').fadeIn(300);
			$(this).addClass('menu_resp_abierto');
			menu=1
		}else{
			$('.menu_resp').fadeOut(300);
			$(this).removeClass('menu_resp_abierto');
			menu=0
		}
    });
	
	$('.item_dest_home').hover(function(e) {
        if(over==0){
			$(this).children('.over_dest_home').stop().fadeIn(300);
			over=1
		}else{
			$(this).children('.over_dest_home').stop().fadeOut(300);
			over=0
		}
    });
	
	$('.footer_ahorranito').ahorranito({
		type:1,
		fontColor1:'#fff'
	});
	
	$('.content_acord').hide();
	$('.btn_acord').click(function(e) {
		if( $(this).parent().children('.content_acord').is(":hidden") ){
			$('.content_acord').slideUp(300);
			$('.btn_acord').removeClass('btn_acord_abierto');
			$(this).addClass('btn_acord_abierto');
			$(this).parent().children('.content_acord').slideDown(300);
			$('.indic_btn_acord').removeClass('item_abierto');
			$('.indic_btn_acord').addClass('item_cerrado');
			$(this).children('.indic_btn_acord').removeClass('item_cerrado');
			$(this).children('.indic_btn_acord').addClass('item_abierto');
			
		}else{
			$(this).parent().children('.content_acord').slideUp(300);
			$(this).children('.indic_btn_acord').removeClass('item_abierto');
		    $(this).children('.indic_btn_acord').addClass('item_cerrado');
			$(this).removeClass('btn_acord_abierto');
			
		}	
     });
    if($('.form_contacto').size()>0){
	   	$('input[placeholder],textarea[placeholder]').placeholder();
		$('.form_contacto').validationEngine({
			promptPosition: "topLeft"
		});
	}
	
	if($("a[rel^='prettyPhoto']").size()>0){
		$("a[rel^='prettyPhoto']").prettyPhoto({
			theme:'light_square',
			social_tools:false
		 });
		
	}
    
	$('.thumb_gal1').click(function(e) {
        var thisSrc = $(this).attr('src');
		$('.main_img').fadeOut(300,function(){
			$(this).attr('src',thisSrc).fadeIn();
		});
		
    });
	
	$('.thumbs_gal_notic img').click(function(e) {
        var thisSrc = $(this).attr('src');
		$('.main_img_notic').fadeOut(300,function(){
			$(this).attr('src',thisSrc).fadeIn();
		});
		
    });
	
});



