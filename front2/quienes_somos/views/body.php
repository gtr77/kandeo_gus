<div class="main_content">
  <div class="content_int">
    <img class="main_img" src="<?php echo base_url(); ?>uploads/quienes_banner/new/<?php echo $quienes_ban->imagen; ?>" />
    <div class="contenido_int">
        
      <?php foreach ($quienes as $q): ?>
      <div class="item_acord">
        <div class="btn_acord">
          <h3 class="left mic"><?php echo $q->titulo; ?></h3>
          <div class="indic_btn_acord item_cerrado"></div>
        </div>
        <div class="content_acord">
          <div class="clearfix">
            <?php if ($q->imagen != ''){ ?>
            <img src="<?php echo base_url(); ?>uploads/quienes/new/<?php echo $q->imagen; ?>" class="left" />
            <?php } ?>
            <p class="main_p">
            <?php echo nl2br($q->texto); ?>
            </p>
          </div>
        </div>
      </div>
      <?php endforeach; ?>
    </div>
  </div>
</div>