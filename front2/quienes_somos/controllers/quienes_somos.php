<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author David Perez
 */
class Quienes_somos extends Front_Controller {

    public function __construct() {
        parent::__construct();
    }

    // ----------------------------------------------------------------------
    
    public function index() {
        $this->set_title(SITENAME, true);
        $this->load->model(array(
                CMSPREFIX."inicio/footer",
                CMSPREFIX."quienes/quienes",
                CMSPREFIX."quienes/quienes_banner",
            )
        );  
        
        $footer = new Footer();
        $footer->getFooter();
        $this->_data['footer'] = $footer; 
        
        $quienes_ban = new Quienes_banner();
        $quienes_ban->getBanner();
        $this->_data['quienes_ban'] = $quienes_ban;
        
        $quienes = new Quienes();
        $quienes->getQuienes();
        $this->_data['quienes'] = $quienes;
        
        $this->_data['seccion'] = 'quienes';  
        $this->build();                  
    }   
}