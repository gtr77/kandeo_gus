<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author David Perez
 */
class Categoria extends Front_Controller {

    public function __construct() {
        parent::__construct();
    }

    // ----------------------------------------------------------------------
    
    public function index($paginaActual = 0) {
        $this->set_title(SITENAME, true);
        $this->load->model(array(
                 CMSPREFIX."inicio/footer",
                 CMSPREFIX."proyectos/categorias",
             )
         );
        
        $cantidadPoPagina = 8;
        
        $footer = new Footer();
        $footer->getFooter();
        $this->_data['footer'] = $footer;
        
        $categorias = new Categorias();
        $cantidad = $categorias->count();
        $registroActual = $cantidadPoPagina * $paginaActual;
        $categorias->limit($cantidadPoPagina,$registroActual);
        $categorias->getCategorias();
        $this->_data['categorias'] = $categorias;  
        
        $paginador = '<div class="paginador">';
        $cantidadPaginas = ceil($cantidad / $cantidadPoPagina);
        for ($i = 0; $i < $cantidadPaginas; $i++) {
            if($paginaActual == $i){ 
                $paginador .='<a class="pagina_activa">'.($i + 1).'</a>';
            }else{ 
                $paginador .='<a href="'.  base_url() .'categoria/index/'.$i.'">'. ($i + 1).'</a>';
          }
        }
        $paginador .= '</div>';
        
        $this->_data['paginador'] = $paginador;
        $this->_data['seccion'] = 'categoria';  
        $this->build();                  
    }   
    
    public function proyecto($id_categoria = '', $paginaActual = 0){
        $this->set_title(SITENAME, true);
        $this->load->helper('text');
        $this->load->model(array(
                 CMSPREFIX."inicio/footer",
                 CMSPREFIX."proyectos/proyectos",
                 CMSPREFIX."proyectos/categorias",
             )
         );
        
        $cantidadPoPagina = 8;
        
        $footer = new Footer();
        $footer->getFooter();
        $this->_data['footer'] = $footer;
        
        $categoria = new Categorias();
        $categoria->getCategoriasById($id_categoria);
        $this->_data['categoria'] = $categoria;
        
        $proyecto = new Proyectos();
        $cantidad = $proyecto->where('id_categoria = ', $id_categoria)->count();
        $registroActual = $cantidadPoPagina * $paginaActual;
        $proyecto->limit($cantidadPoPagina,$registroActual);
        $proyecto->getProyectosByCat($id_categoria);
        $this->_data['proyecto'] = $proyecto;  
        
        $paginador = '<div class="paginador">';
         $cantidadPaginas = ceil($cantidad / $cantidadPoPagina);
         for ($i = 0; $i < $cantidadPaginas; $i++) {
            if($paginaActual == $i){ 
                $paginador .='<a class="pagina_activa">'.($i + 1).'</a>';
            }else{ 
                $paginador .='<a href="'.  base_url() .'categoria/proyecto/'.$id_categoria.'/'.$i.'">'. ($i + 1).'</a>';
          }
        }
        $paginador .= '</div>';
        
        $this->_data['paginador'] = $paginador;
        $this->_data['seccion'] = 'proyecto';
        $this->build('proyecto');                  
    }
    
    public function detalle_proyecto($id = ''){
        $this->load->model(array(
                 CMSPREFIX."inicio/footer",
                 CMSPREFIX."proyectos/proyectos",
                 CMSPREFIX."proyectos/proyectos_banner",
                 CMSPREFIX."proyectos/btn_cotizar",
             )
         );
        
        $footer = new Footer();
        $footer->getFooter();
        $this->_data['footer'] = $footer;        
        
        $proyecto = new Proyectos();
        $proyecto->getProyectosById($id);
        $this->_data['proyecto'] = $proyecto;
        
        $banner = new Proyectos_banner();
        $banner->getProyectosBanByPro($id);
        $this->_data['banner'] = $banner;
        
        $btn = new Btn_cotizar();
        $btn->getBtn();
        $this->_data['btn'] = $btn;
        
        $this->_data['seccion'] = 'detalle';  
        $this->build('detalle'); 
    }
}