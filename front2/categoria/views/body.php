<div class="main_content">
  <div class="content_int">
    <div class="contenido_int">
      <h1 class="main_title mic">PROYECTOS</h1>
      <div class="lista_proyectos">
        <div class="clearfix">          
          <?php foreach ($categorias as $c): ?>
          <a href="<?php echo base_url(); ?>categoria/proyecto/<?php echo $c->id; ?>" class="item_proyecto left">
            <img src="<?php echo base_url(); ?>uploads/categorias/new/<?php echo $c->imagen; ?>" />
            <div class="info_item_proyecto">
              <h2 class="mic"><?php echo $c->titulo; ?></h2>
              <p>
              <?php echo nl2br($c->texto); ?>
              </p>
              <span></span>
            </div>
          </a>
          <?php endforeach; ?>            
        </div>
        
        <?php echo $paginador; ?>
      </div>
    </div>
  </div>
</div>