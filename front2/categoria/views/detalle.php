<div class="main_content">
  <div class="content_int">
    <div class="contenido_int">
      <h1 class="main_title mic"><?php echo $proyecto->titulo; ?></h1>
      <a href="<?php echo base_url(); ?>categoria/proyecto/<?php echo $proyecto->id_categoria; ?>" class="btn_back mic">&laquo; Volver</a>
      <?php 
      $i = 0;
      foreach ($banner as $b) : 
          if($i == 0){
      ?>
      <img src="<?php echo base_url(); ?>uploads/proyectos_banner/new/<?php echo $b->imagen ?>" class="main_img" />
      <?php
          }
          $i++;
          endforeach; 
      ?>
      <div class="content_slider_thumbs">
        <div class="slider2">
          <?php foreach ($banner as $b): ?>
          <li>
              <img class="thumb_gal1 left" src="<?php echo base_url(); ?>uploads/proyectos_banner/new/<?php echo $b->imagen ?>" />
          </li>
          <?php endforeach; ?>
        </div>
      </div>
      
      <div class="texto_proyecto main_p">
        <?php echo nl2br($proyecto->texto); ?>
      </div>
      
      <div class="clearfix">
        <div class="info_proyecto left">
          <h2 class="mic">Contacto</h2>
          <div class="item_info">
            <h3 class="mic">Teléfonos</h3>
            <p>
            <?php echo $proyecto->telefono; ?>
            </p>
          </div>
          <div class="item_info">
            <h3 class="mic">Dirección</h3>
            <p>
            <?php echo $proyecto->direccion; ?>
            </p>
          </div>
          <div class="item_info">
            <h3 class="mic">Ubicación</h3>
            <p>
            <?php echo $proyecto->ubicacion; ?>
            </p>
          </div>
        </div>
        <div class="right mapa_proyecto">
          <iframe class="mapa"  frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="<?php echo $proyecto->mapa; ?>"></iframe>
        </div>
      </div>
      <div class="clearfix">
        <a rel="prettyPhoto" href="<?php echo $proyecto->video; ?>" class="btn_video mic left">Ver video</a>
        <a href="<?php echo $btn->link; ?>" target="_blank" class="btn_cotiza mic left">Cotizar</a>
        <a href="<?php echo $proyecto->mas_info; ?>" target="_blank" class="btn_vermas1 mic right">Más información</a>
      </div>
    </div>
  </div>
</div>