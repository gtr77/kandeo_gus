<div class="main_content">
  <div class="content_int">
    <div class="contenido_int">
      <h1 class="main_title mic"><?php echo $categoria->titulo; ?></h1>
      <a href="<?php echo base_url(); ?>categoria" class="btn_back mic">&laquo; Volver</a>
      <div class="lista_proyectos">
        <div class="clearfix">
          <?php foreach ($proyecto as $p): ?>
          <a href="<?php echo base_url(); ?>categoria/detalle_proyecto/<?php echo $p->id; ?>" class="item_proyecto left">
              <img src="<?php echo base_url(); ?>uploads/proyectos/new/<?php echo $p->imagen; ?>" />
            <div class="info_item_proyecto">
              <h2 class="mic"><?php echo $p->titulo; ?></h2>
              <p>
              <?php echo character_limiter(nl2br($p->texto), 100); ?>
              </p>
              <span></span>
            </div>
          </a>
          <?php endforeach; ?>
        </div>
        
        <?php echo $paginador; ?>
      </div>
    </div>
  </div>
</div>