<div class="main_content">
  <div class="content_int">
    <img class="main_img" src="<?php echo base_url(); ?>uploads/alianzas_banner/new/<?php echo $banner->imagen; ?>" />
    <div class="contenido_int">
      <h1 class="main_title">Alianzas estratégicas</h1>
      <div class="lista_aliados clearfix">
        <?php foreach ($alianzas as $a) : ?>
        <div class="item_proyecto item_alianza left">
          <img src="<?php echo base_url(); ?>uploads/alianzas/new/<?php echo $a->imagen; ?>" />
          <h2 class="mic"><?php echo $a->titulo; ?></h2>
          <p>
          <?php echo nl2br($a->texto); ?>
          </p>
        </div>
        <?php endforeach; ?>        
      </div>
      <?php echo $paginador; ?>
    </div>
  </div>
</div>