<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author David Perez
 */
class Alianzas extends Front_Controller {

    public function __construct() {
        parent::__construct();
    }

    // ----------------------------------------------------------------------
    
    public function index($paginaActual = 0) {
        $this->set_title(SITENAME, true);
        $this->load->model(array(                 
                 CMSPREFIX."inicio/footer",
                 CMSPREFIX."aliados/aliados",
                 CMSPREFIX."aliados/banner_aliados",
             )
         ); 
        
        $footer = new Footer();
        $footer->getFooter();
        $this->_data['footer'] = $footer;
        
        $banner = new Banner_aliados();
        $banner->getBannerAliados();
        $this->_data['banner'] = $banner; 
        
        $cantidadPoPagina = 8;
        
        $alianzas = new Aliados();
        $cantidad = $alianzas->count();
        $registroActual = $cantidadPoPagina * $paginaActual;
        $alianzas->limit($cantidadPoPagina,$registroActual);
        $alianzas->getAliados();
        $this->_data['alianzas'] = $alianzas;  
        
        $paginador = '<div class="paginador">';
        $cantidadPaginas = ceil($cantidad / $cantidadPoPagina);
        for ($i = 0; $i < $cantidadPaginas; $i++) {
            if($paginaActual == $i){ 
                $paginador .='<a class="pagina_activa">'.($i + 1).'</a>';
            }else{ 
                $paginador .='<a href="'.  base_url() .'alianzas/index/'.$i.'">'. ($i + 1).'</a>';
          }
        }
        $paginador .= '</div>';
        
        $this->_data['paginador'] = $paginador;
        
        $this->_data['seccion'] = 'alianzas';  
        $this->build();                  
    }   
}