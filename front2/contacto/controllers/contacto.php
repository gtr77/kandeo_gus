<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author David Perez
 */
class Contacto extends Front_Controller {

    public function __construct() {
        parent::__construct();
    }

    // ----------------------------------------------------------------------
    
    public function index($not = 0) {
        $this->set_title(SITENAME, true);
        $this->load->model(array(
                 CMSPREFIX."inicio/footer",
                 CMSPREFIX."contactenos/contactenos",
             )
         );
        
        $footer = new Footer();
        $footer->getFooter();
        $this->_data['footer'] = $footer;
        
        $contacto = new Contactenos();
        $contacto->getContactenos();
        $this->_data['contacto'] = $contacto;
        $this->_data['not'] = $not;
        
        $this->_data['seccion'] = 'contacto';  
        $this->build();                  
    }   
    
    public function enviar(){
        $this->load->model(array(
                CMSPREFIX."contactenos/contactenos",
            )
        ); 
        
        $c = new Contactenos();
        $c->getContactenos();
        $correo = $c->mail;
        
        
        $para  = $correo;        
        $titulo = 'Contacto desde tu pagina BLP';

        // message
        $mensaje = '
        <html>
        <head>
          <title>Mensaje de contacto</title>
        </head>
        <body>
          <table>
            <tr>
              <th>Campo</th>
              <th>Informacion</th>
            </tr>
            <tr>
              <td>Nombre</td>
              <td>'.$this->limpiarCadena($this->input->post('nombre')).'</td>
            </tr>            
            <tr>
              <td>Correo</td>
              <td>'.$this->limpiarCadena($this->input->post('correo')).'</td>
            </tr>            
            <tr>
              <td>Dirección</td>
              <td>'.$this->limpiarCadena($this->input->post('direccion')).'</td>
            </tr>            
            <tr>
              <td>Teléfono</td>
              <td>'.$this->limpiarCadena($this->input->post('telefono')).'</td>
            </tr>           
            <tr>
              <td>Comentarios</td>
              <td>'.$this->limpiarCadena($this->input->post('comentarios')).'</td>
            </tr>            
          </table>
        </body>
        </html>
        ';

        $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
        $cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        
        $cabeceras .= 'To: Admin <admin@blp.com>' . "\r\n";
        $cabeceras .= 'From: Admin <admin@blp.com>' . "\r\n";

        mail($para, $titulo, $mensaje, $cabeceras);       
        return redirect(base_url().'contacto/index/1');
    }
    
    public function limpiarCadena($valor) {
        $valor = str_ireplace("SELECT", "", $valor);
        $valor = str_ireplace("COPY", "", $valor);
        $valor = str_ireplace("DELETE", "", $valor);
        $valor = str_ireplace("DROP", "", $valor);
        $valor = str_ireplace("DUMP", "", $valor);
        $valor = str_ireplace(" OR ", "", $valor);
        $valor = str_ireplace("%", "", $valor);
        $valor = str_ireplace("LIKE", "", $valor);
        $valor = str_ireplace("--", "", $valor);
        $valor = str_ireplace("^", "", $valor);
        $valor = str_ireplace("[", "", $valor);
        $valor = str_ireplace("]", "", $valor);
        $valor = str_ireplace("\\", "", $valor);
        $valor = str_ireplace("!", "", $valor);
        $valor = str_ireplace("¡", "", $valor);
        $valor = str_ireplace("?", "", $valor);
        $valor = str_ireplace("=", "", $valor);
        $valor = str_ireplace("&", "", $valor);
        return $valor;
    }
}