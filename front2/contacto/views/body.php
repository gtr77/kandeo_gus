<div class="main_content">
  <div class="content_int">
    <div class="contenido_int">
      <h1 class="main_title">Contacto</h1>
      <div class="clearfix">
        <img src="<?php echo base_url(); ?>uploads/contacto/new/<?php echo $contacto->imagen; ?>" class="left img_contacto" />
        <?php echo form_open(base_url().'contacto/enviar', 'class="form_contacto right"'); ?>
          <input type="text" name="nombre" class="input1 validate[required]" placeholder="Nombre" />
          <input type="text" name="correo" class="input1 validate[required,custom[email]]" placeholder="Correo electrónico" />
          <input type="text" name="telefono" class="input1 validate[required]" placeholder="Teléfono" />
          <input type="text" name="direccion" class="input1 validate[required]" placeholder="Dirección" />     
          <textarea class="textarea1" name="comentarios" placeholder="Comentarios"></textarea>
          <input type="submit" class="submit mic" value="Enviar" />
        <?php echo form_close(); ?>
      </div>
    </div>
  </div>
</div>
<script>
<?php if($not == 1){?> alert('Mensaje enviado correctamente'); <?php } ?>
</script>