<div class="main_content">
  <div class="content_int">
    <div class="contenido_int">
      <h1 class="main_title">Galería</h1>
      <div class="clearfix">
        <?php foreach ($galerias as $g): ?>
        <a class="item_proyecto left item_gal" href="<?php echo base_url(); ?>galeria/detalle/<?php echo $g->id; ?>/<?php echo $pagina; ?>">
          <img src="<?php echo base_url(); ?>uploads/galerias/new/<?php echo $g->imagen; ?>" />
          <h2 class="mic"><?php echo $g->titulo; ?></h2>
        </a>
        <?php endforeach; ?>
      </div>
      <?php echo $paginador; ?>
    </div>
  </div>
</div>