<div class="main_content">
  <div class="content_int">
    <div class="contenido_int">
      <div class="main_p texto_proyecto">
        <p>
        <?php echo nl2br($detalle->texto); ?> 
        </p>
      </div>
      <h1 class="main_title">Galería lorem</h1>
      <div class="content_slider_gal">
        <div class="slider">
          <ul>
            <?php foreach ($banner as $b) : ?>
            <li>
                <img src="<?php echo base_url(); ?>uploads/galerias/new/<?php echo $b->imagen; ?>" />
            </li>
            <?php endforeach; ?>
          </ul>
        </div>
      </div>
      
      <div class="clearfix">
        <?php foreach ($galerias as $g): ?>
        <a class="item_proyecto left item_gal" href="<?php echo base_url(); ?>galeria/detalle/<?php echo $g->id; ?>/<?php echo $pagina; ?>">
          <img src="<?php echo base_url(); ?>uploads/galerias/new/<?php echo $g->imagen; ?>" />
          <h2 class="mic"><?php echo $g->titulo; ?></h2>
        </a>
        <?php endforeach; ?>        
      </div>
      <?php echo $paginador; ?>
    </div>
  </div>
</div>