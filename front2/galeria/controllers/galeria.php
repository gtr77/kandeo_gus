<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author David Perez
 */
class Galeria extends Front_Controller {

    public function __construct() {
        parent::__construct();
    }

    // ----------------------------------------------------------------------
    
    public function index($paginaActual = 0) {
        $this->set_title(SITENAME, true);
        $this->load->model(array(
                CMSPREFIX."inicio/footer",
                CMSPREFIX."galerias/galerias",
            )
        );  
        
        $footer = new Footer();
        $footer->getFooter();
        $this->_data['footer'] = $footer;
        
        $cantidadPoPagina = 8;
        
        $galerias = new Galerias();
        $cantidad = $galerias->count();
        $registroActual = $cantidadPoPagina * $paginaActual;
        $galerias->limit($cantidadPoPagina,$registroActual);
        $galerias->getGalerias();
        $this->_data['galerias'] = $galerias;  
        
        $paginador = '<div class="paginador">';
        $cantidadPaginas = ceil($cantidad / $cantidadPoPagina);
        for ($i = 0; $i < $cantidadPaginas; $i++) {
            if($paginaActual == $i){ 
                $paginador .='<a class="pagina_activa">'.($i + 1).'</a>';
            }else{ 
                $paginador .='<a href="'.  base_url() .'galeria/index/'.$i.'">'. ($i + 1).'</a>';
          }
        }
        $paginador .= '</div>';
        
        $this->_data['paginador'] = $paginador;
        $this->_data['pagina'] = $paginaActual;        
        $this->_data['seccion'] = 'galeria';  
        $this->build();                  
    }
    
    public function detalle($id = '', $paginaActual = '') {
        $this->set_title(SITENAME, true);
        $this->load->model(array(
                 CMSPREFIX."inicio/footer",
                 CMSPREFIX."galerias/galerias",
                 CMSPREFIX."galerias/galerias_banner",
             )
        );
        
        $footer = new Footer();
        $footer->getFooter();
        $this->_data['footer'] = $footer;
        
        $banner = new Galerias_banner();
        $banner->getGaleriasBanByGal($id);
        $this->_data['banner'] = $banner;
        
        $detalle = new Galerias();
        $detalle->getGaleriasById($id);
        $this->_data['detalle'] = $detalle;
        
        $cantidadPoPagina = 8;
        
        $galerias = new Galerias();
        $cantidad = $galerias->count();
        $registroActual = $cantidadPoPagina * $paginaActual;
        $galerias->limit($cantidadPoPagina,$registroActual);
        $galerias->getGalerias();
        $this->_data['galerias'] = $galerias;  
        
        $paginador = '<div class="paginador">';
        $cantidadPaginas = ceil($cantidad / $cantidadPoPagina);
        for ($i = 0; $i < $cantidadPaginas; $i++) {
            if($paginaActual == $i){ 
                $paginador .='<a class="pagina_activa">'.($i + 1).'</a>';
            }else{ 
                $paginador .='<a href="'.  base_url() .'galeria/index/'.$i.'">'. ($i + 1).'</a>';
          }
        }
        $paginador .= '</div>';
        $this->_data['pagina'] = $paginaActual;
        $this->_data['paginador'] = $paginador;
        $this->_data['seccion'] = 'galeria_detalle';  
        $this->build('detalle');
    }
}