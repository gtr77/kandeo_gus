<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author David Perez
 */
class Faq extends Front_Controller {

    public function __construct() {
        parent::__construct();
    }

    // ----------------------------------------------------------------------
    
    public function index() {
        $this->set_title(SITENAME, true);
        $this->load->model(array(
                CMSPREFIX."inicio/footer",
                CMSPREFIX."preguntas/preguntas",
                CMSPREFIX."preguntas/preguntas_banner",
            )
        ); 
        
        $footer = new Footer();
        $footer->getFooter();
        $this->_data['footer'] = $footer;
        
        $banner = new Preguntas_banner();
        $banner->getBanner();
        $this->_data['banner'] = $banner;
       
        $preguntas = new Preguntas();
        $preguntas->getPreguntas();
        $this->_data['preguntas'] = $preguntas;
        
        $this->_data['seccion'] = 'faq';  
        $this->build();                  
    }   
}