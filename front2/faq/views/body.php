<div class="main_content">
  <div class="content_int">
    <img class="main_img" src="<?php echo base_url(); ?>uploads/preguntas_banner/new/<?php echo $banner->imagen; ?>" />
    <div class="contenido_int">
      <?php foreach ($preguntas as $p) : ?>
      <div class="item_acord">
        <div class="btn_acord">
          <h3 class="left mic"><?php echo $p->titulo ?></h3>
          <div class="indic_btn_acord item_cerrado"></div>
        </div>
        <div class="content_acord">
          <div class="clearfix">
            <p class="main_p">
                <?php echo nl2br($p->texto); ?>
            </p>
          </div>
        </div>
      </div>
      <?php endforeach; ?>
    </div>
  </div>
</div>