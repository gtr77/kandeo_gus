<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author David Perez
 */
class Home extends Front_Controller {

    public function __construct() {
        parent::__construct();
    }

    // ----------------------------------------------------------------------

    public function index() {
        $this->set_title(SITENAME, true);
        $this->load->model(array(
                /*CMSPREFIX."inicio/banners",
                CMSPREFIX."inicio/footer",
                CMSPREFIX."inicio/noticias_home",
                CMSPREFIX."inicio/proyectos_home",*/
            )
        );
        
        /*$banners = new Banners();
        $banners->getBanners();
        $this->_data['banners'] = $banners; 
        
        $noticias = new Noticias_home();
        $noticias->getNoticiasHome();
        $this->_data['noticias'] = $noticias; 
        
        $proyectos = new Proyectos_home();
        $proyectos->getProyectosHome();
        $this->_data['proyectos'] = $proyectos;
        
        $footer = new Footer();
        $footer->getFooter();
        $this->_data['footer'] = $footer;  */
        
        $this->_data['seccion'] = 'home';
        return $this->build();
    }

    // ----------------------------------------------------------------------
   
}
