<div class="content_head clearfix">
  <div class="main_content">
    <div class="content_menu_desk clearfix left">
      <div class="contenido_menu_desk left">
        <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/img/logo.png" class="logo" /></a>
        <ul class="lista_menu">
          <li>
              <a  class="clearfix" href="<?php echo base_url(); ?>quienes_somos">
              Quiénes Somos <span class="right">01</span>
            </a>
          </li>
          <li>
            <a  class="clearfix" href="<?php echo base_url(); ?>categoria">
              Proyectos <span class="right">02</span>
            </a>
          </li>
          <li>
            <a  class="clearfix" href="#">
              Equipo <span class="right">03</span>
            </a>
          </li>
          <li>
            <a  class="clearfix" href="<?php echo base_url(); ?>alianzas">
              Alianzas estrategicas<span class="right">04</span>
            </a>
          </li>
          <li>
            <a  class="clearfix" href="<?php echo base_url(); ?>faq">
              FAQ <span class="right">05</span>
            </a>
          </li>
          <li>
            <a  class="clearfix" href="<?php echo base_url(); ?>galeria">
              Galería <span class="right">06</span>
            </a>
          </li>
          <li>
            <a  class="clearfix" href="<?php echo base_url(); ?>noticias">
              Noticias y publicaciones <span class="right">07</span>
            </a>
          </li>
          <li>
            <a  class="clearfix" href="<?php echo base_url(); ?>contacto">
              Contacto <span class="right">08</span>
            </a>
          </li>
          
        </ul>
      </div>
      <div class="menu_desk left">
        <a href="javascript:void(0);" class="icon_menu">Menú</a>
        <div class="logo_desk"></div>
      </div>
    </div>
    <div class="content_menu_resp clearfix">
      <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/img/logo.png" class="logo_resp left" width="90" /></a>
      <a class="btn_menu_resp right"></a>
      <div class="menu_resp">
        <ul class="lista_menu">
          <li>
            <a  class="clearfix" href="<?php echo base_url(); ?>quienes_somos">
              Quiénes Somos <span class="right">01</span>
            </a>
          </li>
          <li>
            <a  class="clearfix" href="<?php echo base_url(); ?>categoria">
              Proyectos <span class="right">02</span>
            </a>
          </li>
          <li>
            <a  class="clearfix" href="#">
              Equipo <span class="right">03</span>
            </a>
          </li>
          <li>
            <a  class="clearfix" href="<?php echo base_url(); ?>alianzas">
              Alianzas estratégicas<span class="right">04</span>
            </a>
          </li>
          <li>
            <a  class="clearfix" href="<?php echo base_url(); ?>faq">
              FAQ <span class="right">05</span>
            </a>
          </li>
          <li>
            <a  class="clearfix" href="<?php echo base_url(); ?>galeria">
              Galería <span class="right">06</span>
            </a>
          </li>
          <li>
            <a  class="clearfix" href="<?php echo base_url(); ?>noticias">
              Noticias y publicaciones <span class="right">07</span>
            </a>
          </li>
          <li>
            <a  class="clearfix" href="<?php echo base_url(); ?>contacto">
              Contacto <span class="right">08</span>
            </a>
          </li>
          
        </ul>
      </div>
    </div>
  </div>
</div>