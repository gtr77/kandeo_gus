<div class="footer">
  <div class="main_content">
    <div class="content_footer1 clearfix">
      <div class="left">
        <h3 class="mic">Contáctanos</h3>
        <div class="clearfix">
          <div class="item_cont_footer item_dir left">
            Dirección:<br />
            <?php echo nl2br($footer->direccion); ?>
          </div>
          <div class="item_cont_footer item_tel left">
            Teléfonos:<br />
            <?php echo nl2br($footer->telefono); ?>
          </div>
          <div class="item_cont_footer item_mail left">
            E-mail:<br />
            <?php echo nl2br($footer->mail); ?>
          </div>
        </div>
      </div>
      <a href="index.php?seccion=home"><img src="<?php echo base_url(); ?>assets/img/logo_footer.png" class="right logo_footer" width="280" /></a>
    </div>
    <div class="content_footer2 clearfix">
      <h6 class="left">© 2012 BLP CONSTRUCTORES - Todos los derechos reservados - Prohibida su reproducción parcial o total</h6>
      <div class="redes left">
        <a href="<?php echo nl2br($footer->facebook); ?>" class="fb"></a>
        <a href="<?php echo nl2br($footer->twitter); ?>" class="tw"></a>
        
      </div>
      <div class="footer_ahorranito right"></div>
    </div>
  </div>
</div>