<div class="content_dest_head">
  <div class="main_content">
  <div class="dest_head right">
    <?php foreach ($noticias as $n): ?>
    <div class="item_dest_head">
      <img src="<?php echo base_url(); ?>uploads/noticias_home/new/<?php echo $n->imagen; ?>" class="left" />
      <div class="info_item_dest">
        <h3 class="mic"><?php echo $n->titulo; ?></h3>
        <h4 class="mic"><?php echo $n->subtitulo; ?></h4>
        <p>
        <?php echo nl2br($n->texto); ?>
        </p>
        <a href="<?php echo $n->link; ?>"></a>
      </div>
    </div>
    <?php endforeach; ?>
  </div>
  </div>
</div>






<div class="main_content clearfix">
  <div class="dest_desk clearfix">
    <?php foreach ($proyectos as $p): ?>
    <a href="<?php echo $p->link; ?>" class="item_dest_home left">
      <img src="<?php echo base_url(); ?>uploads/proyectos_home/new/<?php echo $p->imagen; ?>" />
      <div class="over_dest_home clearfix">
        <div class="info_dest_home left">
          <h4 class="mic"><?php echo $p->titulo; ?></h4>
          <h2 class="mic"><?php echo $p->subtitulo; ?></h2>
          <p><?php echo nl2br($p->texto); ?></p>
          <span></span>
        </div>
        <div class="icon_dest_home right"></div>
      </div>
    </a>
    <?php endforeach; ?>    
  </div>
    
  <div class="dest_resp clearfix">
    <?php 
    $i = 0;
    foreach ($noticias as $n): 
        if($i == 0){
    ?>
    <a href="<?php echo $n->link; ?>" class="left notic_home">
      <img src="<?php echo base_url(); ?>uploads/noticias_home/new/<?php echo $n->imagen; ?>" />
      <div class="over_dest_resp mic">
        <div class="tab_over">
          <div class="cell_over">
            NOTICIAS
          </div>
        </div>
      </div>
    </a>
    <?php 
        }
        $i++;
    endforeach; ?>
    <div class="right content_dest_resp">
      <?php 
      $i = 0;
      foreach ($proyectos as $p):
          if($i == 0){
      ?>
      <a class="item_dest_resp" href="<?php echo $p->link; ?>">
        <img src="<?php echo base_url(); ?>uploads/proyectos_home/new/<?php echo $p->imagen; ?>" />
      </a>
      <?php
      }elseif($i == 1){
      ?>
        <a class="item_dest_resp" href="<?php echo $p->link; ?>">
        <div class="over_dest_resp2 mic">
          <div class="tab_over">
            <div class="cell_over">
              PROYECTOS
            </div>
          </div>
        </div>
        <img src="<?php echo base_url(); ?>uploads/proyectos_home/new/<?php echo $p->imagen; ?>" />
      </a>
      <?php
      }
      $i++;
      endforeach;
      ?>
      
    </div>
  </div>
</div>
