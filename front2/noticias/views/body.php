<div class="main_content">
  <div class="content_int">
    <div class="contenido_int">
      <h1 class="main_title">Noticias y publicaciones</h1>
      <?php foreach ($noticias as $n) : ?>
      <div class="item_noticia clearfix">
        <img src="<?php echo base_url(); ?>uploads/noticias/new/<?php echo $n->imagen; ?>" class="left" />
        <h2 class="mic"><?php echo $n->titulo; ?></h2>
        <p class="main_p">
        <?php echo character_limiter(nl2br($n->texto), 300); ?>
        </p>
        <a href="<?php echo base_url(); ?>noticias/detalle/<?php echo $n->id; ?>/<?php echo $pagina; ?>" class="btn_vermas1 mic">Más información</a>
      </div>
      <?php endforeach; ?>
      
      <?php echo $paginador; ?>
    </div>
  </div>
</div>