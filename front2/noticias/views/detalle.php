<div class="main_content">
  <div class="content_int">
    <div class="contenido_int">
      <h1 class="main_title"><?php echo $noticia->titulo; ?></h1>
      <a href="<?php echo base_url(); ?>noticias/index/<?php echo $pagina; ?>" class="btn_back mic">&laquo; Volver</a>
      <div class="clearfix">
        <div class="gal_notic left">
          <?php 
          $i = 0;
          foreach ($banner as $b) : 
              if($i == 0){
          ?>
          <img src="<?php echo base_url(); ?>uploads/noticias/new/<?php echo $b->imagen; ?>" class="main_img_notic" />
          <div class="thumbs_gal_notic">
              <img src="<?php echo base_url(); ?>uploads/noticias/new/<?php echo $b->imagen; ?>" class="left" />
          <?php }elseif($i == 2){ ?>            
            <img src="<?php echo base_url(); ?>uploads/noticias/new/<?php echo $b->imagen; ?>" class="left" style="margin-right: 0;" />
          <?php }else{ ?>
            <img src="<?php echo base_url(); ?>uploads/noticias/new/<?php echo $b->imagen; ?>" class="left" />
          <?php }
            $i++; 
            endforeach;
          ?>            
          </div>
          
        </div>
        <div class="main_p">
            <p>
                <?php echo nl2br($noticia->texto); ?>
            </p>
          </div>
      </div>
    </div>
  </div>
</div>