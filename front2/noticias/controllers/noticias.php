<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author David Perez
 */
class Noticias extends Front_Controller {

    public function __construct() {
        parent::__construct();
    }

    // ----------------------------------------------------------------------
    
    public function index($paginaActual = 0) {
        $this->load->helper('text');
        $this->set_title(SITENAME, true);
        $this->load->model(array(
                CMSPREFIX."inicio/footer",
                CMSPREFIX."news/news",
            )
        );
        
        $footer = new Footer();
        $footer->getFooter();
        $this->_data['footer'] = $footer;
        
        $cantidadPoPagina = 5;
        
        $noticias = new News();
        $cantidad = $noticias->count();
        $registroActual = $cantidadPoPagina * $paginaActual;
        $noticias->limit($cantidadPoPagina,$registroActual);
        $noticias->getNoticias();
        $this->_data['noticias'] = $noticias;  
        
        $paginador = '<div class="paginador">';
        $cantidadPaginas = ceil($cantidad / $cantidadPoPagina);
        for ($i = 0; $i < $cantidadPaginas; $i++) {
            if($paginaActual == $i){ 
                $paginador .='<a class="pagina_activa">'.($i + 1).'</a>';
            }else{ 
                $paginador .='<a href="'.  base_url() .'noticias/index/'.$i.'">'. ($i + 1).'</a>';
          }
        }
        $paginador .= '</div>';
        
        $this->_data['paginador'] = $paginador;
        $this->_data['pagina'] = $paginaActual;
        $this->_data['seccion'] = 'noticias';  
        $this->build();                  
    } 
    
    public function detalle($id = '', $pagina = '') {
        $this->set_title(SITENAME, true);
        $this->load->model(array(
                CMSPREFIX."inicio/footer",
                CMSPREFIX."news/news",
                CMSPREFIX."news/news_banner",
            )
        );
        
        $footer = new Footer();
        $footer->getFooter();
        $this->_data['footer'] = $footer;
        
        $banner = new News_banner();
        $banner->getNewsBanByNew($id);
        $this->_data['banner'] = $banner;
        
        $noticia = new News();
        $noticia->getNoticiasById($id);
        $this->_data['noticia'] = $noticia;
        $this->_data['pagina'] = $pagina;
        
        $this->_data['seccion'] = 'detalle-noticia';  
        $this->build('detalle');   
    }
}