-- Adminer 4.0.3 MySQL dump

SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = '-05:00';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `cms_alianzas`;
CREATE TABLE `cms_alianzas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imagen` varchar(100) NOT NULL,
  `titulo` varchar(20) NOT NULL,
  `texto` varchar(100) NOT NULL,
  `orden_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `cms_alianzas` (`id`, `imagen`, `titulo`, `texto`, `orden_id`) VALUES
(1,	'a301715b9ac50c3b94c8452c5974c53a.jpg',	'Alianza 1',	'Alianza 1Alianza 1Alianza 1Alianza 1Alianza 1Alianza 1Alianza 1Alianza 1v',	1),
(2,	'abb21ba11559e8e14c2fe6ea87d017b7.png',	'Alianza 2',	'Alianza 2Alianza 2Alianza 2Alianza 2Alianza 2Alianza 2Alianza 2Alianza 2',	1);

DROP TABLE IF EXISTS `cms_banners`;
CREATE TABLE `cms_banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imagen` varchar(100) NOT NULL,
  `titulo` varchar(30) NOT NULL,
  `subtitulo` varchar(15) NOT NULL,
  `orden_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `cms_banners` (`id`, `imagen`, `titulo`, `subtitulo`, `orden_id`) VALUES
(1,	'32d7a8f37bd466dbc8cba33c0bce8db5.jpg',	'Imagen 1',	'prueba',	2),
(2,	'02a8c7000679ba276446d6b9be4d1710.jpg',	'Imagen 2',	'prueba 2',	1),
(4,	'74745c655d5df621946f3c97390b660f.jpg',	'Imagen 4',	'prueba 4',	4),
(6,	'de542aba10387deae592e17557d394a0.jpg',	'Imagen 5',	'prueba 5',	5);

DROP TABLE IF EXISTS `cms_ban_alianza`;
CREATE TABLE `cms_ban_alianza` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imagen` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `cms_ban_alianza` (`id`, `imagen`) VALUES
(1,	'd4f80d060e7bebb9a02c1a0741e489ce.jpg');

DROP TABLE IF EXISTS `cms_ban_faqs`;
CREATE TABLE `cms_ban_faqs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imagen` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `cms_ban_faqs` (`id`, `imagen`) VALUES
(1,	'2b530b50eda5911208fe29a85addf672.jpg');

DROP TABLE IF EXISTS `cms_ban_galerias`;
CREATE TABLE `cms_ban_galerias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imagen` varchar(100) NOT NULL,
  `id_galerias` int(11) NOT NULL,
  `orden_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `id_galerias` (`id_galerias`),
  CONSTRAINT `cms_ban_galerias_ibfk_1` FOREIGN KEY (`id_galerias`) REFERENCES `cms_galerias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `cms_ban_galerias` (`id`, `imagen`, `id_galerias`, `orden_id`) VALUES
(1,	'1126c85eb586ad9fd9c09d068ecc94dc.jpg',	1,	1),
(2,	'a849940a5d6754877baf10fdcc8b85c0.jpg',	1,	1),
(3,	'f660e33c67a209b4c8cdab36a3d9ecba.jpg',	2,	1);

DROP TABLE IF EXISTS `cms_ban_noticias`;
CREATE TABLE `cms_ban_noticias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imagen` varchar(100) NOT NULL,
  `id_noticias` int(11) NOT NULL,
  `orden_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `id_noticias` (`id_noticias`),
  CONSTRAINT `cms_ban_noticias_ibfk_1` FOREIGN KEY (`id_noticias`) REFERENCES `cms_noticias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `cms_ban_noticias` (`id`, `imagen`, `id_noticias`, `orden_id`) VALUES
(1,	'65c47bf64b979b470e43d463a973f719.jpg',	1,	3),
(2,	'01b5216edb8089d795f9a3df2c02f02f.png',	1,	2),
(3,	'ba0b2602320f34e626a4156b6eff03f3.png',	1,	1);

DROP TABLE IF EXISTS `cms_ban_proyectos`;
CREATE TABLE `cms_ban_proyectos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imagen` varchar(100) NOT NULL,
  `id_proyecto` int(11) NOT NULL,
  `orden_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `id_proyecto` (`id_proyecto`),
  CONSTRAINT `cms_ban_proyectos_ibfk_1` FOREIGN KEY (`id_proyecto`) REFERENCES `cms_proyectos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `cms_ban_proyectos` (`id`, `imagen`, `id_proyecto`, `orden_id`) VALUES
(1,	'e4f86619ded1daca52b6db12ecba4664.jpg',	1,	1),
(3,	'426b72c44e0668a7687974789253be92.jpg',	1,	1);

DROP TABLE IF EXISTS `cms_ban_quienes`;
CREATE TABLE `cms_ban_quienes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imagen` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `cms_ban_quienes` (`id`, `imagen`) VALUES
(1,	'406265c46c3afd51a4b5b31ffa8bf7fe.jpg');

DROP TABLE IF EXISTS `cms_btn_cotizars`;
CREATE TABLE `cms_btn_cotizars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `cms_btn_cotizars` (`id`, `link`) VALUES
(1,	'http://www.google.com');

DROP TABLE IF EXISTS `cms_categorias`;
CREATE TABLE `cms_categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(15) NOT NULL,
  `texto` varchar(50) NOT NULL,
  `imagen` varchar(100) NOT NULL,
  `orden_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `cms_categorias` (`id`, `titulo`, `texto`, `imagen`, `orden_id`) VALUES
(1,	'Categoria prueb',	'esta es una categoria de prueba',	'3de527d5d479afeebeaf087ced3009d5.jpg',	1),
(3,	'Categoria prueb',	'esta es una categoria de prueba',	'3de527d5d479afeebeaf087ced3009d5.jpg',	9),
(4,	'Categoria prueb',	'esta es una categoria de prueba',	'3de527d5d479afeebeaf087ced3009d5.jpg',	8),
(5,	'Categoria prueb',	'esta es una categoria de prueba',	'3de527d5d479afeebeaf087ced3009d5.jpg',	7),
(7,	'Categoria prueb',	'esta es una categoria de prueba',	'3de527d5d479afeebeaf087ced3009d5.jpg',	6),
(8,	'Categoria prueb',	'esta es una categoria de prueba',	'3de527d5d479afeebeaf087ced3009d5.jpg',	5),
(9,	'Categoria prueb',	'esta es una categoria de prueba',	'3de527d5d479afeebeaf087ced3009d5.jpg',	4),
(10,	'Categoria prueb',	'esta es una categoria de prueba',	'3de527d5d479afeebeaf087ced3009d5.jpg',	3),
(14,	'Categoria prue',	'esta es una categoria de prueba',	'3de527d5d479afeebeaf087ced3009d5.jpg',	2),
(15,	'Categoria prueb',	'esta es una categoria de prueba',	'3de527d5d479afeebeaf087ced3009d5.jpg',	10);

DROP TABLE IF EXISTS `cms_contactos`;
CREATE TABLE `cms_contactos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mail` varchar(100) NOT NULL,
  `imagen` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `cms_contactos` (`id`, `mail`, `imagen`) VALUES
(1,	'david.perez@imagina.co',	'2e4ce2fdb855c693930fc2432960f379.jpg');

DROP TABLE IF EXISTS `cms_faqs`;
CREATE TABLE `cms_faqs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(120) NOT NULL,
  `texto` text NOT NULL,
  `orden_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `cms_faqs` (`id`, `titulo`, `texto`, `orden_id`) VALUES
(1,	'Pregunta 1',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.',	2),
(2,	'Pregunta 2',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.',	1),
(3,	'Pregunta 3',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.',	3);

DROP TABLE IF EXISTS `cms_footers`;
CREATE TABLE `cms_footers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `direccion` text NOT NULL,
  `telefono` text NOT NULL,
  `mail` varchar(20) NOT NULL,
  `twitter` varchar(100) NOT NULL,
  `facebook` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `cms_footers` (`id`, `direccion`, `telefono`, `mail`, `twitter`, `facebook`) VALUES
(1,	'Calle 47 1-65 LC-03\r\nBogotá - Colombia',	'(57+1) 8225678\r\n(57+1) 8225654',	'info@blp.com.co',	'https://twitter.com/',	'https://www.facebook.com/');

DROP TABLE IF EXISTS `cms_galerias`;
CREATE TABLE `cms_galerias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(30) NOT NULL,
  `texto` text NOT NULL,
  `imagen` varchar(100) NOT NULL,
  `orden_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `cms_galerias` (`id`, `titulo`, `texto`, `imagen`, `orden_id`) VALUES
(1,	'Galeria 1',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mauris dui, facilisis non hendrerit quis, varius et sem. Aliquam erat volutpat. Nullam sed metus ligula. Sed vehicula turpis libero, sit amet porttitor massa porta nec. Vivamus et eleifend orci. Quisque ornare lobortis dignissim.',	'36cfe4f06c41808317f4d12ba6e106fe.jpg',	2),
(2,	'Galeria 2',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mauris dui, facilisis non hendrerit quis, varius et sem. Aliquam erat volutpat. Nullam sed metus ligula. Sed vehicula turpis libero, sit amet porttitor massa porta nec. Vivamus et eleifend orci. Quisque ornare lobortis dignissim.',	'd5dfa3b6526d7d406a7ca4e161499525.png',	1),
(4,	'Galeria 1',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mauris dui, facilisis non hendrerit quis, varius et sem. Aliquam erat volutpat. Nullam sed metus ligula. Sed vehicula turpis libero, sit amet porttitor massa porta nec. Vivamus et eleifend orci. Quisque ornare lobortis dignissim.',	'36cfe4f06c41808317f4d12ba6e106fe.jpg',	3),
(5,	'Galeria 2',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mauris dui, facilisis non hendrerit quis, varius et sem. Aliquam erat volutpat. Nullam sed metus ligula. Sed vehicula turpis libero, sit amet porttitor massa porta nec. Vivamus et eleifend orci. Quisque ornare lobortis dignissim.',	'd5dfa3b6526d7d406a7ca4e161499525.png',	4),
(7,	'Galeria 1',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mauris dui, facilisis non hendrerit quis, varius et sem. Aliquam erat volutpat. Nullam sed metus ligula. Sed vehicula turpis libero, sit amet porttitor massa porta nec. Vivamus et eleifend orci. Quisque ornare lobortis dignissim.',	'36cfe4f06c41808317f4d12ba6e106fe.jpg',	5),
(8,	'Galeria 2',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mauris dui, facilisis non hendrerit quis, varius et sem. Aliquam erat volutpat. Nullam sed metus ligula. Sed vehicula turpis libero, sit amet porttitor massa porta nec. Vivamus et eleifend orci. Quisque ornare lobortis dignissim.',	'd5dfa3b6526d7d406a7ca4e161499525.png',	6),
(9,	'Galeria 1',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mauris dui, facilisis non hendrerit quis, varius et sem. Aliquam erat volutpat. Nullam sed metus ligula. Sed vehicula turpis libero, sit amet porttitor massa porta nec. Vivamus et eleifend orci. Quisque ornare lobortis dignissim.',	'36cfe4f06c41808317f4d12ba6e106fe.jpg',	7),
(10,	'Galeria 2',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mauris dui, facilisis non hendrerit quis, varius et sem. Aliquam erat volutpat. Nullam sed metus ligula. Sed vehicula turpis libero, sit amet porttitor massa porta nec. Vivamus et eleifend orci. Quisque ornare lobortis dignissim.',	'd5dfa3b6526d7d406a7ca4e161499525.png',	8),
(14,	'Galeria 1',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mauris dui, facilisis non hendrerit quis, varius et sem. Aliquam erat volutpat. Nullam sed metus ligula. Sed vehicula turpis libero, sit amet porttitor massa porta nec. Vivamus et eleifend orci. Quisque ornare lobortis dignissim.',	'36cfe4f06c41808317f4d12ba6e106fe.jpg',	9),
(15,	'Galeria 2',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mauris dui, facilisis non hendrerit quis, varius et sem. Aliquam erat volutpat. Nullam sed metus ligula. Sed vehicula turpis libero, sit amet porttitor massa porta nec. Vivamus et eleifend orci. Quisque ornare lobortis dignissim.',	'd5dfa3b6526d7d406a7ca4e161499525.png',	10);

DROP TABLE IF EXISTS `cms_groups`;
CREATE TABLE `cms_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `cms_groups` (`id`, `name`, `description`) VALUES
(1,	'superadmin',	'Super Administrador'),
(2,	'admin',	'Administrador'),
(3,	'usuarios',	'Usuarios');

DROP TABLE IF EXISTS `cms_login_attempts`;
CREATE TABLE `cms_login_attempts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `cms_menu`;
CREATE TABLE `cms_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `url` text,
  `icon` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre_UNIQUE` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `cms_menu` (`id`, `title`, `url`, `icon`) VALUES
(12,	'Destacados',	'cms/destacados',	'pictures_folder');

DROP TABLE IF EXISTS `cms_noticias`;
CREATE TABLE `cms_noticias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imagen` varchar(100) NOT NULL,
  `titulo` varchar(30) NOT NULL,
  `texto` text NOT NULL,
  `orden_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `cms_noticias` (`id`, `imagen`, `titulo`, `texto`, `orden_id`) VALUES
(1,	'eef2d68d725f69ab3c728c8c90a86886.jpg',	'Noticia 1',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mauris dui, facilisis non hendrerit quis, varius et sem. Aliquam erat volutpat. Nullam sed metus ligula. Sed vehicula turpis libero, sit amet porttitor massa porta nec. Vivamus et eleifend orci. Quisque ornare lobortis dignissim. Aenean sit amet tortor commodo, viverra metus sed, tincidunt lorem. Vivamus pellentesque facilisis risus, dignissim tempor neque aliquam ac. Nunc scelerisque tellus nulla, nec mattis enim imperdiet vitae. Nunc mattis nisl nec libero eleifend placerat. Integer eu eleifend enim. Cras porttitor molestie urna a bibendum. Pellentesque laoreet eros id ipsum pulvinar, et semper erat tempus. Pellentesque dapibus molestie dolor. Duis luctus neque vitae nibh congue, sed ultrices felis commodo.\r\n\r\nAenean cursus, mauris volutpat ullamcorper convallis, tellus sapien convallis lacus, ac viverra ligula tortor ac erat. Curabitur tincidunt porttitor tortor, vitae suscipit neque consectetur dapibus. Vestibulum nisl diam, blandit in ante et, fermentum faucibus tellus. Praesent neque odio, convallis vitae dolor sollicitudin, tristique tristique urna. Phasellus auctor nisl id neque commodo, in suscipit sapien mollis. Ut et metus sapien. Aliquam sed libero quis est tincidunt tempus. Cras ultrices sollicitudin justo, et mollis purus.\r\n\r\nCurabitur dapibus odio eget diam lobortis cursus. Proin aliquam velit sem, feugiat porta neque fermentum sit amet. Praesent nec elementum neque, vitae ultricies magna. Sed porttitor et lectus in venenatis. Nulla varius magna sed augue rutrum sagittis. Nam mattis turpis quis lorem luctus, et elementum orci faucibus. Nullam faucibus in libero eget semper.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mauris dui, facilisis non hendrerit quis, varius et sem. Aliquam erat volutpat. Nullam sed metus ligula. Sed vehicula turpis libero, sit amet porttitor massa porta nec. Vivamus et eleifend orci. Quisque ornare lobortis dignissim. Aenean sit amet tortor commodo, viverra metus sed, tincidunt lorem. Vivamus pellentesque facilisis risus, dignissim tempor neque aliquam ac. Nunc scelerisque tellus nulla, nec mattis enim imperdiet vitae. Nunc mattis nisl nec libero eleifend placerat. Integer eu eleifend enim. Cras porttitor molestie urna a bibendum. Pellentesque laoreet eros id ipsum pulvinar, et semper erat tempus. Pellentesque dapibus molestie dolor. Duis luctus neque vitae nibh congue, sed ultrices felis commodo.',	1),
(2,	'754ae5e92b2562ddbc7da6a0fd0dd78c.jpg',	'Noticia 2',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mauris dui, facilisis non hendrerit quis, varius et sem. Aliquam erat volutpat. Nullam sed metus ligula. Sed vehicula turpis libero, sit amet porttitor massa porta nec. Vivamus et eleifend orci. Quisque ornare lobortis dignissim. Aenean sit amet tortor commodo, viverra metus sed, tincidunt lorem. Vivamus pellentesque facilisis risus, dignissim tempor neque aliquam ac. Nunc scelerisque tellus nulla, nec mattis enim imperdiet vitae. Nunc mattis nisl nec libero eleifend placerat. Integer eu eleifend enim. Cras porttitor molestie urna a bibendum. Pellentesque laoreet eros id ipsum pulvinar, et semper erat tempus. Pellentesque dapibus molestie dolor. Duis luctus neque vitae nibh congue, sed ultrices felis commodo.\r\n\r\nAenean cursus, mauris volutpat ullamcorper convallis, tellus sapien convallis lacus, ac viverra ligula tortor ac erat. Curabitur tincidunt porttitor tortor, vitae suscipit neque consectetur dapibus. Vestibulum nisl diam, blandit in ante et, fermentum faucibus tellus. Praesent neque odio, convallis vitae dolor sollicitudin, tristique tristique urna. Phasellus auctor nisl id neque commodo, in suscipit sapien mollis. Ut et metus sapien. Aliquam sed libero quis est tincidunt tempus. Cras ultrices sollicitudin justo, et mollis purus.\r\n\r\nCurabitur dapibus odio eget diam lobortis cursus. Proin aliquam velit sem, feugiat porta neque fermentum sit amet. Praesent nec elementum neque, vitae ultricies magna. Sed porttitor et lectus in venenatis. Nulla varius magna sed augue rutrum sagittis. Nam mattis turpis quis lorem luctus, et elementum orci faucibus. Nullam faucibus in libero eget semper.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mauris dui, facilisis non hendrerit quis, varius et sem. Aliquam erat volutpat. Nullam sed metus ligula. Sed vehicula turpis libero, sit amet porttitor massa porta nec. Vivamus et eleifend orci. Quisque ornare lobortis dignissim. Aenean sit amet tortor commodo, viverra metus sed, tincidunt lorem. Vivamus pellentesque facilisis risus, dignissim tempor neque aliquam ac. Nunc scelerisque tellus nulla, nec mattis enim imperdiet vitae. Nunc mattis nisl nec libero eleifend placerat. Integer eu eleifend enim. Cras porttitor molestie urna a bibendum. Pellentesque laoreet eros id ipsum pulvinar, et semper erat tempus. Pellentesque dapibus molestie dolor. Duis luctus neque vitae nibh congue, sed ultrices felis commodo.',	2),
(3,	'e6938a435ce940b5498b73809dcc5a2a.jpg',	'Noticia 3',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mauris dui, facilisis non hendrerit quis, varius et sem. Aliquam erat volutpat. Nullam sed metus ligula. Sed vehicula turpis libero, sit amet porttitor massa porta nec. Vivamus et eleifend orci. Quisque ornare lobortis dignissim. Aenean sit amet tortor commodo, viverra metus sed, tincidunt lorem. Vivamus pellentesque facilisis risus, dignissim tempor neque aliquam ac. Nunc scelerisque tellus nulla, nec mattis enim imperdiet vitae. Nunc mattis nisl nec libero eleifend placerat. Integer eu eleifend enim. Cras porttitor molestie urna a bibendum. Pellentesque laoreet eros id ipsum pulvinar, et semper erat tempus. Pellentesque dapibus molestie dolor. Duis luctus neque vitae nibh congue, sed ultrices felis commodo.\r\n\r\nAenean cursus, mauris volutpat ullamcorper convallis, tellus sapien convallis lacus, ac viverra ligula tortor ac erat. Curabitur tincidunt porttitor tortor, vitae suscipit neque consectetur dapibus. Vestibulum nisl diam, blandit in ante et, fermentum faucibus tellus. Praesent neque odio, convallis vitae dolor sollicitudin, tristique tristique urna. Phasellus auctor nisl id neque commodo, in suscipit sapien mollis. Ut et metus sapien. Aliquam sed libero quis est tincidunt tempus. Cras ultrices sollicitudin justo, et mollis purus.\r\n\r\nCurabitur dapibus odio eget diam lobortis cursus. Proin aliquam velit sem, feugiat porta neque fermentum sit amet. Praesent nec elementum neque, vitae ultricies magna. Sed porttitor et lectus in venenatis. Nulla varius magna sed augue rutrum sagittis. Nam mattis turpis quis lorem luctus, et elementum orci faucibus. Nullam faucibus in libero eget semper.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mauris dui, facilisis non hendrerit quis, varius et sem. Aliquam erat volutpat. Nullam sed metus ligula. Sed vehicula turpis libero, sit amet porttitor massa porta nec. Vivamus et eleifend orci. Quisque ornare lobortis dignissim. Aenean sit amet tortor commodo, viverra metus sed, tincidunt lorem. Vivamus pellentesque facilisis risus, dignissim tempor neque aliquam ac. Nunc scelerisque tellus nulla, nec mattis enim imperdiet vitae. Nunc mattis nisl nec libero eleifend placerat. Integer eu eleifend enim. Cras porttitor molestie urna a bibendum. Pellentesque laoreet eros id ipsum pulvinar, et semper erat tempus. Pellentesque dapibus molestie dolor. Duis luctus neque vitae nibh congue, sed ultrices felis commodo.',	3);

DROP TABLE IF EXISTS `cms_noticia_homes`;
CREATE TABLE `cms_noticia_homes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(12) NOT NULL,
  `subtitulo` varchar(25) NOT NULL,
  `texto` text NOT NULL,
  `link` varchar(100) NOT NULL,
  `imagen` varchar(100) NOT NULL,
  `orden_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `cms_noticia_homes` (`id`, `titulo`, `subtitulo`, `texto`, `link`, `imagen`, `orden_id`) VALUES
(1,	'Noticia 1',	'noticia 1',	'noticia 1',	'',	'd9af01f164e790b9dec4ab3790c76188.jpg',	2),
(2,	'Noticia 2',	'noticia 2',	'noticia 2',	'',	'c37e2c90445c84bbffe0715bad66d935.png',	1),
(4,	'Noticia 3',	'noticia 3',	'noticia 3',	'',	'5bd79ec122277fc0967b35f1f870636a.jpg',	3);

DROP TABLE IF EXISTS `cms_proyectos`;
CREATE TABLE `cms_proyectos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(15) NOT NULL,
  `texto` text NOT NULL,
  `telefono` varchar(100) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `ubicacion` varchar(100) NOT NULL,
  `mapa` varchar(300) NOT NULL,
  `video` varchar(100) NOT NULL,
  `mas_info` varchar(100) NOT NULL,
  `imagen` varchar(100) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `orden_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `id_categoria` (`id_categoria`),
  CONSTRAINT `cms_proyectos_ibfk_1` FOREIGN KEY (`id_categoria`) REFERENCES `cms_categorias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `cms_proyectos` (`id`, `titulo`, `texto`, `telefono`, `direccion`, `ubicacion`, `mapa`, `video`, `mas_info`, `imagen`, `id_categoria`, `orden_id`) VALUES
(1,	'Proyecto 1',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.',	'(415) 124-5678',	'PO Box 21177 lorem ipsum dolor Victoria 8011 lorem',	'PO Box 21177 lorem ipsum dolor Victoria 8011 lorem',	'https://maps.google.com/?ie=UTF8&amp;t=m&amp;ll=4.667452,-74.107933&amp;spn=0.119764,0.145912&amp;z=12&amp;output=embed',	'http://www.youtube.com/watch?v=ZGf6U_NmCr4',	'http://www.google.com.co',	'a8019066c7fb605d09ac0db8f7c1451a.jpg',	1,	1),
(3,	'Proyecto 1',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.',	'(415) 124-5678',	'PO Box 21177 lorem ipsum dolor Victoria 8011 lorem',	'PO Box 21177 lorem ipsum dolor Victoria 8011 lorem',	'https://maps.google.com/?ie=UTF8&amp;t=m&amp;ll=4.667452,-74.107933&amp;spn=0.119764,0.145912&amp;z=12&amp;output=embed',	'http://www.youtube.com/watch?v=ZGf6U_NmCr4',	'http://www.google.com.co',	'a8019066c7fb605d09ac0db8f7c1451a.jpg',	1,	1),
(4,	'Proyecto 1',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.',	'(415) 124-5678',	'PO Box 21177 lorem ipsum dolor Victoria 8011 lorem',	'PO Box 21177 lorem ipsum dolor Victoria 8011 lorem',	'https://maps.google.com/?ie=UTF8&amp;t=m&amp;ll=4.667452,-74.107933&amp;spn=0.119764,0.145912&amp;z=12&amp;output=embed',	'http://www.youtube.com/watch?v=ZGf6U_NmCr4',	'http://www.google.com.co',	'a8019066c7fb605d09ac0db8f7c1451a.jpg',	1,	1),
(5,	'Proyecto 1',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.',	'(415) 124-5678',	'PO Box 21177 lorem ipsum dolor Victoria 8011 lorem',	'PO Box 21177 lorem ipsum dolor Victoria 8011 lorem',	'https://maps.google.com/?ie=UTF8&amp;t=m&amp;ll=4.667452,-74.107933&amp;spn=0.119764,0.145912&amp;z=12&amp;output=embed',	'http://www.youtube.com/watch?v=ZGf6U_NmCr4',	'http://www.google.com.co',	'a8019066c7fb605d09ac0db8f7c1451a.jpg',	1,	1),
(6,	'Proyecto 1',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.',	'(415) 124-5678',	'PO Box 21177 lorem ipsum dolor Victoria 8011 lorem',	'PO Box 21177 lorem ipsum dolor Victoria 8011 lorem',	'https://maps.google.com/?ie=UTF8&amp;t=m&amp;ll=4.667452,-74.107933&amp;spn=0.119764,0.145912&amp;z=12&amp;output=embed',	'http://www.youtube.com/watch?v=ZGf6U_NmCr4',	'http://www.google.com.co',	'a8019066c7fb605d09ac0db8f7c1451a.jpg',	1,	1),
(7,	'Proyecto 1',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.',	'(415) 124-5678',	'PO Box 21177 lorem ipsum dolor Victoria 8011 lorem',	'PO Box 21177 lorem ipsum dolor Victoria 8011 lorem',	'https://maps.google.com/?ie=UTF8&amp;t=m&amp;ll=4.667452,-74.107933&amp;spn=0.119764,0.145912&amp;z=12&amp;output=embed',	'http://www.youtube.com/watch?v=ZGf6U_NmCr4',	'http://www.google.com.co',	'a8019066c7fb605d09ac0db8f7c1451a.jpg',	1,	1),
(8,	'Proyecto 1',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.',	'(415) 124-5678',	'PO Box 21177 lorem ipsum dolor Victoria 8011 lorem',	'PO Box 21177 lorem ipsum dolor Victoria 8011 lorem',	'https://maps.google.com/?ie=UTF8&amp;t=m&amp;ll=4.667452,-74.107933&amp;spn=0.119764,0.145912&amp;z=12&amp;output=embed',	'http://www.youtube.com/watch?v=ZGf6U_NmCr4',	'http://www.google.com.co',	'a8019066c7fb605d09ac0db8f7c1451a.jpg',	1,	1),
(9,	'Proyecto 1',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.',	'(415) 124-5678',	'PO Box 21177 lorem ipsum dolor Victoria 8011 lorem',	'PO Box 21177 lorem ipsum dolor Victoria 8011 lorem',	'https://maps.google.com/?ie=UTF8&amp;t=m&amp;ll=4.667452,-74.107933&amp;spn=0.119764,0.145912&amp;z=12&amp;output=embed',	'http://www.youtube.com/watch?v=ZGf6U_NmCr4',	'http://www.google.com.co',	'a8019066c7fb605d09ac0db8f7c1451a.jpg',	1,	1),
(10,	'Proyecto 1',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.',	'(415) 124-5678',	'PO Box 21177 lorem ipsum dolor Victoria 8011 lorem',	'PO Box 21177 lorem ipsum dolor Victoria 8011 lorem',	'https://maps.google.com/?ie=UTF8&amp;t=m&amp;ll=4.667452,-74.107933&amp;spn=0.119764,0.145912&amp;z=12&amp;output=embed',	'http://www.youtube.com/watch?v=ZGf6U_NmCr4',	'http://www.google.com.co',	'a8019066c7fb605d09ac0db8f7c1451a.jpg',	1,	1),
(11,	'Proyecto 1',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.',	'(415) 124-5678',	'PO Box 21177 lorem ipsum dolor Victoria 8011 lorem',	'PO Box 21177 lorem ipsum dolor Victoria 8011 lorem',	'https://maps.google.com/?ie=UTF8&amp;t=m&amp;ll=4.667452,-74.107933&amp;spn=0.119764,0.145912&amp;z=12&amp;output=embed',	'http://www.youtube.com/watch?v=ZGf6U_NmCr4',	'http://www.google.com.co',	'a8019066c7fb605d09ac0db8f7c1451a.jpg',	1,	1),
(12,	'Proyecto 1',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.',	'(415) 124-5678',	'PO Box 21177 lorem ipsum dolor Victoria 8011 lorem',	'PO Box 21177 lorem ipsum dolor Victoria 8011 lorem',	'https://maps.google.com/?ie=UTF8&amp;t=m&amp;ll=4.667452,-74.107933&amp;spn=0.119764,0.145912&amp;z=12&amp;output=embed',	'http://www.youtube.com/watch?v=ZGf6U_NmCr4',	'http://www.google.com.co',	'a8019066c7fb605d09ac0db8f7c1451a.jpg',	1,	1),
(13,	'Proyecto 1',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.',	'(415) 124-5678',	'PO Box 21177 lorem ipsum dolor Victoria 8011 lorem',	'PO Box 21177 lorem ipsum dolor Victoria 8011 lorem',	'https://maps.google.com/?ie=UTF8&amp;t=m&amp;ll=4.667452,-74.107933&amp;spn=0.119764,0.145912&amp;z=12&amp;output=embed',	'http://www.youtube.com/watch?v=ZGf6U_NmCr4',	'http://www.google.com.co',	'a8019066c7fb605d09ac0db8f7c1451a.jpg',	1,	1),
(15,	'Proyecto 1',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.',	'(415) 124-5678',	'PO Box 21177 lorem ipsum dolor Victoria 8011 lorem',	'PO Box 21177 lorem ipsum dolor Victoria 8011 lorem',	'https://maps.google.com/?ie=UTF8&amp;t=m&amp;ll=4.667452,-74.107933&amp;spn=0.119764,0.145912&amp;z=12&amp;output=embed',	'http://www.youtube.com/watch?v=ZGf6U_NmCr4',	'http://www.google.com.co',	'a8019066c7fb605d09ac0db8f7c1451a.jpg',	10,	1),
(16,	'Proyecto 1',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.',	'(415) 124-5678',	'PO Box 21177 lorem ipsum dolor Victoria 8011 lorem',	'PO Box 21177 lorem ipsum dolor Victoria 8011 lorem',	'https://maps.google.com/?ie=UTF8&amp;t=m&amp;ll=4.667452,-74.107933&amp;spn=0.119764,0.145912&amp;z=12&amp;output=embed',	'http://www.youtube.com/watch?v=ZGf6U_NmCr4',	'http://www.google.com.co',	'a8019066c7fb605d09ac0db8f7c1451a.jpg',	3,	1),
(17,	'Proyecto 1',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.',	'(415) 124-5678',	'PO Box 21177 lorem ipsum dolor Victoria 8011 lorem',	'PO Box 21177 lorem ipsum dolor Victoria 8011 lorem',	'https://maps.google.com/?ie=UTF8&amp;t=m&amp;ll=4.667452,-74.107933&amp;spn=0.119764,0.145912&amp;z=12&amp;output=embed',	'http://www.youtube.com/watch?v=ZGf6U_NmCr4',	'http://www.google.com.co',	'a8019066c7fb605d09ac0db8f7c1451a.jpg',	10,	1);

DROP TABLE IF EXISTS `cms_proyecto_homes`;
CREATE TABLE `cms_proyecto_homes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(15) NOT NULL,
  `subtitulo` varchar(15) NOT NULL,
  `texto` text NOT NULL,
  `link` varchar(100) NOT NULL,
  `imagen` varchar(100) NOT NULL,
  `orden_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `cms_proyecto_homes` (`id`, `titulo`, `subtitulo`, `texto`, `link`, `imagen`, `orden_id`) VALUES
(1,	'Proyecto 1',	'proyecto 1',	'proyecto 1',	'',	'e94ee2532c3db27a6489d7d51b642dd1.jpg',	1),
(2,	'Proyecto 2',	'proyecto 2',	'proyecto 2',	'',	'9fd04441bf43d5cd8b2c8bb9e430b53a.jpg',	1),
(4,	'Proyecto 3',	'proyecto 3',	'proyecto 3',	'',	'95b2fdface876d85a2dc29808f1671e3.jpg',	1);

DROP TABLE IF EXISTS `cms_quienes`;
CREATE TABLE `cms_quienes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(30) NOT NULL,
  `texto` text NOT NULL,
  `imagen` varchar(100) NOT NULL,
  `orden_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `cms_quienes` (`id`, `titulo`, `texto`, `imagen`, `orden_id`) VALUES
(1,	'Historia',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.',	'20c700411adae3d215604deeedab791f.jpg',	2),
(2,	'Filosofia',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.',	'',	1),
(3,	'Misión',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.',	'48fa28569c175569c7da43090725913a.jpg',	3),
(4,	'Visión',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Ut feugiat porttitor neque, rutrum gravida dolor ullamcorper a. In sed dapibus augue. Aliquam nisl elit, consectetur a eros eget, volutpat interdum nulla. Quisque fermentum lorem dolor, fermentum mollis turpis commodo non. In tempor, nulla vel egestas sagittis, arcu sapien dapibus lorem, fringilla vulputate nunc quam id mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt neque et urna pellentesque, id suscipit libero hendrerit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque mauris nisl, adipiscing posuere velit non, congue egestas ipsum. Quisque sagittis velit eu purus mattis vulputate. Morbi tristique, eros vitae commodo dictum, libero arcu rhoncus sapien, vitae venenatis tellus ante a eros.',	'',	4);

DROP TABLE IF EXISTS `cms_sessions`;
CREATE TABLE `cms_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `cms_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('50688685473ccd2d63a1af0b64e7f21e',	'127.0.0.1',	'Mozilla/5.0 (Windows NT 6.1; rv:25.0) Gecko/20100101 Firefox/25.0',	1387223535,	'a:4:{s:9:\"user_data\";s:0:\"\";s:5:\"email\";s:18:\"cms@imaginamos.com\";s:2:\"id\";s:1:\"5\";s:7:\"user_id\";s:1:\"5\";}'),
('873e22a143c4ff8489195d3fc4ff317b',	'127.0.0.1',	'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36',	1387222932,	'a:4:{s:9:\"user_data\";s:0:\"\";s:5:\"email\";s:18:\"cms@imaginamos.com\";s:2:\"id\";s:1:\"5\";s:7:\"user_id\";s:1:\"5\";}'),
('8d34a89ce567f2b34095ecc3672fa381',	'127.0.0.1',	'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36',	1386880708,	'a:4:{s:9:\"user_data\";s:0:\"\";s:5:\"email\";s:18:\"cms@imaginamos.com\";s:2:\"id\";s:1:\"5\";s:7:\"user_id\";s:1:\"5\";}'),
('c776bf407c6a6147e200dfbfc9bd9dd6',	'127.0.0.1',	'Mozilla/5.0 (Windows NT 6.1; rv:26.0) Gecko/20100101 Firefox/26.0',	1389972890,	'a:4:{s:9:\"user_data\";s:0:\"\";s:5:\"email\";s:18:\"cms@imaginamos.com\";s:2:\"id\";s:1:\"5\";s:7:\"user_id\";s:1:\"5\";}'),
('f1c82af4b66a8cc0f911241718424fef',	'127.0.0.1',	'Mozilla/5.0 (Windows NT 6.1; rv:26.0) Gecko/20100101 Firefox/26.0',	1391811815,	'a:4:{s:9:\"user_data\";s:0:\"\";s:5:\"email\";s:18:\"cms@imaginamos.com\";s:2:\"id\";s:1:\"5\";s:7:\"user_id\";s:1:\"5\";}');

DROP TABLE IF EXISTS `cms_users`;
CREATE TABLE `cms_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(40) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `cms_users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(5,	UNHEX('7F000001'),	'administrator',	'092e624ccaf41c1b9c0dd32a1041043a82507bc7',	'e0efe63787',	'cms@imaginamos.com',	NULL,	NULL,	NULL,	'1218e83c71363e71c292b071dace76d3f56b47af',	1343253917,	2014,	1,	NULL,	NULL,	NULL,	NULL);

DROP TABLE IF EXISTS `cms_users_groups`;
CREATE TABLE `cms_users_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_users_groups` (`user_id`),
  KEY `group_users_groups` (`group_id`),
  CONSTRAINT `group_users_groups` FOREIGN KEY (`group_id`) REFERENCES `cms_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_users_groups` FOREIGN KEY (`user_id`) REFERENCES `cms_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `cms_users_groups` (`id`, `user_id`, `group_id`) VALUES
(5,	5,	1);

-- 2014-02-07 18:07:10
